explorer_add_executable(PulseExplorer
  MOC_HEADERS
    DynamicControlsWidget.h
    MainExplorerWindow.h
    showcases/AnaphylaxisShowcaseWidget.h
    showcases/MultiTraumaShowcaseWidget.h
  SOURCES
    DynamicControlsWidget.cxx
    Main.cxx
    MainExplorerWindow.cxx
    showcases/AnaphylaxisShowcaseWidget.cxx
    showcases/MultiTraumaShowcaseWidget.cxx
  UI_FILES
    ui/AnaphylaxisShowcase.ui
    ui/DynamicControls.ui
    ui/MainExplorerWindow.ui
    ui/MultiTraumaShowcase.ui
  LINK_LIBRARIES
    PulseExplorerWidgets
)

#target_include_directories(${PROJECT_NAME} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
#target_include_directories(${PROJECT_NAME} PRIVATE ${CMAKE_CURRENT_BINARY_DIR})

if(NOT MSVC)
  target_link_libraries(PulseExplorer PRIVATE -lpthread -lm)
endif()

#if(MSVC) # Configure running executable out of MSVC
#   if(USE_SYSTEM_Pulse)
#    file(COPY ${Qwt_ROOT_DIR}/lib/qwt.dll  DESTINATION ${Pulse_ROOT}/bin)
#    file(COPY ${Qwt_ROOT_DIR}/lib/qwtd.dll DESTINATION ${Pulse_ROOT}/bin)
#    set_property(TARGET ${PROJECT_NAME} PROPERTY VS_DEBUGGER_WORKING_DIRECTORY "${Pulse_ROOT}/bin")
#  else()
#    set_property(TARGET ${PROJECT_NAME} PROPERTY VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_INSTALL_PREFIX}")
#  endif()
#endif()

#-----------------------------------------------------------------------------
#                                  INSTALL
#-----------------------------------------------------------------------------

set(qt_framework_dest_dir bin)
set(qt_plugin_dest_dir bin)
set(qt_rsrc_dest_dir bin)
set(pulse_dest_dir .)
if(APPLE)
  set(qt_framework_dest_dir "PulseExplorer.app/Contents/Library/Framework")
  set(qt_plugin_dest_dir "PulseExplorer.app/Contents/Plugins")
  set(qt_rsrc_dest_dir "PulseExplorer.app/Contents/Resources")
  set(pulse_dest_dir "PulseExplorer.app/Contents/Pulse")
endif()

#install (DIRECTORY "${CMAKE_SOURCE_DIR}/data" DESTINATION .)
# install pulse components
install(DIRECTORY "${Pulse_ROOT}/bin/config" DESTINATION ${pulse_dest_dir} OPTIONAL)
install(DIRECTORY "${Pulse_ROOT}/bin/ecg" DESTINATION ${pulse_dest_dir} OPTIONAL)
install(DIRECTORY "${Pulse_ROOT}/bin/environments" DESTINATION ${pulse_dest_dir} OPTIONAL)
install(DIRECTORY "${Pulse_ROOT}/bin/nutrition" DESTINATION ${pulse_dest_dir} OPTIONAL)
install(DIRECTORY "${Pulse_ROOT}/bin/patients" DESTINATION ${pulse_dest_dir} OPTIONAL)
install(DIRECTORY "${Pulse_ROOT}/bin/resource" DESTINATION ${pulse_dest_dir} OPTIONAL)
install(DIRECTORY "${Pulse_ROOT}/bin/states" DESTINATION ${pulse_dest_dir} OPTIONAL)
install(DIRECTORY "${Pulse_ROOT}/bin/substances" DESTINATION ${pulse_dest_dir} OPTIONAL)
#install(DIRECTORY "${Pulse_ROOT}/bin/verification/scenarios" DESTINATION ${pulse_dest_dir}
#        PATTERN "*.csv" EXCLUDE OPTIONAL)

# Install (ONLY) QWT shared library
if(Qwt_ROOT_DIR)
  if(WIN32)
    install(FILES ${Qwt_ROOT_DIR}/lib/qwt.dll
             CONFIGURATIONS Release
             RUNTIME DESTINATION bin)
    install(FILES ${Qwt_ROOT_DIR}/lib/qwtd.dll
             CONFIGURATIONS Debug
             RUNTIME DESTINATION bin)
  elseif(NOT APPLE)
    # Grab all the libqwt files
    file(GLOB Qwt_RELEASE_LIBS ${Qwt_ROOT_DIR}/lib/libqwt.*)
    message(STATUS "Qwt_RELEASE_LIBS: ${Qwt_RELEASE_LIBS}")
    if(Qwt_RELEASE_LIBS)
      install(FILES ${Qwt_RELEASE_LIBS}
              CONFIGURATIONS Release
              RUNTIME DESTINATION bin)
    endif()

    file(GLOB Qwt_DEBUG_LIBS ${Qwt_ROOT_DIR}/lib/libqwtd.*)
    message(STATUS "Qwt_DEBUG_LIBS: ${Qwt_DEBUG_LIBS}")
    if(Qwt_DEBUG_LIBS)
      install(FILES ${Qwt_DEBUG_LIBS}
              CONFIGURATIONS Debug
              RUNTIME DESTINATION bin)
    endif()
  else()# APPLE
    # Copy the files up, installation of qwt on a mac is different...
    # But we are copying up so we can run from our pulse install directory when developing
    file(COPY ${Qwt_ROOT_DIR}/lib/qwt.framework DESTINATION ${Pulse_ROOT}/bin) 
  endif()
endif()

install(CODE "
  file(WRITE \"\${CMAKE_INSTALL_PREFIX}/${qt_rsrc_dest_dir}/qt.conf\" \"[Paths]\nPlugins = Plugins\")
  ")
if(APPLE)
  install(FILES "${CMAKE_SOURCE_DIR}/resource/pulse.icns" DESTINATION ${qt_rsrc_dest_dir})
endif()

#-----------------------------------------------------------------------------
#                                    CPACK
#-----------------------------------------------------------------------------

if (WIN32)
  set(CMAKE_INSTALL_UCRT_LIBRARIES TRUE)
endif()
include(InstallRequiredSystemLibraries)

# Get Qt deployment executable
get_target_property( uic_location Qt5::uic IMPORTED_LOCATION )
get_filename_component( _qt_bin_dir ${uic_location} DIRECTORY )
set(qt_root_dir "${_qt_bin_dir}/..")
set(_qt_version "${Qt5_VERSION_MAJOR}.${Qt5_VERSION_MINOR}.${Qt5_VERSION_PATCH}")
message(STATUS "QT located : ${qt_root_dir}")

if(WIN32)
  find_program(DEPLOYQT_EXECUTABLE windeployqt HINTS "${_qt_bin_dir}")
  if( NOT EXISTS ${DEPLOYQT_EXECUTABLE} )
    message( FATAL_ERROR "Failed to locate qtdeploy executable" )
  else()
    install( CODE "execute_process(COMMAND \"${DEPLOYQT_EXECUTABLE}\" \"\${CMAKE_INSTALL_PREFIX}/bin/qwt.dll\")" )
    install( CODE "execute_process(COMMAND \"${DEPLOYQT_EXECUTABLE}\" --opengl \"\${CMAKE_INSTALL_PREFIX}/bin/PulseExplorer.exe\")" )
  endif()
  install (FILES "${CMAKE_SOURCE_DIR}/cmake/PulseExplorer.bat" DESTINATION "./")
elseif(APPLE)
  install(DIRECTORY "${qt_root_dir}/plugins/iconengines" DESTINATION ${qt_plugin_dest_dir})
  install(DIRECTORY "${qt_root_dir}/plugins/imageformats" DESTINATION ${qt_plugin_dest_dir})
  install(DIRECTORY "${qt_root_dir}/plugins/platforms" DESTINATION ${qt_plugin_dest_dir})
  install(DIRECTORY "${qt_root_dir}/translations" DESTINATION ${qt_plugin_dest_dir})
  install(DIRECTORY ${Qwt_ROOT_DIR}/lib/qwt.framework DESTINATION ${qt_framework_dest_dir})
  install(CODE "
    file(GLOB_RECURSE QTPLUGINS \"\${CMAKE_INSTALL_PREFIX}/${qt_plugin_dest_dir}/*${CMAKE_SHARED_LIBRARY_SUFFIX}\")
    # Fix permissions on copied libraries so they can be fixed up.
    set(BU_CHMOD_BUNDLE_ITEMS ON) # fixes permissions on libraries
    include(BundleUtilities)
    fixup_bundle(\"\${CMAKE_INSTALL_PREFIX}/PulseExplorer.app\" \"\${QTPLUGINS}\" \"${qt_root_dir}/lib;${Qwt_ROOT_DIR}/lib/\")
  ")
else()
  foreach(target ${QT_LIBRARIES})
    get_target_property(type ${target} TYPE)
    if(NOT type STREQUAL "SHARED_LIBRARY")
      continue()
    endif()
    get_property(location TARGET ${target} PROPERTY LOCATION_RELEASE)
    # Install .so and versioned .so.x.y
    get_filename_component(QT_LIB_DIR_tmp ${location} PATH)
    get_filename_component(QT_LIB_NAME_tmp ${location} NAME)
    string(REPLACE ".${_qt_version}" "" QT_LIB_NAME_tmp ${QT_LIB_NAME_tmp})
    install(DIRECTORY ${QT_LIB_DIR_tmp}/
        DESTINATION "bin" COMPONENT Runtime
        FILES_MATCHING PATTERN "${QT_LIB_NAME_tmp}*"
        PATTERN "${QT_LIB_NAME_tmp}*.debug" EXCLUDE)
  endforeach()
  install(DIRECTORY "${qt_root_dir}/plugins/iconengines" DESTINATION ${qt_plugin_dest_dir})
  install(DIRECTORY "${qt_root_dir}/plugins/imageformats" DESTINATION ${qt_plugin_dest_dir})
  install(DIRECTORY "${qt_root_dir}/plugins/platforms" DESTINATION ${qt_plugin_dest_dir})
  install(DIRECTORY "${qt_root_dir}/translations" DESTINATION ${qt_plugin_dest_dir})
  # ICU libraries
  file(GLOB ICU_LIBS ${qt_root_dir}/lib/libicu*)
  install(FILES ${ICU_LIBS}
    DESTINATION "bin" COMPONENT Runtime)
  # Shell script to start the application
  configure_file(${CMAKE_SOURCE_DIR}/cmake/PulseExplorer.sh.in PulseExplorer.sh @ONLY)
  install (FILES ${CMAKE_BINARY_DIR}/src/PulseExplorer.sh DESTINATION "./")
endif()

# Define package dependencies
set(EXPLORER_DEPS "")

if(EXISTS /etc/redhat-release)
  file(READ /etc/redhat-release RHEL_VERSION)
  string(REGEX REPLACE ".*release ([^\\. ]*).*" "\\1" RHEL_VERSION "${RHEL_VERSION}")
  set(CPACK_SYSTEM_NAME "el${RHEL_VERSION}.${CMAKE_SYSTEM_PROCESSOR}")
  set(CPACK_RPM_PACKAGE_AUTOREQPROV " no")
  set(CPACK_RPM_PACKAGE_REQUIRES "${EXPLORER_DEPS}")
else()
  set(CPACK_SYSTEM_NAME "${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR}")
endif()

if (APPLE)
  #set(CPACK_GENERATOR ZIP)
  set(CPACK_GENERATOR DragNDrop)
elseif(MSVC)
  set(CPACK_GENERATOR ZIP)
else()
  set(CPACK_GENERATOR TGZ)
endif()
set(CPACK_PACKAGE_NAME              "PulseExplorer")
set(CPACK_PACKAGE_VENDOR            "Kitware, Inc.")
set(CPACK_PACKAGE_CONTACT           "kitware@kitware.com")
set(CPACK_MONOLITHIC_INSTALL        true)
set(CPACK_PACKAGE_VERSION_MAJOR     "${PROJECT_VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR     "${PROJECT_VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH     "${PROJECT_VERSION_PATCH}")
set(CPACK_PACKAGE_VERSION           "${PROJECT_VERSION}")
#set(CPACK_RESOURCE_FILE_LICENSE     "${CMAKE_SOURCE_DIR}/LICENSE")
set(CPACK_PACKAGE_FILE_NAME         "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}-${CPACK_SYSTEM_NAME}")
set(CPACK_PACKAGE_ICON              "${CMAKE_SOURCE_DIR}/resource/pulse_logo.png")
#set(CPACK_DMG_DS_STORE              "${CMAKE_CURRENT_SOURCE_DIR}/packaging/DS_Store")

if(APPLE)
  set(CPACK_BUNDLE_STARTUP_COMMAND  PulseExplorer)
  set(CPACK_PACKAGE_ICON            "${CMAKE_SOURCE_DIR}/resource/pulse_logo.png")
  set(CPACK_BUNDLE_ICON             pulse.icns)
  set(MACOSX_BUNDLE_ICON_FILE       pulse.icns)
  set(MACOSX_BUNDLE_BUNDLE_VERSION  "${PROJECT_VERSION}")
  set(MACOSX_BUNDLE_EXECUTABLE_NAME PulseExplorer)
  set_target_properties(PulseExplorer
    PROPERTIES
    MACOSX_BUNDLE TRUE
    MACOSX_BUNDLE_INFO_PLIST ${CMAKE_SOURCE_DIR}/resource/explorer.plist.in)
endif()

# NSIS Package options
set(CPACK_NSIS_INSTALLED_ICON_NAME "bin\\\\PulseExplorer.exe")
set(CPACK_NSIS_MUI_ICON "${CMAKE_CURRENT_SOURCE_DIR}/resource\\\\pulse.ico")
set(CPACK_NSIS_MUI_UNIICON "${CMAKE_CURRENT_SOURCE_DIR}/resource\\\\pulse.ico")
set(CPACK_NSIS_CONTACT "${CPACK_PACKAGE_CONTACT}")
set(CPACK_NSIS_HELP_LINK "http://www.kitware.com")
set(CPACK_NSIS_URL_INFO_ABOUT "http://pulse.kitware.com")
set(CPACK_NSIS_MENU_LINKS "http://pulse.kitware.com" "Pulse Documentation")
set(CPACK_PACKAGE_EXECUTABLES "PulseExplorer" "Pulse Physiology Explorer" ${CPACK_PACKAGE_EXECUTABLES})

include (CPack)

