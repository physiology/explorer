/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QDockWidget>
#include "controls/DataRequestViewWidget.h"
#include "controls/QPulse.h"

namespace Ui {
  class AnaphylaxisShowcaseWidget;
}

class QAnaphylaxisShowcaseWidget : public QDockWidget, public PulseListener
{
  Q_OBJECT
public:
  QAnaphylaxisShowcaseWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QAnaphylaxisShowcaseWidget();

  bool ConfigurePulse(PhysiologyEngine& pulse, QDataRequestViewWidget& drWidget);

  void AtSteadyState(PhysiologyEngine& pulse) override {}
  void AtSteadyStateUpdateUI() override {} // Main Window will call this to update UI Components
  void ProcessPhysiology(PhysiologyEngine& pulse) override;
  void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override;// Main Window will call this to update UI Components
  void EngineErrorUI() override {};// Main Window will call this to update UI Components

signals:
protected slots:
  void ApplyAirwayObstruction();
  void InjectEpinephrine();

private:
  class Controls;
  Controls* m_Controls;
};