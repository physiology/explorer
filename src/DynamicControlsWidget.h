/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QDockWidget>

#include "controls/QPulse.h"

#include "pulse/cdm/engine/SEAction.h"
#include "pulse/cdm/engine/SEDataRequest.h"
#include "pulse/cdm/scenario/SEScenario.h"

namespace Ui {
  class DynamicControlsWidget;
}

enum class ScenarioInput { None = 0, State, Patient };
class QDynamicControlsWidget : public QDockWidget, public PulseListener
{
  Q_OBJECT
public:
  QDynamicControlsWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QDynamicControlsWidget();

  void Clear();
  void Reset();
  bool ValidPatient();

  QString GetScenarioName();
  QString GetSelectedShowcase();

  bool SetupShowcase(QString name);
  void SetupPulseEditor();
  void LoadScenario(SEScenario& scenario);
  void SaveScenario(SEScenario& scenario);
  void StartEngine();

  void AddTabWidgets(QTabWidget& tabWidget, int tabIdx);

  void AtSteadyState(PhysiologyEngine& pulse) override;
  void AtSteadyStateUpdateUI() override; // Main Window will call this to update UI Components
  void ProcessPhysiology(PhysiologyEngine& pulse) override;
  void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override;// Main Window will call this to update UI Components
  void EngineErrorUI() override {};

public slots:
    void LoadState();
    void ClearPatient();
    void OpenAction(SEAction const& action, SEScalarTime const& pTime);
    void UpdateAction(SEAction const&, SEScalarTime const&);

protected:
  void LoadState(std::string const& engineStateFile);

private:
  class Controls;
  Controls* m_Controls;
  
};