/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "DynamicControlsWidget.h"
#include "ui_DynamicControls.h"
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <thread>

#include "controls/PatientEditorWidget.h"
#include "controls/ConditionsEditorWidget.h"
#include "controls/ActionsEditorWidget.h"
#include "controls/DataRequestViewWidget.h"
#include "controls/ScenarioActionsEditorWidget.h"
#include "controls/EnvironmentWidget.h"
#include "controls/ModifiersWidget.h"
//Showcases
#include "showcases/AnaphylaxisShowcaseWidget.h"
#include "showcases/MultiTraumaShowcaseWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/engine/SEConditionManager.h"
#include "pulse/cdm/engine/SEDataRequestManager.h"
#include "pulse/cdm/engine/SEPatientConfiguration.h"
#include "pulse/cdm/engine/SEEngineTracker.h"
#include "pulse/cdm/patient/SEPatient.h"
#include "pulse/cdm/properties/SEScalarVolume.h"
#include "pulse/cdm/properties/SEScalarVolumePerTime.h"
#include "pulse/cdm/utils/FileUtils.h"
#include "pulse/cdm/utils/TimingProfile.h"

class QDynamicControlsWidget::Controls : public Ui::DynamicControlsWidget
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  ~Controls()
  {
    delete PatientEditor;
    delete ConditionsEditor;
    delete ActionsEditor;
    delete DataRequestEditor;
    delete ScenarioActionsEditor;
    delete EnvironmentEditor;
    delete AnaphylaxisShowcaseWidget;
    delete MultiTraumaShowcaseWidget;
  }
  ScenarioInput                 ScenarioMode = ScenarioInput::None;

  QPulse&                       Pulse;
  QPatientEditorWidget*         PatientEditor = nullptr;
  QConditionsEditorWidget*      ConditionsEditor = nullptr;
  QActionsEditorWidget*         ActionsEditor = nullptr;
  QScenarioActionsEditorWidget* ScenarioActionsEditor = nullptr;
  QDataRequestViewWidget*       DataRequestEditor = nullptr;
  QEnvironmentWidget*           EnvironmentEditor = nullptr;
  QModifiersWidget*             ModifiersEditor = nullptr;

  QAnaphylaxisShowcaseWidget* AnaphylaxisShowcaseWidget = nullptr;
  QMultiTraumaShowcaseWidget* MultiTraumaShowcaseWidget = nullptr;
};

QDynamicControlsWidget::QDynamicControlsWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QDockWidget(parent,flags)
{
  m_Controls = new Controls(qp);
  m_Controls->setupUi(this);

  m_Controls->PatientEditor = new QPatientEditorWidget(m_Controls->Pulse, this);
  m_Controls->PatientEditor->setTitleBarWidget(new QWidget());
  m_Controls->PatientEditor->setVisible(true);
  m_Controls->PatientTab->layout()->addWidget(m_Controls->PatientEditor);

  m_Controls->ConditionsEditor = new QConditionsEditorWidget(m_Controls->Pulse, this);
  m_Controls->ConditionsEditor->setTitleBarWidget(new QWidget());
  m_Controls->ConditionsEditor->setVisible(true);
  m_Controls->ConditionsTab->layout()->addWidget(m_Controls->ConditionsEditor);

  m_Controls->ActionsEditor = new QActionsEditorWidget(m_Controls->Pulse, this);
  m_Controls->ActionsEditor->setTitleBarWidget(new QWidget());
  m_Controls->ActionsEditor->setVisible(true);
  m_Controls->ActionsTab->layout()->addWidget(m_Controls->ActionsEditor);

  m_Controls->DataRequestEditor = new QDataRequestViewWidget(m_Controls->Pulse, this);
  m_Controls->DataRequestEditor->setTitleBarWidget(new QWidget());
  m_Controls->DataRequestEditor->setVisible(true);

  m_Controls->ModifiersEditor = new QModifiersWidget(m_Controls->Pulse, this);
  m_Controls->ModifiersEditor->setTitleBarWidget(new QWidget());
  m_Controls->ModifiersEditor->setVisible(true);

  m_Controls->ScenarioActionsEditor = new QScenarioActionsEditorWidget(m_Controls->Pulse, this);
  m_Controls->ScenarioActionsEditor->setTitleBarWidget(new QWidget());
  m_Controls->ScenarioActionsEditor->setVisible(true);
  connect(m_Controls->ScenarioActionsEditor, SIGNAL(LoadScenario()), parentWidget(), SLOT(LoadScenario()));
  connect(m_Controls->ScenarioActionsEditor, SIGNAL(SaveScenario()), parentWidget(), SLOT(SaveScenario()));
  connect(m_Controls->ScenarioActionsEditor, SIGNAL(OpenAction(SEAction const&, SEScalarTime const&)), SLOT(OpenAction(SEAction const&, SEScalarTime const&)));
  connect(m_Controls->ActionsEditor, SIGNAL(UpdateAction(SEAction const&, SEScalarTime const&)), parentWidget(), SLOT(UpdateAction(SEAction const&, SEScalarTime const&)));

  m_Controls->EnvironmentEditor = new QEnvironmentWidget(m_Controls->Pulse, this);
  m_Controls->EnvironmentEditor->setTitleBarWidget(new QWidget());
  m_Controls->EnvironmentEditor->setVisible(true);

  QPixmap pic("resource/pulse_logo.png");
  m_Controls->Icon->setPixmap(pic);
  m_Controls->ShowcaseWidget->setVisible(true);

  // Add Showcase Widgets

  connect(m_Controls->LoadShowcase, SIGNAL(clicked()), parentWidget(), SLOT(StartShowcase()));

  m_Controls->AnaphylaxisShowcaseWidget = new QAnaphylaxisShowcaseWidget(m_Controls->Pulse, this);
  m_Controls->AnaphylaxisShowcaseWidget->setTitleBarWidget(new QWidget());
  m_Controls->ShowcaseTab->layout()->addWidget(m_Controls->AnaphylaxisShowcaseWidget);
  m_Controls->AnaphylaxisShowcaseWidget->setVisible(false);

  m_Controls->MultiTraumaShowcaseWidget = new QMultiTraumaShowcaseWidget(m_Controls->Pulse, this);
  m_Controls->MultiTraumaShowcaseWidget->setTitleBarWidget(new QWidget());
  m_Controls->ShowcaseTab->layout()->addWidget(m_Controls->MultiTraumaShowcaseWidget);
  m_Controls->MultiTraumaShowcaseWidget->setVisible(false);

}

QDynamicControlsWidget::~QDynamicControlsWidget()
{
  delete m_Controls;
}

void QDynamicControlsWidget::AddTabWidgets(QTabWidget& tabWidget, int tabIdx)
{
  tabWidget.widget(tabIdx++)->layout()->addWidget(m_Controls->DataRequestEditor);
  tabWidget.widget(tabIdx++)->layout()->addWidget(m_Controls->ModifiersEditor);
  tabWidget.widget(tabIdx++)->layout()->addWidget(m_Controls->EnvironmentEditor);
  tabWidget.widget(tabIdx++)->layout()->addWidget(m_Controls->ScenarioActionsEditor);
}

void QDynamicControlsWidget::Clear()
{
  m_Controls->ScenarioMode = ScenarioInput::None;
  m_Controls->PatientEditor->Clear();
  m_Controls->ConditionsEditor->Clear();
  m_Controls->ActionsEditor->Clear();
  m_Controls->DataRequestEditor->Clear();
  m_Controls->ScenarioActionsEditor->Clear();
  m_Controls->EnvironmentEditor->Reset();
  m_Controls->ModifiersEditor->Reset();

  m_Controls->ShowcaseWidget->setVisible(true);
  m_Controls->AnaphylaxisShowcaseWidget->setVisible(false);
  m_Controls->MultiTraumaShowcaseWidget->setVisible(false);
  m_Controls->Pulse.RemoveListener(m_Controls->AnaphylaxisShowcaseWidget);
  m_Controls->Pulse.RemoveListener(m_Controls->MultiTraumaShowcaseWidget);
  m_Controls->DynamicControlsTab->setCurrentWidget(m_Controls->PatientTab);
}

void QDynamicControlsWidget::Reset()
{
  m_Controls->DataRequestEditor->Reset();
  m_Controls->PatientEditor->EnableSetupControls(true);
  m_Controls->ActionsEditor->Clear();
  m_Controls->ActionsEditor->EnableInput(true);
  m_Controls->ActionsEditor->EnableEditControls(true);
  m_Controls->EnvironmentEditor->Reset();
  m_Controls->EnvironmentEditor->EnableControls(true, false);
  m_Controls->ScenarioActionsEditor->EnableControls(true);
  m_Controls->ModifiersEditor->Reset();
  m_Controls->ModifiersEditor->EnableControls(false);

  m_Controls->DynamicControlsTab->setCurrentWidget(m_Controls->PatientTab);

  if (m_Controls->Pulse.GetEngineStateFilename().empty())
  {
    m_Controls->PatientEditor->EnableInput(true);
    m_Controls->ConditionsEditor->EnableInput(true);

    m_Controls->PatientEditor->PatientToControls(m_Controls->Pulse.GetPatientConfiguration().GetPatient());
    m_Controls->ConditionsEditor->ConditionsToControls(m_Controls->Pulse.GetPatientConfiguration().GetConditions());
  }
  else
  {
    LoadState(m_Controls->Pulse.GetEngineStateFilename());
  }
}

QString QDynamicControlsWidget::GetScenarioName()
{
  return m_Controls->ScenarioActionsEditor->GetName();
}

QString QDynamicControlsWidget::GetSelectedShowcase()
{
  return m_Controls->ShowcaseComboBox->currentText();
}


void QDynamicControlsWidget::OpenAction(SEAction const& action, SEScalarTime const& pTime)
{
  m_Controls->DynamicControlsTab->setCurrentWidget(m_Controls->ActionsTab);
  m_Controls->ActionsEditor->OpenAction(action, pTime);
}

void QDynamicControlsWidget::UpdateAction(const SEAction& action, const SEScalarTime& pTime)
{
  m_Controls->ScenarioActionsEditor->UpdateAction(action,pTime);
}

void QDynamicControlsWidget::SetupPulseEditor()
{
  m_Controls->DynamicControlsTab->setCurrentWidget(m_Controls->PatientTab);
  m_Controls->AnaphylaxisShowcaseWidget->setVisible(false);
  m_Controls->MultiTraumaShowcaseWidget->setVisible(false);
  m_Controls->DataRequestEditor->EnableControls(true);
  m_Controls->PatientEditor->Clear();
  m_Controls->ConditionsEditor->Clear();
  m_Controls->ConditionsEditor->EnableInput(true);
  m_Controls->ActionsEditor->EnableInput(true);
  m_Controls->ActionsEditor->EnableEditControls(true);
  m_Controls->EnvironmentEditor->EnableControls(true, false);
  m_Controls->ModifiersEditor->EnableControls(false);
  m_Controls->ScenarioActionsEditor->EnableControls(true);
}

bool QDynamicControlsWidget::ValidPatient()
{
  return m_Controls->PatientEditor->ValidPatient();
}

void QDynamicControlsWidget::ClearPatient()
{
  m_Controls->PatientEditor->ClearPatient();
  m_Controls->Pulse.SetEngineStateFilename("");
  // If you load a state with conditions, then clear the state (via this method)
  // Should the conditions be removed? I am going to say no, but if you think yes
  // Then, uncommnent the next line
  //m_Controls->ConditionsEditor->Reset();
  m_Controls->ConditionsEditor->EnableInput(true);
}

void QDynamicControlsWidget::LoadState()
{
  QString filename = QFileDialog::getOpenFileName(this,
    "Open State", QPulse::GetDataDir()+"/states", "JSON (*.json);;Protobuf Binary (*.pbb)");
  if (filename.isEmpty())
    return;

  // Let's try to make the separate file relative to the working directory
  std::string cwd = GetCurrentWorkingDirectory();
  std::replace(cwd.begin(), cwd.end(), '\\', '/');
  QStringList pieces = filename.split(QString::fromStdString(cwd));
  if (pieces.size() > 1)
    filename = "." + pieces[1];
  LoadState(filename.toStdString());
}

void QDynamicControlsWidget::LoadState(std::string const& engineStateFile)
{
  m_Controls->ConditionsEditor->ClearConditions();
  if (!m_Controls->Pulse.GetEngine().SerializeFromFile(engineStateFile))
  {
    QMessageBox msgBox(this);
    msgBox.setWindowTitle("Error!");
    std::string err = "Unable to load engine state : " + engineStateFile;
    msgBox.setText(QString::fromStdString(err));
    msgBox.exec();
    return;
  }
  // Remove any data requests in the state, we will use ours
  // We could copy the Data Requests out and into our Scenario
  // If we wanted to preserve data requests in the state
  QString esFile = QString::fromStdString(engineStateFile);
  m_Controls->Pulse.GetEngine().GetEngineTracker()->Clear();
  m_Controls->Pulse.SetEngineStateFilename(engineStateFile);
  m_Controls->PatientEditor->PatientToControls(m_Controls->Pulse.GetEngine().GetPatient());
  m_Controls->PatientEditor->EnableInput(false);
  m_Controls->PatientEditor->SetStateFilename(esFile);
  m_Controls->ConditionsEditor->ConditionsToControls(m_Controls->Pulse.GetEngine().GetConditionManager());
  m_Controls->ConditionsEditor->EnableInput(false);
}

void QDynamicControlsWidget::LoadScenario(SEScenario& scenario)
{
  m_Controls->Pulse.GetScenario().Clear();
  m_Controls->ScenarioMode = ScenarioInput::None;

  if (scenario.HasEngineStateFile())
  {
    m_Controls->ScenarioMode = ScenarioInput::State;
    m_Controls->Pulse.SetEngineStateFilename(scenario.GetEngineStateFile());
  }
  else if (scenario.HasPatientConfiguration())
  {
    if (scenario.GetPatientConfiguration().HasPatient())
    {
      m_Controls->ScenarioMode = ScenarioInput::Patient;
      m_Controls->Pulse.GetScenario().GetPatientConfiguration().GetPatient().Copy(scenario.GetPatientConfiguration().GetPatient());
      m_Controls->Pulse.GetScenario().GetPatientConfiguration().GetConditions().Copy(scenario.GetPatientConfiguration().GetConditions(),
                                                                                     m_Controls->Pulse.GetScenario().GetSubstanceManager());
    }
    else if (scenario.GetPatientConfiguration().HasPatientFile())
    {
      std::string patient_file = scenario.GetPatientConfiguration().GetPatientFile();
      if (patient_file.find("/") == std::string::npos)
        patient_file = QPulse::GetDataDir().toStdString()+"/patients/"+patient_file;
      m_Controls->Pulse.SetPatientFilename(patient_file);

      if (!scenario.GetPatientConfiguration().HasConditions())
      {
        // Let's check to see if there is an initial state associated with this patient file
        std::string state_file = patient_file;
        size_t start_pos = state_file.find("patients");
        if (start_pos != std::string::npos)
        {
          state_file.replace(start_pos, 8, "states");
          start_pos = state_file.find(".json");
          state_file.replace(start_pos, 7, "@0s.json");
          // Does this file exist
          if (FILE* file = fopen(state_file.c_str(), "r"))
          {
            fclose(file);
            // Ask user if they would like to use a state
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question(this, "Found Compatible Patient State", "Would you like to load the state rather than the patient?",
              QMessageBox::Yes | QMessageBox::No);
            if (reply == QMessageBox::Yes)
            {
              scenario.SetEngineStateFile(state_file);
              m_Controls->Pulse.LogToGUI("Replacing Patient with associated initial state");
              m_Controls->ScenarioMode = ScenarioInput::State;
            }
          }
        }
      }

      if (m_Controls->ScenarioMode == ScenarioInput::None)
      {// Still not set, not using a state
        if (m_Controls->Pulse.GetScenario().GetPatientConfiguration().GetPatient().SerializeFromFile(patient_file))
        {
          m_Controls->ScenarioMode = ScenarioInput::Patient;
          m_Controls->Pulse.GetScenario().GetPatientConfiguration().GetConditions().Copy(scenario.GetPatientConfiguration().GetConditions(),
                                                                                         m_Controls->Pulse.GetScenario().GetSubstanceManager());
        }
        else
        {
          QMessageBox msgBox(this);
          msgBox.setWindowTitle("Error!");
          msgBox.setText("Unable to load patient file : " + QString::fromStdString(patient_file));
          msgBox.exec();
          m_Controls->ScenarioMode = ScenarioInput::None;
        }
      }
    }
    else
    {
      QMessageBox msgBox(this);
      msgBox.setWindowTitle("Error!");
      msgBox.setText("Scenario patient configuration is invalid");
      msgBox.exec();
      m_Controls->ScenarioMode = ScenarioInput::None;
    }
  }
  else
  {
    QMessageBox msgBox(this);
    msgBox.setWindowTitle("Error!");
    msgBox.setText("Scenario does not contain patient or state information");
    msgBox.exec();
    m_Controls->ScenarioMode = ScenarioInput::None;
  }

  if (m_Controls->ScenarioMode == ScenarioInput::None)
    return;

  if (m_Controls->ScenarioMode == ScenarioInput::State)
  {
    m_Controls->Pulse.SetEngineStateFilename(scenario.GetEngineStateFile());
    if (!m_Controls->Pulse.GetEngine().SerializeFromFile(scenario.GetEngineStateFile()))
    {
      QMessageBox msgBox(this);
      msgBox.setWindowTitle("Error!");
      QString err = "Unable to load engine state : ";// +m_Controls->Pulse.GetScenario().GetEngineStateFile();
      msgBox.setText(err);
      msgBox.exec();
    }
    m_Controls->PatientEditor->PatientToControls(m_Controls->Pulse.GetEngine().GetPatient());
    m_Controls->ConditionsEditor->ConditionsToControls(m_Controls->Pulse.GetEngine().GetConditionManager());
  }
  else
  {
    m_Controls->PatientEditor->PatientToControls(m_Controls->Pulse.GetPatientConfiguration().GetPatient());
    m_Controls->ConditionsEditor->ConditionsToControls(m_Controls->Pulse.GetPatientConfiguration().GetConditions());
  }
  // Populate the GUI with the data in our QPulse Scenario
  m_Controls->DataRequestEditor->LoadScenario(scenario);
  m_Controls->ScenarioActionsEditor->LoadScenario(scenario);
  // TODO Look for an Initial Environment Condition
  m_Controls->PatientEditor->EnableInput(m_Controls->ScenarioMode == ScenarioInput::Patient);
  m_Controls->ConditionsEditor->EnableInput(m_Controls->ScenarioMode == ScenarioInput::Patient);
  // TODO Look for any active modifier actions
}

void QDynamicControlsWidget::SaveScenario(SEScenario& scenario)
{
  if (!m_Controls->Pulse.GetEngineStateFilename().empty())
    scenario.SetEngineStateFile(m_Controls->Pulse.GetEngineStateFilename());
  else
  {
    if (!m_Controls->Pulse.IsRunning())// Only update from GUI if we are setting up the scenario
      m_Controls->PatientEditor->ControlsToPatient(scenario.GetPatientConfiguration().GetPatient());
    else
      scenario.GetPatientConfiguration().GetPatient().Copy(m_Controls->Pulse.GetScenario().GetPatientConfiguration().GetPatient());
    m_Controls->ConditionsEditor->ControlsToConditions(scenario.GetPatientConfiguration().GetConditions(),
                                                       scenario.GetSubstanceManager());
  }
  m_Controls->DataRequestEditor->SaveScenario(scenario);
  m_Controls->ScenarioActionsEditor->SaveScenario(scenario);
}

void QDynamicControlsWidget::StartEngine()
{
  m_Controls->AnaphylaxisShowcaseWidget->setVisible(false);
  m_Controls->MultiTraumaShowcaseWidget->setVisible(false);

  m_Controls->PatientEditor->ControlsToPatient(m_Controls->Pulse.GetPatientConfiguration().GetPatient());
  m_Controls->PatientEditor->EnableInput(false);
  m_Controls->PatientEditor->EnableSetupControls(false);

  m_Controls->ConditionsEditor->ControlsToConditions(m_Controls->Pulse.GetPatientConfiguration().GetConditions(),
                                                     m_Controls->Pulse.GetScenario().GetSubstanceManager());
  m_Controls->ConditionsEditor->EnableInput(false);

  m_Controls->ActionsEditor->EnableInput(false);
  m_Controls->ActionsEditor->EnableEditControls(false);

  m_Controls->EnvironmentEditor->EnableControls(true, false);
  m_Controls->ModifiersEditor->EnableControls(false);

  m_Controls->ScenarioActionsEditor->EnableControls(false);

  m_Controls->DynamicControlsTab->setCurrentWidget(m_Controls->ActionsTab);
}

bool QDynamicControlsWidget::SetupShowcase(QString name)
{
  m_Controls->DynamicControlsTab->setCurrentWidget(m_Controls->ShowcaseTab);
  m_Controls->AnaphylaxisShowcaseWidget->setVisible(false);
  m_Controls->MultiTraumaShowcaseWidget->setVisible(false);
  m_Controls->DataRequestEditor->EnableControls(true);
  m_Controls->ShowcaseWidget->setVisible(false);
  m_Controls->ActionsEditor->EnableInput(false);
  m_Controls->ActionsEditor->EnableEditControls(false);
  m_Controls->ScenarioActionsEditor->EnableControls(false);
  m_Controls->EnvironmentEditor->EnableControls(false, false);
  m_Controls->ModifiersEditor->EnableControls(false);

  if (name == "Anaphylaxis")
  {
#ifdef PARAVIEW  
    m_Controls->GeometryView->RenderSpO2(true);
#endif
    m_Controls->AnaphylaxisShowcaseWidget->setVisible(true);
    if (!m_Controls->AnaphylaxisShowcaseWidget->ConfigurePulse(m_Controls->Pulse.GetEngine(), *m_Controls->DataRequestEditor))
      return false;
    m_Controls->Pulse.RegisterListener(m_Controls->AnaphylaxisShowcaseWidget);
  }
  else if (name == "Multi Trauma")
  {
    m_Controls->MultiTraumaShowcaseWidget->setVisible(true);
    if (!m_Controls->MultiTraumaShowcaseWidget->ConfigurePulse(m_Controls->Pulse.GetEngine(), *m_Controls->DataRequestEditor))
      return false;
    m_Controls->Pulse.RegisterListener(m_Controls->MultiTraumaShowcaseWidget);
  }
  else
    return false;

  m_Controls->PatientEditor->EnableInput(false);
  m_Controls->PatientEditor->ControlsToPatient(m_Controls->Pulse.GetPatientConfiguration().GetPatient());
  m_Controls->PatientEditor->EnableSetupControls(false);

  m_Controls->ConditionsEditor->EnableInput(false);
  m_Controls->ConditionsEditor->ControlsToConditions(m_Controls->Pulse.GetPatientConfiguration().GetConditions(),
    m_Controls->Pulse.GetScenario().GetSubstanceManager());

  m_Controls->ActionsEditor->EnableInput(false);
  m_Controls->ActionsEditor->EnableEditControls(false);

  m_Controls->EnvironmentEditor->EnableControls(true, false);
  m_Controls->ModifiersEditor->EnableControls(false);

  m_Controls->ScenarioActionsEditor->EnableControls(false);
  return true;
}

void QDynamicControlsWidget::AtSteadyState(PhysiologyEngine& pulse)
{
  m_Controls->PatientEditor->AtSteadyState(pulse);
  m_Controls->ConditionsEditor->AtSteadyState(pulse);
  m_Controls->ActionsEditor->AtSteadyState(pulse);
  m_Controls->DataRequestEditor->AtSteadyState(pulse);
  m_Controls->EnvironmentEditor->AtSteadyState(pulse);
  m_Controls->ModifiersEditor->AtSteadyState(pulse);
  m_Controls->ScenarioActionsEditor->AtSteadyState(pulse);

  m_Controls->AnaphylaxisShowcaseWidget->AtSteadyState(pulse);
  m_Controls->MultiTraumaShowcaseWidget->AtSteadyState(pulse);
}
void QDynamicControlsWidget::AtSteadyStateUpdateUI()
{
  m_Controls->PatientEditor->AtSteadyStateUpdateUI();
  m_Controls->ConditionsEditor->AtSteadyStateUpdateUI();
  m_Controls->ActionsEditor->AtSteadyStateUpdateUI();
  m_Controls->DataRequestEditor->AtSteadyStateUpdateUI();
  m_Controls->EnvironmentEditor->AtSteadyStateUpdateUI();
  m_Controls->ModifiersEditor->AtSteadyStateUpdateUI();
  m_Controls->ScenarioActionsEditor->AtSteadyStateUpdateUI();

  m_Controls->AnaphylaxisShowcaseWidget->AtSteadyStateUpdateUI();
  m_Controls->MultiTraumaShowcaseWidget->AtSteadyStateUpdateUI();
}

void QDynamicControlsWidget::ProcessPhysiology(PhysiologyEngine& pulse)
{
  m_Controls->PatientEditor->ProcessPhysiology(pulse);
  m_Controls->ConditionsEditor->ProcessPhysiology(pulse);
  m_Controls->ActionsEditor->ProcessPhysiology(pulse);
  m_Controls->DataRequestEditor->ProcessPhysiology(pulse);
  m_Controls->EnvironmentEditor->ProcessPhysiology(pulse);
  m_Controls->ModifiersEditor->ProcessPhysiology(pulse);
  m_Controls->ScenarioActionsEditor->ProcessPhysiology(pulse);

  m_Controls->AnaphylaxisShowcaseWidget->ProcessPhysiology(pulse);
  m_Controls->MultiTraumaShowcaseWidget->ProcessPhysiology(pulse);
}
void QDynamicControlsWidget::PhysiologyUpdateUI(const std::vector<SEAction const*>& actions)
{
  m_Controls->PatientEditor->PhysiologyUpdateUI(actions);
  m_Controls->ConditionsEditor->PhysiologyUpdateUI(actions);
  m_Controls->ActionsEditor->PhysiologyUpdateUI(actions);
  m_Controls->DataRequestEditor->PhysiologyUpdateUI(actions);
  m_Controls->EnvironmentEditor->PhysiologyUpdateUI(actions);
  m_Controls->ModifiersEditor->PhysiologyUpdateUI(actions);
  m_Controls->ScenarioActionsEditor->PhysiologyUpdateUI(actions);

  m_Controls->AnaphylaxisShowcaseWidget->PhysiologyUpdateUI(actions);
  m_Controls->MultiTraumaShowcaseWidget->PhysiologyUpdateUI(actions);
}
