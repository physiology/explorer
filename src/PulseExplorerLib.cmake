# Get the latest abbreviated commit hash of the working branch
find_package(Git)
if(Git_FOUND)
  execute_process(
    COMMAND ${GIT_EXECUTABLE} log -1 --format=%h
    WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
    OUTPUT_VARIABLE export_git_hash
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
else()
  set(export_git_hash "unknown")
endif()

configure_file(${CMAKE_SOURCE_DIR}/cmake/PulseExplorerBuildInformation.cpp.in
               ${CMAKE_BINARY_DIR}/src/PulseExplorerBuildInformation.cpp)

explorer_add_library(PulseExplorerWidgets
  MOC_HEADERS
    controls/ActionsEditorWidget.h
    controls/CollapsableWidget.h
    controls/ConditionsEditorWidget.h
    controls/DataRequestWidget.h
    controls/DataRequestViewWidget.h
    controls/EnvironmentWidget.h
    #controls/Icon.h
    controls/LabeledComboBox.h
    controls/LabeledDialWidget.h
    controls/ModifiersWidget.h
    controls/NumberWidget.h
    #controls/Palette.h
    controls/PatientEditorWidget.h
    controls/PlotSetWidget.h
    controls/PulsePainterPlot.h
    controls/QwtPulsePlot.h
    controls/QwtPulsePlotLoop.h
    controls/QPulse.h
    controls/ScalarWidget.h
    controls/ScenarioActionsEditorWidget.h
    controls/ScenarioSaveDialog.h
    controls/TimeWidget.h
    controls/VitalsMonitorWidget.h
    controls/VentilatorWidget.h
    controls/VentilatorHysteresisWidget.h
    #controls/WidgetStyle.h
    controls/actions/ActionWidget.h
    controls/actions/AcuteRespiratoryDistressSyndromeExacerbationWidget.h
    controls/actions/AcuteStressWidget.h
    controls/actions/AirwayObstructionWidget.h
    controls/actions/ArrhythmiaWidget.h
    controls/actions/AsthmaAttackWidget.h
    controls/actions/BagValveMaskAutomatedWidget.h
    controls/actions/BagValveMaskSqueezeWidget.h
    controls/actions/BrainInjuryWidget.h
    controls/actions/BronchoconstrictionWidget.h
    controls/actions/ChestCompressionAutomatedWidget.h
    controls/actions/ChestCompressionWidget.h
    controls/actions/ChestOcclusiveDressingWidget.h
    controls/actions/ChronicObstructivePulmonaryDiseaseExacerbationWidget.h
    controls/actions/DyspneaWidget.h
    controls/actions/ExerciseWidget.h
    controls/actions/HemorrhageWidget.h
    controls/actions/HemothoraxWidget.h
    controls/actions/ImpairedAlveolarExchangeExacerbationWidget.h
    controls/actions/IntubationWidget.h
    controls/actions/MechanicalVentilatorContinuousPositiveAirwayPressureWidget.h    
    controls/actions/MechanicalVentilatorHoldWidget.h
    controls/actions/MechanicalVentilatorLeakWidget.h
    controls/actions/MechanicalVentilatorPressureControlWidget.h
    controls/actions/MechanicalVentilatorVolumeControlWidget.h
    controls/actions/NeedleDecompressionWidget.h
    controls/actions/PericardialEffusionWidget.h
    controls/actions/PneumoniaExacerbationWidget.h
    controls/actions/PulmonaryShuntExacerbationWidget.h
    controls/actions/RespiratoryFatigueWidget.h
    controls/actions/SerializeStateWidget.h
    controls/actions/SubstanceBolusWidget.h
    controls/actions/SubstanceCompoundInfusionWidget.h
    controls/actions/SubstanceInfusionWidget.h
    controls/actions/SupplementalOxygenWidget.h
    controls/actions/TensionPneumothoraxWidget.h
    controls/actions/TubeThoracostomyWidget.h
    controls/actions/UrinateWidget.h
    controls/conditions/ConditionWidget.h
    controls/conditions/AcuteRespiratoryDistressSyndromeWidget.h
    controls/conditions/ChronicAnemiaWidget.h
    controls/conditions/ChronicObstructivePulmonaryDiseaseWidget.h
    controls/conditions/ChronicPericardialEffusionWidget.h
    controls/conditions/ChronicRenalStenosisWidget.h
    controls/conditions/ChronicVentricularSystolicDysfunctionWidget.h
    controls/conditions/ImpairedAlveolarExchangeWidget.h
    controls/conditions/PneumoniaWidget.h
    controls/conditions/PulmonaryFibrosisWidget.h
    controls/conditions/PulmonaryShuntWidget.h
  PUBLIC_HEADERS
    controls/PulseData.h
    controls/ScalarQuantityWidget.inl
    controls/ScalarQuantityWidget.h
  CPP_FILES
    controls/ActionsEditorWidget.cxx
    controls/CollapsableWidget.cxx
    controls/ConditionsEditorWidget.cxx
    controls/DataRequestViewWidget.cxx
    controls/DataRequestWidget.cxx
    controls/EnvironmentWidget.cxx
    #controls/Icon.cpp
    controls/LabeledComboBox.cxx
    controls/LabeledDialWidget.cxx
    controls/ModifiersWidget.cxx
    controls/NumberWidget.cxx
    #controls/Palette.cpp
    controls/PatientEditorWidget.cxx
    controls/PlotSetWidget.cxx
    controls/PulsePainterPlot.cxx
    controls/QPulse.cxx
    controls/QwtPulsePlot.cxx
    controls/QwtPulsePlotLoop.cxx
    controls/ScalarWidget.cxx
    controls/ScenarioActionsEditorWidget.cxx
    controls/ScenarioSaveDialog.cxx
    controls/TimeWidget.cxx
    #controls/WidgetStyle.cpp
    controls/VentilatorWidget.cxx
    controls/VentilatorHysteresisWidget.cxx
    controls/VitalsMonitorWidget.cxx
    controls/actions/ActionWidget.cxx
    controls/actions/AcuteRespiratoryDistressSyndromeExacerbationWidget.cxx
    controls/actions/AcuteStressWidget.cxx
    controls/actions/AirwayObstructionWidget.cxx
    controls/actions/ArrhythmiaWidget.cxx
    controls/actions/AsthmaAttackWidget.cxx
    controls/actions/BagValveMaskAutomatedWidget.cxx
    controls/actions/BagValveMaskSqueezeWidget.cxx
    controls/actions/BrainInjuryWidget.cxx
    controls/actions/BronchoconstrictionWidget.cxx
    controls/actions/ChestCompressionAutomatedWidget.cxx
    controls/actions/ChestCompressionWidget.cxx
    controls/actions/ChestOcclusiveDressingWidget.cxx
    controls/actions/ChronicObstructivePulmonaryDiseaseExacerbationWidget.cxx
    controls/actions/DyspneaWidget.cxx
    controls/actions/ExerciseWidget.cxx
    controls/actions/HemorrhageWidget.cxx
    controls/actions/HemothoraxWidget.cxx
    controls/actions/ImpairedAlveolarExchangeExacerbationWidget.cxx
    controls/actions/IntubationWidget.cxx
    controls/actions/MechanicalVentilatorContinuousPositiveAirwayPressureWidget.cxx
    controls/actions/MechanicalVentilatorHoldWidget.cxx
    controls/actions/MechanicalVentilatorLeakWidget.cxx
    controls/actions/MechanicalVentilatorPressureControlWidget.cxx
    controls/actions/MechanicalVentilatorVolumeControlWidget.cxx
    controls/actions/NeedleDecompressionWidget.cxx
    controls/actions/PericardialEffusionWidget.cxx
    controls/actions/PneumoniaExacerbationWidget.cxx
    controls/actions/PulmonaryShuntExacerbationWidget.cxx
    controls/actions/RespiratoryFatigueWidget.cxx
    controls/actions/SerializeStateWidget.cxx
    controls/actions/SubstanceBolusWidget.cxx
    controls/actions/SubstanceCompoundInfusionWidget.cxx
    controls/actions/SubstanceInfusionWidget.cxx
    controls/actions/SupplementalOxygenWidget.cxx
    controls/actions/TensionPneumothoraxWidget.cxx
    controls/actions/TubeThoracostomyWidget.cxx
    controls/actions/UrinateWidget.cxx
    controls/conditions/ConditionWidget.cxx
    controls/conditions/AcuteRespiratoryDistressSyndromeWidget.cxx
    controls/conditions/ChronicAnemiaWidget.cxx
    controls/conditions/ChronicObstructivePulmonaryDiseaseWidget.cxx
    controls/conditions/ChronicPericardialEffusionWidget.cxx
    controls/conditions/ChronicRenalStenosisWidget.cxx
    controls/conditions/ChronicVentricularSystolicDysfunctionWidget.cxx
    controls/conditions/ImpairedAlveolarExchangeWidget.cxx
    controls/conditions/PneumoniaWidget.cxx
    controls/conditions/PulmonaryFibrosisWidget.cxx
    controls/conditions/PulmonaryShuntWidget.cxx
  CONFIG_CPP_FILES
    PulseExplorerBuildInformation.cpp
  UI_FILES
    ui/ActionsEditor.ui
    ui/Action.ui
    ui/ConditionsEditor.ui
    ui/Condition.ui
    ui/DataRequest.ui
    ui/DataRequestView.ui
    ui/Environment.ui
    ui/LabeledDialWidget.ui
    ui/Modifiers.ui
    ui/NumberWidget.ui
    ui/PatientEditor.ui
    ui/PlotSet.ui
    ui/ScenarioActionsEditor.ui
    ui/ScenarioSave.ui
    ui/Time.ui
    ui/Ventilator.ui
    ui/VentilatorHysteresis.ui
    ui/VitalsMonitor.ui
  QT_RESOURCES
    resources.qrc
  PUBLIC_LINK_LIBRARIES
    Pulse::Pulse
    Qwt::Qwt
    ${QT_LIBRARIES}
)


if( ${${PROJECT_NAME}_LIBRARY_ONLY} OR ${${PROJECT_NAME}_CREATE_EXPORT})
  set(${PROJECT_NAME}_INSTALL_FOLDER "pulse/explorer")
  #-----------------------------------------------------------------------------
  # This variable controls the prefix used to generate the following files:
  #  ${PROJECT_NAME}ConfigVersion.cmake
  #  ${PROJECT_NAME}Config.cmake
  #  ${PROJECT_NAME}Targets.cmake
  # and it also used to initialize ${PROJECT_NAME}_INSTALL_CONFIG_DIR value.
  set(export_config_name ${PROJECT_NAME})
  set(${PROJECT_NAME}_INSTALL_CONFIG_DIR "lib/cmake/${${PROJECT_NAME}_INSTALL_FOLDER}")
  file(MAKE_DIRECTORY ${CMAKE_INSTALL_PREFIX}/${${PROJECT_NAME}_INSTALL_CONFIG_DIR})
  message(STATUS "Exporting to ${CMAKE_INSTALL_PREFIX}/${${PROJECT_NAME}_INSTALL_CONFIG_DIR}")
  #------------------------------------------------------------------------------
  # Configure ${PROJECT_NAME}ConfigVersion.cmake common to build and install tree
  include(CMakePackageConfigHelpers)
  set(config_version_file ${PROJECT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake)
  write_basic_package_version_file(
    ${config_version_file}
    VERSION "${${PROJECT_NAME}_VERSION}"
    COMPATIBILITY ExactVersion
    )
  #------------------------------------------------------------------------------
  # Export '${PROJECT_NAME}Targets.cmake' for a build tree
  export(
    EXPORT ${PROJECT_NAME}_EXPORTS
    NAMESPACE PulseExplorer::
    FILE "${CMAKE_BINARY_DIR}/${PROJECT_NAME}Targets.cmake"
    )

  # Configure '${PROJECT_NAME}Config.cmake' for a build tree
  set(build_config ${CMAKE_BINARY_DIR}/${PROJECT_NAME}Config.cmake)
  configure_package_config_file(
    ${CMAKE_SOURCE_DIR}/cmake/${PROJECT_NAME}ConfigBuild.cmake.in
    ${build_config}
    INSTALL_DESTINATION "${PROJECT_BINARY_DIR}"
    )

  file(GLOB _modules ${PROJECT_SOURCE_DIR}/cmake/modules/*.cmake)
  list(APPEND _modules ${PROJECT_SOURCE_DIR}/cmake/find.cmake)

  #file(MAKE_DIRECTORY "${PROJECT_BINARY_DIR}/modules")
  file(COPY ${_modules}
    DESTINATION "${PROJECT_BINARY_DIR}/modules"
    )

  # Only install libs/headers if we are only building the library
  # If you need these and the app, use the build directory package
  #------------------------------------------------------------------------------
  # Export '${PROJECT_NAME}Targets.cmake' for an install tree
  install(
    EXPORT ${PROJECT_NAME}_EXPORTS
    NAMESPACE PulseExplorer::
    FILE ${PROJECT_NAME}Targets.cmake
    DESTINATION ${${PROJECT_NAME}_INSTALL_CONFIG_DIR}
    )

  # config files
  set(install_config ${PROJECT_BINARY_DIR}/CMakeFiles/${PROJECT_NAME}Config.cmake)
  configure_package_config_file(
    ${CMAKE_SOURCE_DIR}/cmake/${PROJECT_NAME}ConfigInstall.cmake.in
    ${install_config}
    INSTALL_DESTINATION ${${PROJECT_NAME}_INSTALL_CONFIG_DIR}
    )

  install(
    FILES ${config_version_file} ${install_config}
    DESTINATION "${${PROJECT_NAME}_INSTALL_CONFIG_DIR}"
    )
    
  # module files
  install(
    FILES ${_modules}
    DESTINATION "${${PROJECT_NAME}_INSTALL_CONFIG_DIR}/modules"
    )
    
endif()
