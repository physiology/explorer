/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/

#include "MainExplorerWindow.h"
//#include "controls/Palette.h"
//#include "controls/WidgetStyle.h"

#include <QApplication>
#include <QDir>
//#include <QStyleFactory>


#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
int main(int argc, char *argv[])
{
  QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QApplication::setApplicationName("Pulse Explorer");
  QApplication::setOrganizationName("Kitware");
  QApplication::setOrganizationDomain("kitware.com");
  QApplication::setApplicationVersion("3.2");
  QApplication app(argc, argv);

#ifdef Q_OS_MAC
  // If we are not debugging, set current dir to the .app
  if(!QCoreApplication::applicationDirPath().contains("Innerbuild"))
    QDir::setCurrent(QCoreApplication::applicationDirPath() + "/../Pulse");
  //app.setStyle(QStyleFactory::create("macintosh"));
#endif

  //app.setPalette(Palette{ Palette::Workspace });
  //auto* const s = new WidgetStyle{ &app };
  //app.setStyle(s);

  MainExplorerWindow w;
  w.show();
  return app.exec();
}
