/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/
#pragma once

#include <QWidget>

#include <memory>

class PulsePainterPlot : public QWidget
{
  Q_OBJECT

public:

  /// Note due to historical reasons, not all UpdateMode work with all ScaleModes
  enum class UpdateMode
  {
    Timeline,   // Original mode, X-Axis is time data
    InPlace,    // Update with index, "overwrite" old value
    RawData     // Just the data X/Y, no semantics
  };

  enum class ScaleMode
  {
    Fixed,      // Take the user defined Min/Max and scale to that
    Adaptive,   // grow if the data exceeds the user given size
    Expanding,   // "Rememeber" the extremese
  };

  PulsePainterPlot(QWidget* parent = nullptr);
  virtual ~PulsePainterPlot();

  void setMaxPoints(int count);
  void setYRange(double min, double max);
  QPair<double, double> getYRange();

  void setXRange(double min, double max);

  void setPadding(double amount = 0.05);

  void setScaleMode(ScaleMode mode);
  void setUpdateMode(UpdateMode mode);

  void enableXZero(bool doDraw);
  void enableYZero(bool doDraw);

public slots:
  void append(double time, double value);
  void clear();

protected:
  void paintEvent(QPaintEvent*) override;

private:
  class Private;
  std::unique_ptr<Private> d;
};
