/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QDockWidget>

#include "QPulse.h"
#include "controls/CollapsableWidget.h"

#include "pulse/cdm/PhysiologyEngine.h"
class SEAction;
class SEActionManager;

namespace Ui {
  class ActionsEditorWidget;
}

class QActionsEditorWidget : public QDockWidget, public PulseListener
{
  Q_OBJECT
public:
  QActionsEditorWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QActionsEditorWidget();

  void Clear();
  void EnableInput(bool b);
  void EnableEditControls(bool b);
  void OpenAction(SEAction const&, SEScalarTime const&);

  void AtSteadyState(PhysiologyEngine& pulse) override;
  void AtSteadyStateUpdateUI() override; // Main Window will call this to update UI Components
  void ProcessPhysiology(PhysiologyEngine& pulse) override;
  void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override; // Main Window will call this to update UI Components
  void EngineErrorUI() override {};// Main Window will call this to update UI Components
signals:
  void UpdateAction(SEAction const&, SEScalarTime const&);

protected:

private:
  class Controls;
  Controls* m_Controls;
  
};