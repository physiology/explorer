/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "LabeledComboBox.h"

QLabeledComboBox::QLabeledComboBox(QWidget* parent, QString label, int maxWidth) : QWidget(parent)
{
  Layout = new QHBoxLayout();
  Layout->setSpacing(6);
  Layout->setContentsMargins(9, 9, 9, 9);
  QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  sizePolicy.setHorizontalStretch(0);
  sizePolicy.setVerticalStretch(0);
  Label = new QLabel(label, this);
  sizePolicy.setHeightForWidth(Label->sizePolicy().hasHeightForWidth());
  Label->setSizePolicy(sizePolicy);
  Layout->addWidget(Label);
  Spacer = new QSpacerItem(40, 30, QSizePolicy::Expanding, QSizePolicy::Maximum);
  Layout->addItem(Spacer);
  ComboBox = new QComboBox(this);
  ComboBox->setMinimumSize(QSize(maxWidth, 0));
  ComboBox->setMaximumSize(QSize(maxWidth, 20));
  Layout->addWidget(ComboBox);
  setLayout(Layout);
}

QLabeledComboBox::QLabeledComboBox(QWidget *parent, QString label, std::set<QString> options, int maxWidth) : QLabeledComboBox(parent, label, maxWidth)
{
  for(auto v : options)
    ComboBox->addItem(v);
}

QLabeledComboBox::QLabeledComboBox(QWidget* parent, QString label, std::vector<QString> options, int maxWidth) : QLabeledComboBox(parent, label, maxWidth)
{
  for (auto v : options)
    ComboBox->addItem(v);
}

void QLabeledComboBox::Reset()
{
}

void QLabeledComboBox::SetEnabled(bool b)
{
  ComboBox->setEnabled(b);
}

QComboBox* QLabeledComboBox::GetComboBox()
{
  return ComboBox;
}

void QLabeledComboBox::SetCurrentIndex(int index)
{
    ComboBox->setCurrentIndex(index);
}

int QLabeledComboBox::GetIndex()
{
  return ComboBox->currentIndex();
}
std::string QLabeledComboBox::GetText()
{
  return ComboBox->currentText().toStdString();
}
