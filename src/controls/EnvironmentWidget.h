/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QDockWidget>
#include "QPulse.h"

class SEEnvironmentalConditions;

namespace Ui {
  class EnvironmentWidget;
}

class QEnvironmentWidget : public QDockWidget, public PulseListener
{
  Q_OBJECT
public:
  QEnvironmentWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QEnvironmentWidget();

  void Reset();
  //SECondition& GetInitialEnvironmentConditions();

  void EnableControls(bool conditions, bool actions);

  void AtSteadyState(PhysiologyEngine& pulse) override;
  void AtSteadyStateUpdateUI() override;// Main Window will call this to update UI Components
  void ProcessPhysiology(PhysiologyEngine& pulse) override;
  void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override;// Main Window will call this to update UI Components
  void EngineErrorUI() override {};// Main Window will call this to update UI Components

//signals:
protected slots:
  void EnableCooling();
  void EnableCoolingSurfaceArea();
  void EnableCoolingSurfaceAreaFraction();
  void EnableHeating();
  void EnableHeatingSurfaceArea();
  void EnableHeatingSurfaceAreaFraction();
  void EnableAppliedTemp();
  void EnableAppliedTempSurfaceArea();
  void EnableAppliedSurfaceAreaFraction();
  void LoadEnvironmentFile();
  void LoadEnvironmentFile(const std::string& fileName);
  void SaveEnvironmentFile();
  void SaveEnvironmentFile(const std::string& fileName);
  

  void ControlsToEnvironment();
  void EnvironmentToControls();

private:
  class Controls;
  Controls* m_Controls;
};