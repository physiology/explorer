/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "ModifiersWidget.h"
#include "ui_Modifiers.h"
#include <QLayout>
#include <QGroupBox>
#include <QFileDialog>
#include <QMessageBox>

#include "controls/AddPopup.h"
#include "controls/ModifiersWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/engine/PulseEngine.h"
#include "pulse/cdm/engine/SEActionManager.h"
#include "pulse/cdm/engine/SEPatientActionCollection.h"
#include "pulse/cdm/patient/actions/SECardiovascularMechanicsModification.h"
#include "pulse/cdm/system/physiology/SECardiovascularMechanicsModifiers.h"
#include "pulse/cdm/patient/actions/SERespiratoryMechanicsModification.h"
#include "pulse/cdm/system/physiology/SERespiratoryMechanicsModifiers.h"
#include "pulse/cdm/properties/SEScalarUnsigned.h"
#include "pulse/cdm/properties/SEScalarVolume.h"
#include "pulse/cdm/utils/FileUtils.h"

class QModifiersWidget::Controls : public Ui::ModifiersWidget
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}

  QPulse&                                        Pulse;
  std::string type = "Modifiers";
  // Cardiovascular Modifiers
  QScalarWidget* ArterialComplianceMultiplier;
  QScalarWidget* ArterialResistanceMultiplier;
  QScalarWidget* PulmonaryComplianceMultiplier;
  QScalarWidget* PulmonaryResistanceMultiplier;
  QScalarWidget* SystemicResistanceMultiplier;
  QScalarWidget* SystemicComplianceMultiplier;
  QScalarWidget* VenousComplianceMultiplier;
  QScalarWidget* VenousResistanceMultiplier;
  QScalarWidget* HeartRateMultiplier;
  QScalarWidget* StrokeVolumeMultiplier;
  bool SendCardiovascularAction = false;
  bool PullCardiovascularModifiers = false;
  bool PushCardiovascularModifiersToControls = false;
  SECardiovascularMechanicsModification* CardioModifiers;

  // Respiratory Modifiers
  QScalarWidget* LeftComplianceMultiplier;
  QScalarWidget* RightComplianceMultiplier;
  QScalarWidget* LeftExpiratoryResistanceMultiplier;
  QScalarWidget* LeftInspiratoryResistanceMultiplier;
  QScalarWidget* RightExpiratoryResistanceMultiplier;
  QScalarWidget* RightInspiratoryResistanceMultiplier;
  QScalarWidget* UpperExpiratoryResistanceMultiplier;
  QScalarWidget* UpperInspiratoryResistanceMultiplier;
  QScalarWidget* RespirationRateMultiplier;
  QScalarWidget* TidalVolumeMultiplier;
  QScalarQuantityWidget<VolumeUnit>* LeftLungVolumeIncrement;
  QScalarQuantityWidget<VolumeUnit>* RightLungVolumeIncrement;
  bool SendRespiratoryAction = false;
  bool PullRespiratoryModifiers = false;
  bool PushRespiratoryModifiersToControls = false;
  SERespiratoryMechanicsModification* RespModifiers;
};

QModifiersWidget::QModifiersWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QDockWidget(parent,flags)
{
  m_Controls = new Controls(qp);
  m_Controls->setupUi(this);

  ////////////////////////////////////
  // Setup Cardiovascular Modifiers //
  ////////////////////////////////////
  m_Controls->SendCardiovascularAction = false;
  m_Controls->PullCardiovascularModifiers = false;
  m_Controls->PushCardiovascularModifiersToControls = false;
  m_Controls->CardioModifiers = new SECardiovascularMechanicsModification();

  m_Controls->ArterialComplianceMultiplier = new QScalarWidget("Arterial Compliance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->ArterialComplianceMultiplier->EnableInput(false);
  m_Controls->CardiovascularProperties->layout()->addWidget(m_Controls->ArterialComplianceMultiplier);

  m_Controls->ArterialResistanceMultiplier = new QScalarWidget("Arterial Resistance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->ArterialResistanceMultiplier->EnableInput(false);
  m_Controls->CardiovascularProperties->layout()->addWidget(m_Controls->ArterialResistanceMultiplier);

  m_Controls->PulmonaryComplianceMultiplier = new QScalarWidget("Pulmonary Compliance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->PulmonaryComplianceMultiplier->EnableInput(false);
  m_Controls->CardiovascularProperties->layout()->addWidget(m_Controls->PulmonaryComplianceMultiplier);

  m_Controls->PulmonaryResistanceMultiplier = new QScalarWidget("Pulmonary Resistance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->PulmonaryResistanceMultiplier->EnableInput(false);
  m_Controls->CardiovascularProperties->layout()->addWidget(m_Controls->PulmonaryResistanceMultiplier);

  m_Controls->SystemicResistanceMultiplier = new QScalarWidget("Systemic Resistance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->SystemicResistanceMultiplier->EnableInput(false);
  m_Controls->CardiovascularProperties->layout()->addWidget(m_Controls->SystemicResistanceMultiplier);

  m_Controls->SystemicComplianceMultiplier = new QScalarWidget("Systemic Compliance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->SystemicComplianceMultiplier->EnableInput(false);
  m_Controls->CardiovascularProperties->layout()->addWidget(m_Controls->SystemicComplianceMultiplier);

  m_Controls->VenousComplianceMultiplier = new QScalarWidget("Venous Compliance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->VenousComplianceMultiplier->EnableInput(false);
  m_Controls->CardiovascularProperties->layout()->addWidget(m_Controls->VenousComplianceMultiplier);

  m_Controls->VenousResistanceMultiplier = new QScalarWidget("Venous Resistance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->VenousResistanceMultiplier->EnableInput(false);
  m_Controls->CardiovascularProperties->layout()->addWidget(m_Controls->VenousResistanceMultiplier);

  m_Controls->HeartRateMultiplier = new QScalarWidget("Heart Rate Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->HeartRateMultiplier->EnableInput(false);
  m_Controls->CardiovascularProperties->layout()->addWidget(m_Controls->HeartRateMultiplier);

  m_Controls->StrokeVolumeMultiplier = new QScalarWidget("Stroke Volume Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->StrokeVolumeMultiplier->EnableInput(false);
  m_Controls->CardiovascularProperties->layout()->addWidget(m_Controls->StrokeVolumeMultiplier);

  /////////////////////////////////
  // Setup Respiratory Modifiers //
  /////////////////////////////////
  m_Controls->SendRespiratoryAction = false;
  m_Controls->PullRespiratoryModifiers = false;
  m_Controls->PushRespiratoryModifiersToControls = false;
  m_Controls->RespModifiers = new SERespiratoryMechanicsModification();

  m_Controls->LeftComplianceMultiplier = new QScalarWidget("Left Compliance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->LeftComplianceMultiplier->EnableInput(false);
  m_Controls->RespiratoryProperties->layout()->addWidget(m_Controls->LeftComplianceMultiplier);

  m_Controls->RightComplianceMultiplier = new QScalarWidget("Right Compliance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->RightComplianceMultiplier->EnableInput(false);
  m_Controls->RespiratoryProperties->layout()->addWidget(m_Controls->RightComplianceMultiplier);

  m_Controls->LeftExpiratoryResistanceMultiplier = new QScalarWidget("Left Expiratory Resistance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->LeftExpiratoryResistanceMultiplier->EnableInput(false);
  m_Controls->RespiratoryProperties->layout()->addWidget(m_Controls->LeftExpiratoryResistanceMultiplier);

  m_Controls->LeftInspiratoryResistanceMultiplier = new QScalarWidget("Left Inspiratory Resistance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->LeftInspiratoryResistanceMultiplier->EnableInput(false);
  m_Controls->RespiratoryProperties->layout()->addWidget(m_Controls->LeftInspiratoryResistanceMultiplier);

  m_Controls->RightExpiratoryResistanceMultiplier = new QScalarWidget("Right Expiratory Resistance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->RightExpiratoryResistanceMultiplier->EnableInput(false);
  m_Controls->RespiratoryProperties->layout()->addWidget(m_Controls->RightExpiratoryResistanceMultiplier);

  m_Controls->RightInspiratoryResistanceMultiplier = new QScalarWidget("Right Inspiratory Resistance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->RightInspiratoryResistanceMultiplier->EnableInput(false);
  m_Controls->RespiratoryProperties->layout()->addWidget(m_Controls->RightInspiratoryResistanceMultiplier);

  m_Controls->UpperExpiratoryResistanceMultiplier = new QScalarWidget("Upper Expiratory Resistance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->UpperExpiratoryResistanceMultiplier->EnableInput(false);
  m_Controls->RespiratoryProperties->layout()->addWidget(m_Controls->UpperExpiratoryResistanceMultiplier);

  m_Controls->UpperInspiratoryResistanceMultiplier = new QScalarWidget("Upper Inspiratory Resistance Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->UpperInspiratoryResistanceMultiplier->EnableInput(false);
  m_Controls->RespiratoryProperties->layout()->addWidget(m_Controls->UpperInspiratoryResistanceMultiplier);

  m_Controls->RespirationRateMultiplier = new QScalarWidget("Respiration Rate Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->RespirationRateMultiplier->EnableInput(false);
  m_Controls->RespiratoryProperties->layout()->addWidget(m_Controls->RespirationRateMultiplier);

  m_Controls->TidalVolumeMultiplier = new QScalarWidget("Tidal Volume Multiplier", 1.0, 10.0, .1, ScalarOptionWidget::Check, this);
  m_Controls->TidalVolumeMultiplier->EnableInput(false);
  m_Controls->RespiratoryProperties->layout()->addWidget(m_Controls->TidalVolumeMultiplier);

  /////////////////////////////////
  // Setup Lung Volume Modifiers //
  /////////////////////////////////

  m_Controls->LeftLungVolumeIncrement = new QScalarQuantityWidget<VolumeUnit>("Left Lung Volume Increment", 0.0, 100.0, 1.0, VolumeUnit::mL, ScalarOptionWidget::Check, this);
  m_Controls->LeftLungVolumeIncrement->EnableInput(false);
  m_Controls->RespiratoryProperties->layout()->addWidget(m_Controls->LeftLungVolumeIncrement);

  m_Controls->RightLungVolumeIncrement = new QScalarQuantityWidget<VolumeUnit>("Right Lung Volume Increment", 0.0, 100.0, 1.0, VolumeUnit::mL, ScalarOptionWidget::Check, this);
  m_Controls->RightLungVolumeIncrement->EnableInput(false);
  m_Controls->RespiratoryProperties->layout()->addWidget(m_Controls->RightLungVolumeIncrement);


  connect(m_Controls->ApplyCardiovascular, SIGNAL(clicked()), this, SLOT(CardiovascularControlsToPulse()));
  connect(m_Controls->ResetCardiovascular, SIGNAL(clicked()), this, SLOT(PulseToCardiovascularControls()));
  connect(m_Controls->ApplyRespiratory, SIGNAL(clicked()), this, SLOT(RespiratoryControlsToPulse()));
  connect(m_Controls->ResetRespiratory, SIGNAL(clicked()), this, SLOT(PulseToRespiratoryControls()));
  Reset();
}

QModifiersWidget::~QModifiersWidget()
{
  // TODO Clean up all the widgets
  m_Controls->Modifiers->close();
  delete m_Controls;
}

void QModifiersWidget::Reset()
{
  EnableControls(false);
  PulseToCardiovascularControls();
  PulseToRespiratoryControls();
}

void QModifiersWidget::EnableControls(bool b)
{
  m_Controls->ArterialComplianceMultiplier->EnableInput(b,false);
  m_Controls->ArterialResistanceMultiplier->EnableInput(b, false);
  m_Controls->PulmonaryComplianceMultiplier->EnableInput(b, false);
  m_Controls->PulmonaryResistanceMultiplier->EnableInput(b, false);
  m_Controls->SystemicResistanceMultiplier->EnableInput(b, false);
  m_Controls->SystemicComplianceMultiplier->EnableInput(b, false);
  m_Controls->VenousComplianceMultiplier->EnableInput(b, false);
  m_Controls->VenousResistanceMultiplier->EnableInput(b, false);
  m_Controls->HeartRateMultiplier->EnableInput(b, false);
  m_Controls->StrokeVolumeMultiplier->EnableInput(b, false);
  m_Controls->ApplyCardiovascular->setEnabled(b);
  m_Controls->ResetCardiovascular->setEnabled(b);

  m_Controls->LeftComplianceMultiplier->EnableInput(b, false);
  m_Controls->RightComplianceMultiplier->EnableInput(b, false);
  m_Controls->LeftExpiratoryResistanceMultiplier->EnableInput(b, false);
  m_Controls->LeftInspiratoryResistanceMultiplier->EnableInput(b, false);
  m_Controls->RightExpiratoryResistanceMultiplier->EnableInput(b, false);
  m_Controls->RightInspiratoryResistanceMultiplier->EnableInput(b, false);
  m_Controls->UpperExpiratoryResistanceMultiplier->EnableInput(b, false);
  m_Controls->UpperInspiratoryResistanceMultiplier->EnableInput(b, false);
  m_Controls->RespirationRateMultiplier->EnableInput(b, false);
  m_Controls->TidalVolumeMultiplier->EnableInput(b, false);
  m_Controls->LeftLungVolumeIncrement->EnableInput(b, false);
  m_Controls->RightLungVolumeIncrement->EnableInput(b, false);
  m_Controls->ApplyRespiratory->setEnabled(b);
  m_Controls->ResetRespiratory->setEnabled(b);
}

void QModifiersWidget::ResetControls()
{
  m_Controls->ArterialComplianceMultiplier->Reset();
  m_Controls->ArterialResistanceMultiplier->Reset();
  m_Controls->PulmonaryComplianceMultiplier->Reset();
  m_Controls->PulmonaryResistanceMultiplier->Reset();
  m_Controls->SystemicResistanceMultiplier->Reset();
  m_Controls->SystemicComplianceMultiplier->Reset();
  m_Controls->VenousComplianceMultiplier->Reset();
  m_Controls->VenousResistanceMultiplier->Reset();
  m_Controls->HeartRateMultiplier->Reset();
  m_Controls->StrokeVolumeMultiplier->Reset();

  m_Controls->LeftComplianceMultiplier->Reset();
  m_Controls->RightComplianceMultiplier->Reset();
  m_Controls->LeftExpiratoryResistanceMultiplier->Reset();
  m_Controls->LeftInspiratoryResistanceMultiplier->Reset();
  m_Controls->RightExpiratoryResistanceMultiplier->Reset();
  m_Controls->RightInspiratoryResistanceMultiplier->Reset();
  m_Controls->UpperExpiratoryResistanceMultiplier->Reset();
  m_Controls->UpperInspiratoryResistanceMultiplier->Reset();
  m_Controls->RespirationRateMultiplier->Reset();
  m_Controls->TidalVolumeMultiplier->Reset();
  m_Controls->LeftLungVolumeIncrement->Reset();
  m_Controls->RightLungVolumeIncrement->Reset();
}

void QModifiersWidget::CardiovascularControlsToPulse()
{
  m_Controls->CardioModifiers->Clear();
  auto& m = m_Controls->CardioModifiers->GetModifiers();
  if (m_Controls->ArterialComplianceMultiplier->IsChecked())
    m_Controls->ArterialComplianceMultiplier->GetValue(m.GetArterialComplianceMultiplier());
  if (m_Controls->ArterialResistanceMultiplier->IsChecked())
    m_Controls->ArterialResistanceMultiplier->GetValue(m.GetArterialResistanceMultiplier());
  if (m_Controls->PulmonaryComplianceMultiplier->IsChecked())
    m_Controls->PulmonaryComplianceMultiplier->GetValue(m.GetPulmonaryComplianceMultiplier());
  if (m_Controls->PulmonaryResistanceMultiplier->IsChecked())
    m_Controls->PulmonaryResistanceMultiplier->GetValue(m.GetPulmonaryResistanceMultiplier());
  if (m_Controls->SystemicResistanceMultiplier->IsChecked())
    m_Controls->SystemicResistanceMultiplier->GetValue(m.GetSystemicResistanceMultiplier());
  if (m_Controls->SystemicComplianceMultiplier->IsChecked())
    m_Controls->SystemicComplianceMultiplier->GetValue(m.GetSystemicComplianceMultiplier());
  if (m_Controls->VenousComplianceMultiplier->IsChecked())
    m_Controls->VenousComplianceMultiplier->GetValue(m.GetVenousComplianceMultiplier());
  if (m_Controls->VenousResistanceMultiplier->IsChecked())
    m_Controls->VenousResistanceMultiplier->GetValue(m.GetVenousResistanceMultiplier());
  if (m_Controls->HeartRateMultiplier->IsChecked())
    m_Controls->HeartRateMultiplier->GetValue(m.GetHeartRateMultiplier());
  if (m_Controls->StrokeVolumeMultiplier->IsChecked())
    m_Controls->StrokeVolumeMultiplier->GetValue(m.GetStrokeVolumeMultiplier());

  m_Controls->SendCardiovascularAction = true;
}

void QModifiersWidget::UpdateControls(SECardiovascularMechanicsModifiers const& m)
{

}

void QModifiersWidget::PulseToCardiovascularControls()
{
  m_Controls->PullCardiovascularModifiers = true;
}

void QModifiersWidget::RespiratoryControlsToPulse()
{
  m_Controls->RespModifiers->Clear();
  auto& m = m_Controls->RespModifiers->GetModifiers();
  if (m_Controls->LeftComplianceMultiplier->IsChecked())
    m_Controls->LeftComplianceMultiplier->GetValue(m.GetLeftComplianceMultiplier());
  if (m_Controls->RightComplianceMultiplier->IsChecked())
    m_Controls->RightComplianceMultiplier->GetValue(m.GetRightComplianceMultiplier());
  if (m_Controls->LeftExpiratoryResistanceMultiplier->IsChecked())
    m_Controls->LeftExpiratoryResistanceMultiplier->GetValue(m.GetLeftExpiratoryResistanceMultiplier());
  if (m_Controls->LeftInspiratoryResistanceMultiplier->IsChecked())
    m_Controls->LeftInspiratoryResistanceMultiplier->GetValue(m.GetLeftInspiratoryResistanceMultiplier());
  if (m_Controls->RightExpiratoryResistanceMultiplier->IsChecked())
    m_Controls->RightExpiratoryResistanceMultiplier->GetValue(m.GetRightExpiratoryResistanceMultiplier());
  if (m_Controls->RightInspiratoryResistanceMultiplier->IsChecked())
    m_Controls->RightInspiratoryResistanceMultiplier->GetValue(m.GetRightInspiratoryResistanceMultiplier());
  if (m_Controls->UpperExpiratoryResistanceMultiplier->IsChecked())
    m_Controls->UpperExpiratoryResistanceMultiplier->GetValue(m.GetUpperExpiratoryResistanceMultiplier());
  if (m_Controls->UpperInspiratoryResistanceMultiplier->IsChecked())
    m_Controls->UpperInspiratoryResistanceMultiplier->GetValue(m.GetUpperInspiratoryResistanceMultiplier());
  if (m_Controls->RespirationRateMultiplier->IsChecked())
    m_Controls->RespirationRateMultiplier->GetValue(m.GetRespirationRateMultiplier());
  if (m_Controls->TidalVolumeMultiplier->IsChecked())
    m_Controls->TidalVolumeMultiplier->GetValue(m.GetTidalVolumeMultiplier());

  if (m_Controls->LeftLungVolumeIncrement->IsChecked())
    m_Controls->LeftLungVolumeIncrement->GetValue(m.GetLungVolumeIncrement(eLungCompartment::LeftLung));
  if (m_Controls->RightLungVolumeIncrement->IsChecked())
    m_Controls->RightLungVolumeIncrement->GetValue(m.GetLungVolumeIncrement(eLungCompartment::RightLung));

  m_Controls->SendRespiratoryAction = true;
}

void QModifiersWidget::UpdateControls(SERespiratoryMechanicsModifiers const& m)
{

}

void QModifiersWidget::PulseToRespiratoryControls()
{
  m_Controls->PullRespiratoryModifiers = true;
}

void QModifiersWidget::AtSteadyState(PhysiologyEngine& pulse)
{
  
}
void QModifiersWidget::AtSteadyStateUpdateUI()
{
  bool show = (m_Controls->Pulse.GetMode() == ExplorerMode::Editor);
  EnableControls(show);
}

void QModifiersWidget::ProcessPhysiology(PhysiologyEngine& pulse)
{// This is called from a thread, you should NOT update UI here
  // This is where we pull data from pulse, and push any actions to it

  if (m_Controls->SendCardiovascularAction)
  {
    m_Controls->CardioModifiers->SetIncremental(true);
    pulse.ProcessAction(*m_Controls->CardioModifiers);
    m_Controls->SendCardiovascularAction = false;
  }

  if (m_Controls->PullCardiovascularModifiers)
  {
    if (pulse.GetActionManager().GetPatientActions().HasCardiovascularMechanicsModification())
    {
      m_Controls->CardioModifiers->Copy(
        *pulse.GetActionManager().GetPatientActions().GetCardiovascularMechanicsModification());
    }
    else
    {
      m_Controls->CardioModifiers->Clear();
    }
    m_Controls->PullCardiovascularModifiers = false;
    m_Controls->PushCardiovascularModifiersToControls = true;
  }

  if (m_Controls->SendRespiratoryAction)
  {
    m_Controls->RespModifiers->SetIncremental(true);
    pulse.ProcessAction(*m_Controls->RespModifiers);
    m_Controls->SendRespiratoryAction = false;
  }

  if (m_Controls->PullRespiratoryModifiers)
  {
    if (pulse.GetActionManager().GetPatientActions().HasRespiratoryMechanicsModification())
    {
      m_Controls->RespModifiers->Copy(
        *pulse.GetActionManager().GetPatientActions().GetRespiratoryMechanicsModification());
    }
    else
    {
      m_Controls->RespModifiers->Clear();
    }
    m_Controls->PullRespiratoryModifiers = false;
    m_Controls->PushRespiratoryModifiersToControls = true;
  }
}
void QModifiersWidget::PhysiologyUpdateUI(const std::vector<SEAction const*>& actions)
{// This is called from a slot, you can update UI here
  if (m_Controls->PushCardiovascularModifiersToControls)
  {
    UpdateControls(m_Controls->CardioModifiers->GetModifiers());
    m_Controls->PushCardiovascularModifiersToControls = false;
  }

  if (m_Controls->PushRespiratoryModifiersToControls)
  {
    UpdateControls(m_Controls->RespModifiers->GetModifiers());
    m_Controls->PushRespiratoryModifiersToControls = false;
  }
}
