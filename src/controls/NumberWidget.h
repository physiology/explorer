/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/
#pragma once

#include <functional>
#include <memory>



#include <QFrame>

class PhysiologyEngine;
class SEDataRequest;

class NumberWidget : public QFrame
{
  Q_OBJECT
public:
  NumberWidget(QFrame* parent = nullptr);
  virtual ~NumberWidget();

  Q_PROPERTY(QString name READ getName WRITE setName)
  Q_PROPERTY(QString unit READ getUnit WRITE setUnit)
  Q_PROPERTY(double value READ getValue WRITE setValue)
  Q_PROPERTY(int precision READ getPrecision WRITE setPrecision)

  void setup(QString name, QString unit, int precision = 0, bool isRatio=false);

  QString getName() const;
  void setName(QString name);

  QString getUnit() const;
  void setUnit(QString unit);

  double getValue() const;
  void setValue(double);

  void setPrecision(int val);
  int getPrecision() const;

  void processPhysiology(PhysiologyEngine& pulse);

  void setDataRequest(SEDataRequest* request);

  void updateValue();

  void setWithoutUpdate(double val);

private:
  struct Private;
  Private* d;
};
