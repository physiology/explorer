/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/

#include "PlotSetWidget.h"
#include "ui_PlotSet.h"

#include "QwtPulsePlot.h"
#include "DataRequestWidget.h"

#include "pulse/engine/PulseEngine.h"
#include "pulse/engine/PulseScenario.h"
#include "pulse/cdm/engine/SEEngineTracker.h"
#include "pulse/cdm/engine/SEDataRequestManager.h"
#include "pulse/cdm/properties/SEScalarTime.h"

// The method is used to set a plot name as well as its title
// When getting the name, we don't want the unit in the name, so includeUnit=false
// When getting the title,we want the unit, so includeUnit=true
// The Explorer DataRequest creation UI does not provide a way for the user to select a unit
// So the unit shown will be in whatever unit the engine uses for the requested property
// If we ever do provide a way for the user to select the DR unit, we will need to revisit this logic

// TODO When we load a scenario, with data requests, do they have a unit? Are we honoring it?
std::string ToString(SEDataRequest& dr, bool includeUnit=false)
{
  std::stringstream ss;
  switch (dr.GetCategory())
  {
  case eDataRequest_Category::Patient:
    ss << "Patient ";
  case eDataRequest_Category::Physiology:
  case eDataRequest_Category::Environment:
  case eDataRequest_Category::AnesthesiaMachine:
  case eDataRequest_Category::ECG:
  case eDataRequest_Category::Inhaler:
  {
    if (!includeUnit)
      ss << dr.GetPropertyName();
    else if (dr.GetUnit())
      ss << dr.GetPropertyName() << "(" << dr.GetUnit() << ")";
    else
      ss << dr.GetPropertyName();
    break;
  }
  case eDataRequest_Category::GasCompartment:
  case eDataRequest_Category::LiquidCompartment:
  case eDataRequest_Category::ThermalCompartment:
  case eDataRequest_Category::TissueCompartment:
  {
    if (dr.HasSubstanceName())
    {
      if (!includeUnit)
        ss << dr.GetCompartmentName() << " " << dr.GetSubstanceName() << " " << dr.GetPropertyName();
      else if (dr.GetUnit())
        ss << dr.GetCompartmentName() << " " << dr.GetSubstanceName() << " " << dr.GetPropertyName() << " (" << dr.GetUnit() << ")";
      else
        ss << dr.GetCompartmentName() << " " << dr.GetSubstanceName() << " " << dr.GetPropertyName();
        
    }
    else
    {
      if (!includeUnit)
        ss << dr.GetCompartmentName() << " " << dr.GetPropertyName();
      else if (dr.GetUnit())
        ss << dr.GetCompartmentName() << " " << dr.GetPropertyName() << " (" << dr.GetUnit() << ")";
      else
        ss << dr.GetCompartmentName() << " " << dr.GetPropertyName();
    }
    break;
  }
  case eDataRequest_Category::Substance:
  {
    if (dr.HasCompartmentName())
    {
      if (!includeUnit)
        ss << dr.GetSubstanceName() << " " << dr.GetCompartmentName() << " " << dr.GetPropertyName();
      else if (dr.GetUnit())
        ss << dr.GetSubstanceName() << " " << dr.GetCompartmentName() << " " << dr.GetPropertyName() << " (" << dr.GetUnit() << ")";
      else
        ss << dr.GetSubstanceName() << " " << dr.GetCompartmentName() << " " << dr.GetPropertyName();
    }
    else
    {
      if (!includeUnit)
        ss << dr.GetSubstanceName() << " " << dr.GetPropertyName();
      else if (dr.GetUnit())
        ss << dr.GetSubstanceName() << " " << dr.GetPropertyName() << " (" << dr.GetUnit() << ")";
      else
        ss << dr.GetSubstanceName() << " " << dr.GetPropertyName();
    }
    break;
  }
  default:
    ss << "Unhandled data request category: " << eDataRequest_Category_Name(dr.GetCategory()) << std::endl;
  }
  return ss.str();
}

class QPlotSetWidget::Controls : public Ui::PlotSetWidget
{
public:
  Controls(SEDataRequestManager& drMgr) : DataRequestManager(drMgr) {}
  SEDataRequestManager&              DataRequestManager;
  QDataRequestWidget*                DataRequestWidget;
  size_t                             CurrentPlot = -1;
  std::vector<QwtPulsePlot*>         Plots;
  std::vector<const SEDataRequest*>  DataRequests;
  std::vector<const SEDataRequest*>  EngineDataRequests;

};

QPlotSetWidget::QPlotSetWidget(QPulse& qp, SEDataRequestManager& drMgr, QWidget *parent, Qt::WindowFlags flags) : QDockWidget(parent, flags)
{
  m_Controls = new Controls(drMgr);
  m_Controls->setupUi(this);

  m_Controls->DataRequestWidget = new QDataRequestWidget(qp, this);
  m_Controls->PlotContainer->layout()->addWidget(m_Controls->DataRequestWidget);
  m_Controls->DataRequestWidget->setTitleBarWidget(new QWidget());
  m_Controls->PlotSetButtons->hide();
  m_Controls->DataRequestButtons->show();
  m_Controls->DataRequestWidget->show();
  m_Controls->PlotComboBox->hide();

  connect(m_Controls->PlotComboBox, SIGNAL(currentIndexChanged(int)),SLOT(SwitchPlot()));
  connect(m_Controls->AddPlotButton, SIGNAL(clicked()),SLOT(AddPlot()));
  connect(m_Controls->RemovePlotButton, SIGNAL(clicked()), SLOT(RemovePlot()));
  connect(m_Controls->AddDRButton, SIGNAL(clicked()), SLOT(AddDataRequest()));
  connect(m_Controls->CancelDRButton, SIGNAL(clicked()), SLOT(CancelDataRequest()));

  this->setWindowTitle(QString(""));
}

QPlotSetWidget::~QPlotSetWidget()
{
  delete m_Controls;
}

void QPlotSetWidget::Clear()
{
  for (QwtPulsePlot* plot : m_Controls->Plots)
     plot->Clear();
  m_Controls->EngineDataRequests.clear();
}

void QPlotSetWidget::AddDataRequest(SEDataRequest& dr)
{
  std::string name = ToString(dr);
  int idx=-1;
  for(; idx<m_Controls->Plots.size(); idx++)
    if (m_Controls->Plots[idx]->GetName() == name)
      break;
  if (idx == -1)
  {
    idx = (int)m_Controls->Plots.size();
    QwtPulsePlot* newPlot = new QwtPulsePlot(this,1000);
    QPalette pal = newPlot->GetPlot().palette();
    pal.setColor(QPalette::Window, Qt::white);
    newPlot->GetPlot().setPalette(pal);
    newPlot->GetPlot().setAutoFillBackground(true);
    newPlot->GetCurve().setPen(Qt::blue, 2);
    newPlot->GetGrid().setPen(Qt::gray);
    newPlot->GetGrid().enableX(true);
    newPlot->GetGrid().enableY(true);
    m_Controls->Plots.push_back(newPlot);
    m_Controls->DataRequests.push_back(&dr);
    newPlot->SetName(name);
    // For now, use the name as the title, we'll update it with a unit later
    newPlot->GetPlot().setTitle(dr.GetPropertyName().c_str());
    m_Controls->PlotComboBox->addItem(name.c_str());
    m_Controls->PlotContainer->layout()->addWidget(newPlot);
  }
  // Update Controls
  m_Controls->PlotComboBox->show();
  m_Controls->PlotComboBox->setCurrentIndex(idx);
  m_Controls->DataRequestWidget->hide();
  m_Controls->PlotSetButtons->show();
  m_Controls->DataRequestButtons->hide();
}

void QPlotSetWidget::AddDataRequest()
{
  SEDataRequest& dr = m_Controls->DataRequestWidget->GetDataRequest(m_Controls->DataRequestManager);
  this->AddDataRequest(dr);
}

void QPlotSetWidget::CancelDataRequest()
{
  m_Controls->DataRequestWidget->hide();
  m_Controls->PlotSetButtons->show();
  m_Controls->DataRequestButtons->hide();
}

void QPlotSetWidget::AddPlot()
{
  m_Controls->PlotComboBox->hide();
  m_Controls->DataRequestWidget->show();
  m_Controls->PlotSetButtons->hide();
  m_Controls->DataRequestButtons->show();
  for (QwtPulsePlot* plot : m_Controls->Plots)
    plot->GetPlot().hide();
}
void QPlotSetWidget::RemovePlot()
{
  int idx = m_Controls->PlotComboBox->currentIndex();
  delete m_Controls->Plots[idx];
  m_Controls->Plots.erase(m_Controls->Plots.begin() + idx);
  m_Controls->DataRequests.erase(m_Controls->DataRequests.begin() + idx);
  m_Controls->EngineDataRequests.erase(m_Controls->EngineDataRequests.begin() + idx);
  m_Controls->PlotComboBox->removeItem(idx);
  if (m_Controls->Plots.size() == 0)
    AddPlot();
  else
    m_Controls->PlotComboBox->setCurrentIndex(idx);
}

void QPlotSetWidget::SwitchPlot()
{
  int idx = m_Controls->PlotComboBox->currentIndex();
  if (m_Controls->Plots.size() == 0)
    return;
  for (QwtPulsePlot* plot : m_Controls->Plots)
    plot->GetPlot().hide();
  m_Controls->Plots[idx]->GetPlot().show();
}

void QPlotSetWidget::AtSteadyState(PhysiologyEngine& pulse)
{

}
void QPlotSetWidget::AtSteadyStateUpdateUI()
{

}

void QPlotSetWidget::ProcessPhysiology(PhysiologyEngine& pulse)
{// This is called from a thread, you should NOT update UI here
  // This is where we pull data from pulse, and push any actions to it
  double v;
  const SEDataRequest* dr;
  double time = pulse.GetSimulationTime(TimeUnit::s);
  if (m_Controls->DataRequests.size() != m_Controls->EngineDataRequests.size())
  {
    // New Data Request!
    for (size_t i = 0; i < m_Controls->DataRequests.size(); i++)
    {
      if (i+1 > m_Controls->EngineDataRequests.size())
      {
        SEDataRequest const* dr = m_Controls->DataRequests[i];
        SEDataRequest* pulseDr = &pulse.GetEngineTracker()->GetDataRequestManager().CopyDataRequest(*dr);
        m_Controls->EngineDataRequests.push_back(pulseDr);
        // Update the plot title to include the unit
        // TrackRequest will hook up the request to the engine,
        // which includes setting the unit to the engine unit if none was requested
        pulse.GetEngineTracker()->TrackRequest(*pulseDr);
        std::string unit = pulse.GetEngineTracker()->GetUnit(*pulseDr);
        if (!unit.empty())
        {
          // The name is the original string used when creating the plot
          // We don't include the unit in the original name
          std::string name = ToString(*pulseDr, false);
          std::string title = ToString(*pulseDr, true);
          for (int idx = 0; idx < m_Controls->Plots.size(); idx++)
            if (m_Controls->Plots[idx]->GetName() == name)
              m_Controls->Plots[idx]->GetPlot().setTitle(title.c_str());
        }

      }
    }
  }
  // Push the values into the plots
  for (size_t i = 0; i < m_Controls->Plots.size(); i++)
  {
    dr = m_Controls->EngineDataRequests[i];
    v = pulse.GetEngineTracker()->GetValue(*dr);
    m_Controls->Plots[i]->Append(time, v);
  }
}

void QPlotSetWidget::PhysiologyUpdateUI(const std::vector<SEAction const*>& actions)
{
  if (!m_Controls->PlotComboBox->isHidden())
  {
    int idx = m_Controls->PlotComboBox->currentIndex();
    m_Controls->Plots[idx]->UpdateUI();
  }
}