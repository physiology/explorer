/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QWidget>
#include "controls/conditions/ConditionWidget.h"

class QChronicAnemiaWidget : public QConditionWidget
{
  Q_OBJECT
public:
  QChronicAnemiaWidget(QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QChronicAnemiaWidget();

  virtual void Reset() override;
  virtual void EnableProperties(bool b) override;
  virtual void EnableConverter(bool b) override;
  virtual SECondition const& GetCondition() override;
  virtual void SetCondition(SECondition const& c) override;

private:
  class Controls;
  Controls* m_Controls;
  
};
