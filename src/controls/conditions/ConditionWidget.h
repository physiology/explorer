/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QWidget>
class SECondition;

namespace Ui {
  class ConditionWidget;
}

class QConditionWidget : public QWidget
{
  Q_OBJECT
public:
  QConditionWidget(QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QConditionWidget();

  virtual void Reset();
  virtual bool IsSelected();
  virtual void SetEnabled(bool b);// Turn on/off check box
  virtual void EnableCondition(bool b);// Sets Check to on/off
  virtual void EnableConverter(bool b) = 0;

  virtual SECondition const& GetCondition()=0;
  virtual void SetCondition(SECondition const& c)=0;

protected slots:
virtual void EnableConditionInput(bool b);// Tells condition properties to be all on/off
virtual void EnableProperties(bool b) = 0;// Turn on/off properties

private:
  class Controls;
  Controls* m_Controls;
};
