/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/conditions/ChronicAnemiaWidget.h"
#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/patient/conditions/SEChronicAnemia.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QChronicAnemiaWidget::Controls
{
public:
  Controls() {}
  SEChronicAnemia Condition;
  QScalarWidget* ReductionFactor;
};

QChronicAnemiaWidget::QChronicAnemiaWidget(QWidget *parent, Qt::WindowFlags flags) : QConditionWidget(parent, flags)
{
  m_Controls = new Controls();
  m_Controls->ReductionFactor = new QScalarWidget("Reduction Factor", 0, 1, 0.1, ScalarOptionWidget::None, this);
  layout()->addWidget(m_Controls->ReductionFactor);
  Reset();
}

QChronicAnemiaWidget::~QChronicAnemiaWidget()
{
  delete m_Controls;
}

void QChronicAnemiaWidget::Reset()
{
  QConditionWidget::Reset();
  m_Controls->Condition.Clear();
  m_Controls->ReductionFactor->Reset();
  EnableProperties(false);
}

void QChronicAnemiaWidget::EnableProperties(bool b)
{
  m_Controls->ReductionFactor->EnableInput(b);
}
void QChronicAnemiaWidget::EnableConverter(bool b)
{
}

const SECondition& QChronicAnemiaWidget::GetCondition()
{
  m_Controls->Condition.Clear();
  m_Controls->ReductionFactor->GetValue(m_Controls->Condition.GetReductionFactor());
  return m_Controls->Condition;
}

void QChronicAnemiaWidget::SetCondition(const SECondition& c)
{
  m_Controls->Condition.Copy(static_cast<const SEChronicAnemia&>(c));
  m_Controls->ReductionFactor->SetValue(m_Controls->Condition.GetReductionFactor());
  QConditionWidget::EnableCondition(c.IsActive());
}

