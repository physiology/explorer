/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/conditions/ChronicPericardialEffusionWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/patient/conditions/SEChronicPericardialEffusion.h"
#include "pulse/cdm/properties/SEScalarVolume.h"

class QChronicPericardialEffusionWidget::Controls
{
public:
  Controls() {}
  ~Controls()
  {
    //delete AccumulatedVolume;
  }
  QScalarQuantityWidget<VolumeUnit>* AccumulatedVolume;
  SEChronicPericardialEffusion Condition;
};

QChronicPericardialEffusionWidget::QChronicPericardialEffusionWidget(QWidget *parent, Qt::WindowFlags flags) : QConditionWidget(parent,flags)
{
  m_Controls = new Controls();
  m_Controls->AccumulatedVolume = new QScalarQuantityWidget<VolumeUnit>("Accumulated Volume", 0, 1000, 1, VolumeUnit::mL, ScalarOptionWidget::None, this);
  m_Controls->AccumulatedVolume->AddUnit(VolumeUnit::L);
  layout()->addWidget(m_Controls->AccumulatedVolume);
  Reset();
}

QChronicPericardialEffusionWidget::~QChronicPericardialEffusionWidget()
{
  delete m_Controls;
}


void QChronicPericardialEffusionWidget::Reset()
{
  QConditionWidget::Reset();
  m_Controls->Condition.Clear();
  m_Controls->AccumulatedVolume->Reset();
  EnableProperties(false);
}

void QChronicPericardialEffusionWidget::EnableProperties(bool b)
{
  m_Controls->AccumulatedVolume->EnableInput(b);
  m_Controls->AccumulatedVolume->EnableConverter(!b);
}
void QChronicPericardialEffusionWidget::EnableConverter(bool b)
{
  m_Controls->AccumulatedVolume->EnableConverter(b);
}

const SECondition& QChronicPericardialEffusionWidget::GetCondition()
{
  m_Controls->Condition.Clear();
  m_Controls->AccumulatedVolume->GetValue(m_Controls->Condition.GetAccumulatedVolume());
  return m_Controls->Condition;
}

void QChronicPericardialEffusionWidget::SetCondition(const SECondition& c)
{
  m_Controls->Condition.Copy(static_cast<const SEChronicPericardialEffusion&>(c));
  m_Controls->AccumulatedVolume->SetValue(m_Controls->Condition.GetAccumulatedVolume());
  QConditionWidget::EnableCondition(c.IsActive());
}

