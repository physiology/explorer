/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/conditions/ConditionWidget.h"
#include "ui_Condition.h"
#include <QMessageBox>
#include "controls/ScalarWidget.h"

class QConditionWidget::Controls : public Ui::ConditionWidget
{
public:
  Controls() {}
};

QConditionWidget::QConditionWidget(QWidget *parent, Qt::WindowFlags flags) : QWidget(parent,flags)
{
  m_Controls = new Controls();
  m_Controls->setupUi(this);

  connect(m_Controls->Enabled, SIGNAL(toggled(bool)), this, SLOT(EnableConditionInput(bool)));
}

QConditionWidget::~QConditionWidget()
{
  delete m_Controls;
}

void QConditionWidget::Reset()
{
  m_Controls->Enabled->setChecked(false);
  SetEnabled(true);
}

bool QConditionWidget::IsSelected()
{
  return m_Controls->Enabled->isChecked();
}

void QConditionWidget::SetEnabled(bool b)
{
  QWidget::setEnabled(b);
  m_Controls->Enabled->setEnabled(b);
  EnableProperties(b?m_Controls->Enabled->isChecked():false);
}

void QConditionWidget::EnableCondition(bool b)
{
  m_Controls->Enabled->setChecked(b);
}

void QConditionWidget::EnableConditionInput(bool b)
{
  EnableProperties(b);
}
