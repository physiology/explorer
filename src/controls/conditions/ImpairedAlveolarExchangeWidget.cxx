/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/conditions/ImpairedAlveolarExchangeWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/patient/conditions/SEImpairedAlveolarExchange.h"
#include "pulse/cdm/properties/SEScalar0To1.h"
#include "pulse/cdm/properties/SEScalarArea.h"

class QImpairedAlveolarExchangeWidget::Controls
{
public:
  Controls() {}
  SEImpairedAlveolarExchange       Condition;
  QScalarWidget*                   Severity;
  QScalarWidget*                   ImpairedFraction;
  QScalarQuantityWidget<AreaUnit>* ImpairedSurfaceArea;
};

QImpairedAlveolarExchangeWidget::QImpairedAlveolarExchangeWidget(QWidget *parent, Qt::WindowFlags flags) : QConditionWidget(parent,flags)
{
  m_Controls = new Controls();
  m_Controls->Severity = new QScalarWidget("Severity", 0, 1, 0.1, ScalarOptionWidget::Radio, this);
  layout()->addWidget(m_Controls->Severity);
  m_Controls->ImpairedFraction = new QScalarWidget("Impaired Fraction", 0, 1, 0.1, ScalarOptionWidget::Radio, this);
  layout()->addWidget(m_Controls->ImpairedFraction);
  m_Controls->ImpairedSurfaceArea = new QScalarQuantityWidget<AreaUnit>("Impaired Surface Area", 0, 100, 1, AreaUnit::m2, ScalarOptionWidget::Radio, this);
  m_Controls->ImpairedSurfaceArea->AddUnit(AreaUnit::cm2);
  layout()->addWidget(m_Controls->ImpairedSurfaceArea);
  connect(m_Controls->Severity->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableSeverity()));
  connect(m_Controls->ImpairedFraction->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableImpairedFraction()));
  connect(m_Controls->ImpairedSurfaceArea->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableImpairedSurfaceArea()));
  Reset();
}

QImpairedAlveolarExchangeWidget::~QImpairedAlveolarExchangeWidget()
{
  delete m_Controls;
}

void QImpairedAlveolarExchangeWidget::Reset()
{
  QConditionWidget::Reset();
  m_Controls->Condition.Clear();
  m_Controls->Severity->Reset();
  m_Controls->ImpairedFraction->Reset();
  m_Controls->ImpairedSurfaceArea->Reset();
  EnableProperties(false);
}

void QImpairedAlveolarExchangeWidget::EnableProperties(bool b)
{
  if (!b)
  {
    m_Controls->Severity->FullDisable();
    m_Controls->ImpairedFraction->FullDisable();
    m_Controls->ImpairedSurfaceArea->FullDisable();
  }
  else
  {
    m_Controls->Severity->FullEnable();
    m_Controls->Severity->EnableInput(b);
    m_Controls->ImpairedFraction->FullEnable();
    m_Controls->ImpairedFraction->EnableInput(!b);
    m_Controls->ImpairedSurfaceArea->FullEnable();
    m_Controls->ImpairedSurfaceArea->EnableInput(!b);
  }
  m_Controls->ImpairedSurfaceArea->EnableConverter(!b);
}
void QImpairedAlveolarExchangeWidget::EnableConverter(bool b)
{
}

void QImpairedAlveolarExchangeWidget::EnableSeverity()
{
  bool b = m_Controls->Severity->IsChecked();
  m_Controls->Severity->EnableInput(b);
  m_Controls->ImpairedFraction->EnableInput(!b);
  m_Controls->ImpairedSurfaceArea->EnableInput(!b);
}
void QImpairedAlveolarExchangeWidget::EnableImpairedFraction()
{
  bool b = m_Controls->ImpairedFraction->IsChecked();
  m_Controls->Severity->EnableInput(!b);
  m_Controls->ImpairedFraction->EnableInput(b);
  m_Controls->ImpairedSurfaceArea->EnableInput(!b);
}
void QImpairedAlveolarExchangeWidget::EnableImpairedSurfaceArea()
{
  bool b = m_Controls->ImpairedSurfaceArea->IsChecked();
  m_Controls->Severity->EnableInput(!b);
  m_Controls->ImpairedFraction->EnableInput(!b);
  m_Controls->ImpairedSurfaceArea->EnableInput(b);
}

const SECondition& QImpairedAlveolarExchangeWidget::GetCondition()
{
  m_Controls->Condition.Clear();
  if (m_Controls->Severity->IsChecked())
    m_Controls->Severity->GetValue(m_Controls->Condition.GetSeverity());
  else if (m_Controls->ImpairedFraction->IsChecked())
    m_Controls->ImpairedFraction->GetValue(m_Controls->Condition.GetImpairedFraction());
  else
    m_Controls->ImpairedSurfaceArea->GetValue(m_Controls->Condition.GetImpairedSurfaceArea());
  return m_Controls->Condition;
}

void QImpairedAlveolarExchangeWidget::SetCondition(const SECondition& c)
{
  m_Controls->Condition.Copy(static_cast<const SEImpairedAlveolarExchange&>(c));
  m_Controls->Severity->SetValue(m_Controls->Condition.GetSeverity());
  m_Controls->ImpairedFraction->SetValue(m_Controls->Condition.GetImpairedFraction());
  m_Controls->ImpairedSurfaceArea->SetValue(m_Controls->Condition.GetImpairedSurfaceArea());
  QConditionWidget::EnableCondition(c.IsActive());
}
