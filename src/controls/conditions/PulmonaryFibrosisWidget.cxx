/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/conditions/PulmonaryFibrosisWidget.h"
#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/patient/conditions/SEPulmonaryFibrosis.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QPulmonaryFibrosisWidget::Controls
{
public:
  Controls() {}
  QScalarWidget* Severity;
  SEPulmonaryFibrosis Condition;
};

QPulmonaryFibrosisWidget::QPulmonaryFibrosisWidget(QWidget *parent, Qt::WindowFlags flags) : QConditionWidget(parent,flags)
{
  m_Controls = new Controls();
  m_Controls->Severity = new QScalarWidget("Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  layout()->addWidget(m_Controls->Severity);
  Reset();
}

QPulmonaryFibrosisWidget::~QPulmonaryFibrosisWidget()
{
  delete m_Controls;
}

void QPulmonaryFibrosisWidget::Reset()
{
  QConditionWidget::Reset();
  m_Controls->Condition.Clear();
  m_Controls->Severity->Reset();
  EnableProperties(false);
}

void QPulmonaryFibrosisWidget::EnableProperties(bool b)
{
  m_Controls->Severity->EnableInput(b);
}
void QPulmonaryFibrosisWidget::EnableConverter(bool b)
{
}

const SECondition& QPulmonaryFibrosisWidget::GetCondition()
{
  m_Controls->Condition.Clear();
  m_Controls->Severity->GetValue(m_Controls->Condition.GetSeverity());
  return m_Controls->Condition;
}

void QPulmonaryFibrosisWidget::SetCondition(const SECondition& c)
{
  m_Controls->Condition.Copy(static_cast<const SEPulmonaryFibrosis&>(c));
  m_Controls->Severity->SetValue(m_Controls->Condition.GetSeverity());
  QConditionWidget::EnableCondition(c.IsActive());
}

