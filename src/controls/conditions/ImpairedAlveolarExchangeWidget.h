/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QWidget>
#include "controls/conditions/ConditionWidget.h"

class QImpairedAlveolarExchangeWidget : public QConditionWidget
{
  Q_OBJECT
public:
  QImpairedAlveolarExchangeWidget(QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QImpairedAlveolarExchangeWidget();

  virtual void Reset() override;
  virtual void EnableProperties(bool b) override;
  virtual void EnableConverter(bool b) override;
  virtual SECondition const& GetCondition() override;
  virtual void SetCondition(SECondition const& c) override;

protected slots:
  void EnableSeverity();
  void EnableImpairedFraction();
  void EnableImpairedSurfaceArea();

private:
  class Controls;
  Controls* m_Controls;
  
};
