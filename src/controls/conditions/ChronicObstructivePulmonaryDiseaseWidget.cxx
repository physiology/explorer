/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/conditions/ChronicObstructivePulmonaryDiseaseWidget.h"
#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/patient/conditions/SEChronicObstructivePulmonaryDisease.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QChronicObstructivePulmonaryDiseaseWidget::Controls
{
public:
  Controls() {}
  SEChronicObstructivePulmonaryDisease Condition;
  QScalarWidget* BronchitisSeverity;
  QScalarWidget* EmphysemaLeftLungSeverity;
  QScalarWidget* EmphysemaRightLungSeverity;
};

QChronicObstructivePulmonaryDiseaseWidget::QChronicObstructivePulmonaryDiseaseWidget(QWidget *parent, Qt::WindowFlags flags) : QConditionWidget(parent,flags)
{
  m_Controls = new Controls();
  m_Controls->BronchitisSeverity = new QScalarWidget("Bronchitis Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  m_Controls->EmphysemaLeftLungSeverity = new QScalarWidget("Emphysema Left Lung Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  m_Controls->EmphysemaRightLungSeverity = new QScalarWidget("Emphysema Right Lung Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  layout()->addWidget(m_Controls->BronchitisSeverity);
  layout()->addWidget(m_Controls->EmphysemaLeftLungSeverity);
  layout()->addWidget(m_Controls->EmphysemaRightLungSeverity);
  Reset();
}

QChronicObstructivePulmonaryDiseaseWidget::~QChronicObstructivePulmonaryDiseaseWidget()
{
  delete m_Controls;
}

void QChronicObstructivePulmonaryDiseaseWidget::Reset()
{
  QConditionWidget::Reset();
  m_Controls->Condition.Clear();
  m_Controls->BronchitisSeverity->Reset();
  m_Controls->EmphysemaLeftLungSeverity->Reset();
  m_Controls->EmphysemaRightLungSeverity->Reset();
  EnableProperties(false);
}

void QChronicObstructivePulmonaryDiseaseWidget::EnableProperties(bool b)
{
  m_Controls->BronchitisSeverity->EnableInput(b);
  m_Controls->EmphysemaLeftLungSeverity->EnableInput(b);
  m_Controls->EmphysemaRightLungSeverity->EnableInput(b);
}
void QChronicObstructivePulmonaryDiseaseWidget::EnableConverter(bool b)
{
}

const SECondition& QChronicObstructivePulmonaryDiseaseWidget::GetCondition()
{
  m_Controls->Condition.Clear();
  m_Controls->BronchitisSeverity->GetValue(m_Controls->Condition.GetBronchitisSeverity());
  m_Controls->EmphysemaLeftLungSeverity->GetValue(m_Controls->Condition.GetEmphysemaSeverity(eLungCompartment::LeftLung));
  m_Controls->EmphysemaRightLungSeverity->GetValue(m_Controls->Condition.GetEmphysemaSeverity(eLungCompartment::RightLung));
  return m_Controls->Condition;
}

void QChronicObstructivePulmonaryDiseaseWidget::SetCondition(const SECondition& c)
{
  m_Controls->Condition.Copy(static_cast<const SEChronicObstructivePulmonaryDisease&>(c));
  m_Controls->BronchitisSeverity->SetValue(m_Controls->Condition.GetBronchitisSeverity());
  m_Controls->EmphysemaLeftLungSeverity->SetValue(m_Controls->Condition.GetEmphysemaSeverity(eLungCompartment::LeftLung));
  m_Controls->EmphysemaRightLungSeverity->SetValue(m_Controls->Condition.GetEmphysemaSeverity(eLungCompartment::RightLung));
  QConditionWidget::EnableCondition(c.IsActive());
}

