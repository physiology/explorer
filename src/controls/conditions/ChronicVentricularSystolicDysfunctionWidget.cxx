/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/conditions/ChronicVentricularSystolicDysfunctionWidget.h"
#include <QMessageBox>

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/patient/conditions/SEChronicVentricularSystolicDysfunction.h"

class QChronicVentricularSystolicDysfunctionWidget::Controls
{
public:
  Controls() {}
  SEChronicVentricularSystolicDysfunction Condition;
};

QChronicVentricularSystolicDysfunctionWidget::QChronicVentricularSystolicDysfunctionWidget(QWidget *parent, Qt::WindowFlags flags) : QConditionWidget(parent,flags)
{
  m_Controls = new Controls();
  Reset();
}

QChronicVentricularSystolicDysfunctionWidget::~QChronicVentricularSystolicDysfunctionWidget()
{
  delete m_Controls;
}

void QChronicVentricularSystolicDysfunctionWidget::Reset()
{
  QConditionWidget::Reset();
  m_Controls->Condition.Clear();
  EnableProperties(false);
}

void QChronicVentricularSystolicDysfunctionWidget::EnableProperties(bool b)
{
}
void QChronicVentricularSystolicDysfunctionWidget::EnableConverter(bool b)
{
}

const SECondition& QChronicVentricularSystolicDysfunctionWidget::GetCondition()
{
  m_Controls->Condition.Clear();
  return m_Controls->Condition;
}

void QChronicVentricularSystolicDysfunctionWidget::SetCondition(const SECondition& c)
{
  QConditionWidget::EnableCondition(c.IsActive());
}
