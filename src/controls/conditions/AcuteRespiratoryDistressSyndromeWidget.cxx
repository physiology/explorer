/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/conditions/AcuteRespiratoryDistressSyndromeWidget.h"
#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/patient/conditions/SEAcuteRespiratoryDistressSyndrome.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QAcuteRespiratoryDistressSyndromeWidget::Controls
{
public:
  Controls() {}
  SEAcuteRespiratoryDistressSyndrome Condition;
  QScalarWidget* LeftLungSeverity;
  QScalarWidget* RightLungSeverity;
};

QAcuteRespiratoryDistressSyndromeWidget::QAcuteRespiratoryDistressSyndromeWidget(QWidget *parent, Qt::WindowFlags flags) : QConditionWidget(parent,flags)
{
  m_Controls = new Controls();
  m_Controls->LeftLungSeverity = new QScalarWidget("Left Lung Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  layout()->addWidget(m_Controls->LeftLungSeverity);
  m_Controls->RightLungSeverity = new QScalarWidget("Right Lung Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  layout()->addWidget(m_Controls->RightLungSeverity);
  Reset();
}

QAcuteRespiratoryDistressSyndromeWidget::~QAcuteRespiratoryDistressSyndromeWidget()
{
  delete m_Controls;
}

void QAcuteRespiratoryDistressSyndromeWidget::Reset()
{
  QConditionWidget::Reset();
  m_Controls->Condition.Clear();
  m_Controls->LeftLungSeverity->Reset();
  m_Controls->RightLungSeverity->Reset();
  EnableProperties(false);
}

void QAcuteRespiratoryDistressSyndromeWidget::EnableProperties(bool b)
{
  m_Controls->LeftLungSeverity->EnableInput(b);
  m_Controls->RightLungSeverity->EnableInput(b);
}
void QAcuteRespiratoryDistressSyndromeWidget::EnableConverter(bool b)
{
}

const SECondition& QAcuteRespiratoryDistressSyndromeWidget::GetCondition()
{
  m_Controls->Condition.Clear();
  m_Controls->LeftLungSeverity->GetValue(m_Controls->Condition.GetSeverity(eLungCompartment::LeftLung));
  m_Controls->RightLungSeverity->GetValue(m_Controls->Condition.GetSeverity(eLungCompartment::RightLung));
  return m_Controls->Condition;
}

void QAcuteRespiratoryDistressSyndromeWidget::SetCondition(const SECondition& c)
{
  m_Controls->Condition.Copy(static_cast<const SEAcuteRespiratoryDistressSyndrome&>(c));
  m_Controls->LeftLungSeverity->SetValue(m_Controls->Condition.GetSeverity(eLungCompartment::LeftLung));
  m_Controls->RightLungSeverity->SetValue(m_Controls->Condition.GetSeverity(eLungCompartment::RightLung));
  QConditionWidget::EnableCondition(c.IsActive());
}

