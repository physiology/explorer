/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/conditions/PneumoniaWidget.h"
#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/patient/conditions/SEPneumonia.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QPneumoniaWidget::Controls
{
public:
  Controls() {}
  QScalarWidget* LeftLungSeverity;
  QScalarWidget* RightLungSeverity;
  SEPneumonia Condition;
};

QPneumoniaWidget::QPneumoniaWidget(QWidget *parent, Qt::WindowFlags flags) : QConditionWidget(parent,flags)
{
  m_Controls = new Controls();
  m_Controls->LeftLungSeverity = new QScalarWidget("Left Lung Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  m_Controls->RightLungSeverity = new QScalarWidget("Right Lung Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  layout()->addWidget(m_Controls->LeftLungSeverity);
  layout()->addWidget(m_Controls->RightLungSeverity);
  Reset();
}

QPneumoniaWidget::~QPneumoniaWidget()
{
  delete m_Controls;
}

void QPneumoniaWidget::Reset()
{
  QConditionWidget::Reset();
  m_Controls->Condition.Clear();
  m_Controls->LeftLungSeverity->Reset();
  m_Controls->RightLungSeverity->Reset();
  EnableProperties(false);
}

void QPneumoniaWidget::EnableProperties(bool b)
{
  m_Controls->LeftLungSeverity->EnableInput(b);
  m_Controls->RightLungSeverity->EnableInput(b);
}
void QPneumoniaWidget::EnableConverter(bool b)
{
}

const SECondition& QPneumoniaWidget::GetCondition()
{
  m_Controls->Condition.Clear();
  m_Controls->LeftLungSeverity->GetValue(m_Controls->Condition.GetSeverity(eLungCompartment::LeftLung));
  m_Controls->RightLungSeverity->GetValue(m_Controls->Condition.GetSeverity(eLungCompartment::RightLung));
  return m_Controls->Condition;
}

void QPneumoniaWidget::SetCondition(const SECondition& c)
{
  m_Controls->Condition.Copy(static_cast<const SEPneumonia&>(c));
  m_Controls->LeftLungSeverity->SetValue(m_Controls->Condition.GetSeverity(eLungCompartment::LeftLung));
  m_Controls->RightLungSeverity->SetValue(m_Controls->Condition.GetSeverity(eLungCompartment::RightLung));
  QConditionWidget::EnableCondition(c.IsActive());
}

