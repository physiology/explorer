/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/conditions/ChronicRenalStenosisWidget.h"
#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/patient/conditions/SEChronicRenalStenosis.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QChronicRenalStenosisWidget::Controls
{
public:
  Controls() {}
  SEChronicRenalStenosis Condition;
  QScalarWidget* LeftKidneySeverity;
  QScalarWidget* RightKidneySeverity;
};

QChronicRenalStenosisWidget::QChronicRenalStenosisWidget(QWidget *parent, Qt::WindowFlags flags) : QConditionWidget(parent,flags)
{
  m_Controls = new Controls();
  m_Controls->LeftKidneySeverity = new QScalarWidget("Left Kidney Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  m_Controls->RightKidneySeverity = new QScalarWidget("Right Kidney Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  layout()->addWidget(m_Controls->LeftKidneySeverity);
  layout()->addWidget(m_Controls->RightKidneySeverity);
  Reset();
}

QChronicRenalStenosisWidget::~QChronicRenalStenosisWidget()
{
  delete m_Controls;
}

void QChronicRenalStenosisWidget::Reset()
{
  QConditionWidget::Reset();
  m_Controls->Condition.Clear();
  m_Controls->LeftKidneySeverity->Reset();
  m_Controls->RightKidneySeverity->Reset();
  EnableProperties(false);
}

void QChronicRenalStenosisWidget::EnableProperties(bool b)
{
  m_Controls->LeftKidneySeverity->EnableInput(b);
  m_Controls->RightKidneySeverity->EnableInput(b);
}
void QChronicRenalStenosisWidget::EnableConverter(bool b)
{
}

const SECondition& QChronicRenalStenosisWidget::GetCondition()
{
  m_Controls->Condition.Clear();
  m_Controls->LeftKidneySeverity->GetValue(m_Controls->Condition.GetLeftKidneySeverity());
  m_Controls->RightKidneySeverity->GetValue(m_Controls->Condition.GetRightKidneySeverity());
  return m_Controls->Condition;
}

void QChronicRenalStenosisWidget::SetCondition(const SECondition& c)
{
  m_Controls->Condition.Copy(static_cast<const SEChronicRenalStenosis&>(c));
  m_Controls->LeftKidneySeverity->SetValue(m_Controls->Condition.GetLeftKidneySeverity());
  m_Controls->RightKidneySeverity->SetValue(m_Controls->Condition.GetRightKidneySeverity());
  QConditionWidget::EnableCondition(c.IsActive());
}

