/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/conditions/PulmonaryShuntWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/patient/conditions/SEPulmonaryShunt.h"
#include "pulse/cdm/properties/SEScalar0To1.h"
#include "pulse/cdm/properties/SEScalarArea.h"

class QPulmonaryShuntWidget::Controls
{
public:
  Controls() {}
  SEPulmonaryShunt Condition;
  QScalarWidget* Severity;
};

QPulmonaryShuntWidget::QPulmonaryShuntWidget(QWidget *parent, Qt::WindowFlags flags) : QConditionWidget(parent,flags)
{
  m_Controls = new Controls();
  m_Controls->Severity = new QScalarWidget("Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  layout()->addWidget(m_Controls->Severity);
  Reset();
}

QPulmonaryShuntWidget::~QPulmonaryShuntWidget()
{
  delete m_Controls;
}

void QPulmonaryShuntWidget::Reset()
{
  QConditionWidget::Reset();
  m_Controls->Condition.Clear();
  m_Controls->Severity->Reset();
  EnableProperties(false);
}

void QPulmonaryShuntWidget::EnableProperties(bool b)
{
  m_Controls->Severity->EnableInput(b);
}
void QPulmonaryShuntWidget::EnableConverter(bool b)
{
}

const SECondition& QPulmonaryShuntWidget::GetCondition()
{
  m_Controls->Condition.Clear();
  m_Controls->Severity->GetValue(m_Controls->Condition.GetSeverity());
  return m_Controls->Condition;
}

void QPulmonaryShuntWidget::SetCondition(const SECondition& c)
{
  m_Controls->Condition.Copy(static_cast<const SEPulmonaryShunt&>(c));
  m_Controls->Severity->SetValue(m_Controls->Condition.GetSeverity());
  QConditionWidget::EnableCondition(c.IsActive());
}
