/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QDockWidget>
#include "QPulse.h"
#include "CollapsableWidget.h"

#include "pulse/engine/PulseEngine.h"
#include "pulse/engine/PulseScenario.h"
#include "pulse/cdm/engine/SEDataRequest.h"
#include "pulse/cdm/engine/SEActionManager.h"
#include "pulse/cdm/engine/SEAction.h"

namespace Ui {
  class ScenarioActionsEditorWidget;
}

class QScenarioActionsEditorWidget : public QDockWidget, public PulseListener
{
  Q_OBJECT
protected:
  class ScenarioAction;
public:
  QScenarioActionsEditorWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QScenarioActionsEditorWidget();

  void Clear();
  void DrawScenario();
  void EnableControls(bool b);
  void UpdateAction(SEAction const&, SEScalarTime const&);

  void LoadScenario(SEScenario& scenario);
  void SaveScenario(SEScenario& scenario);

  QString GetName();

  void AtSteadyState(PhysiologyEngine& pulse) override;
  void AtSteadyStateUpdateUI() override; // Main Window will call this to update UI Components
  void ProcessPhysiology(PhysiologyEngine& pulse) override;
  void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override;// Main Window will call this to update UI Components
  void EngineErrorUI() override {};// Main Window will call this to update UI Components

signals:
  void OpenAction(SEAction const&, SEScalarTime const&);
public slots:
  void EditAction();
  void DeleteAction();

protected slots:
  void ClearActions();

protected:
  bool SameAction(SEAction const&, SEAction const&);
  bool SameType(SEAction const&, SEAction const&);

private:
  class Controls;
  Controls* m_Controls;
};
