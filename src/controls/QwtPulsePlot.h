/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QWidget>

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>

class QwtPulsePlot : public QWidget
{
    Q_OBJECT
public:
  
  QwtPulsePlot(QWidget* parent=nullptr, size_t max_size=300);
  virtual ~QwtPulsePlot();

  void Clear();

  QwtPlot& GetPlot();
  QwtPlotCurve& GetCurve();
  QwtPlotGrid& GetGrid();


  std::string GetName() const;
  void SetName(const std::string& name);

  void SetDataRange(double min, double max);
  void Append(double time, double value);
  void UpdateUI(bool pad=true);

private:
  class Data;
  Data* m_Data;
};
