/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QDockWidget>
#include "QPulse.h"
#include "pulse/cdm/engine/SEDataRequest.h"
#include "pulse/cdm/engine/SEDataRequestManager.h"

namespace Ui {
  class PlotSetWidget;
}

class QPlotSetWidget : public QDockWidget, public PulseListener
{
  Q_OBJECT
  friend class QDataRequestViewWidget;
public:
  explicit QPlotSetWidget(QPulse& qp, SEDataRequestManager& drMgr, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QPlotSetWidget();

  void Clear();

  void AtSteadyState(PhysiologyEngine& pulse) override;
  void AtSteadyStateUpdateUI() override;// Main Window will call this to update UI Components
  void ProcessPhysiology(PhysiologyEngine& pulse) override;
  void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override;// Main Window will call this to update UI Components
  void EngineErrorUI() override {};// Main Window will call this to update UI Components

protected:
  void AddDataRequest(SEDataRequest& dr);

signals:
protected slots:
  void SwitchPlot();
  void AddPlot();
  void RemovePlot();
  void AddDataRequest();
  void CancelDataRequest();
private:
  class Controls;
  Controls* m_Controls;
};

