/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QDockWidget>
#include "QPulse.h"

class SEPatient;

namespace Ui {
  class PatientEditorWidget;
}

class QPatientEditorWidget : public QDockWidget, public PulseListener
{
  Q_OBJECT
public:
  QPatientEditorWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QPatientEditorWidget();

  void Clear();
  void EnableInput(bool b);
  void EnableSetupControls(bool b);
  void ClearPatient();
  bool ValidPatient();
  void PatientToControls(SEPatient const& p);
  void ControlsToPatient(SEPatient& p);
  void SetStateFilename(QString& fileName);

  void AtSteadyState(PhysiologyEngine& pulse) override;
  void AtSteadyStateUpdateUI() override; // Main Window will call this to update UI Components
  void ProcessPhysiology(PhysiologyEngine& pulse) override {}
  void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override {}// Main Window will call this to update UI Components
  void EngineErrorUI() override {};// Main Window will call this to update UI Components

protected:

protected slots:
  void UpdateUI();
  void ResetControls();
  void LoadPatientFile();
  void SavePatientFile();
  void ShowAdvancedProperties();
  void SetInitialStateFilename();

  void EnableConverter(bool b);

private:
  class Controls;
  Controls* m_Controls;
  
};