/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QWidget>
#include <QHBoxLayout>
#include <QCheckBox>
#include <QLabel>
#include <QDoubleSpinBox>
#include <QSpacerItem>
#include <QComboBox>
#include <QRadioButton>

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/properties/SEScalar.h"


enum class ScalarOptionWidget { None, Check, Radio };

class QScalarWidget : public QWidget
{
  Q_OBJECT
public:
  explicit QScalarWidget(const QString& name, double min, double max, double step, ScalarOptionWidget optWidget=ScalarOptionWidget::Check, QWidget *parent = nullptr, bool seperate_label = false);

  void Reset();
  bool IsChecked();

  void SetDecimals(int prec);
  void SetValue(const SEScalar& s);
  void GetValue(SEScalar& s);
  void EnableInput(bool b);
  void EnableInput(bool check, bool value);
  void FullDisable();
  void FullEnable();

  const QRadioButton* GetRadioButton() { return m_Radio; }

public slots:
  void CheckProperty(bool b);

protected:

  QHBoxLayout*    m_Layout;
  QCheckBox*      m_Check;
  QRadioButton*   m_Radio;
  QLabel*         m_Label;
  QDoubleSpinBox* m_Value;
  QSpacerItem*    m_Spacer1;
  QSpacerItem*    m_Spacer2;
  double          m_LastValue;
};

class QScalarConvertWidget : public QWidget
{
  Q_OBJECT
public:
  explicit QScalarConvertWidget(QWidget *parent = nullptr) : QWidget(parent) {};

protected slots:
  virtual void ConvertValue() = 0;
  virtual void CheckProperty(bool b) = 0;
};
