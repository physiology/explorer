// Property of Kitware, Inc. and KbPort.
// PROPRIETARY: DO NOT DISTRIBUTE.

#pragma once

#include <QChar>

class QWidget;

QChar pulseIcon(QWidget const* widget);
void setPulseIcon(QChar icon, QWidget* widget);
