/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QDockWidget>
#include "QPulse.h"

namespace Ui {
  class ModifiersWidget;
}

class SECardiovascularMechanicsModifiers;
class SERespiratoryMechanicsModifiers;

class QModifiersWidget : public QDockWidget, public PulseListener
{
  Q_OBJECT
public:
  QModifiersWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QModifiersWidget();

  void Reset();

  void EnableControls(bool b);

  void UpdateControls(SECardiovascularMechanicsModifiers const& m);
  void UpdateControls(SERespiratoryMechanicsModifiers const& m);

  void AtSteadyState(PhysiologyEngine& pulse) override;
  void AtSteadyStateUpdateUI() override;// Main Window will call this to update UI Components
  void ProcessPhysiology(PhysiologyEngine& pulse) override;
  void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override;// Main Window will call this to update UI Components
  void EngineErrorUI() override {};// Main Window will call this to update UI Components

//signals:
protected slots:
  void ResetControls();
  void CardiovascularControlsToPulse();
  void PulseToCardiovascularControls();
  void RespiratoryControlsToPulse();
  void PulseToRespiratoryControls();

private:
  class Controls;
  Controls* m_Controls;
};