/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include <QApplication>
#include <QDockWidget>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLayout>
#include <QMainWindow>
#include <QWidget>
#include <QPlainTextEdit>
#include <QFile>
#include <QDoubleSpinBox>
#include <QGroupBox>
#include <QScrollBar>
#include <QTimer>
#include <QThread>
#include <QPointer>
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <QCloseEvent>
#include <QMessageBox>

#include "cdm/CommonDataModel.h"
#include "cdm/utils/FileUtils.h"

#ifdef PARAVIEW
#include <pqActiveObjects.h>
#include <pqAlwaysConnectedBehavior.h>
#include <pqApplicationCore.h>
#include <pqObjectBuilder.h>
#include <pqPersistentMainWindowStateBehavior.h>
#include <pqContextView.h>
#include <pqXYChartView.h>
#include <pqRenderView.h>

#include <vtkSMProxy.h>
#include <vtkSMPropertyHelper.h>
#include <vtkSMReaderFactory.h>

#include "GeometryView.h"
#endif

#include "AddPopup.h"
#include "ui_AddPopup.h"
#include "ExplorerIntroWidget.h"
#include "ui_AddPopup.h"
#include "DataRequestsWidget.h"
#include "VitalsMonitorWidget.h"
#include "DynamicControlsWidget.h"
#include "EnvironmentWidget.h"
#include "cdm/CommonDataModel.h"
#include "PulsePhysiologyEngine.h"
#include "cdm/engine/SEDataRequestManager.h"
#include "cdm/engine/SEEngineTracker.h"
#include "cdm/properties/SEScalarTime.h"

class AddPopup::Controls : public Ui::AddPopup
{
public:

	virtual ~Controls()
	{
		
	}

#ifdef PARAVIEW  
	QPointer<GeometryView>            GeometryView;
	pqRenderView*                     ParaViewRenderView;
#endif
	QPulse*                           Pulse = nullptr;
	QPointer<QThread>                 Thread;
	std::stringstream                 Status;
	double                            CurrentSimTime_s;
	InputMode                         Mode = InputMode::None;
	QEnvironmentWidget* envir;
};

AddPopup::AddPopup(std::string name,QWidget *parent):QDialog(parent),Ui(new Ui::AddPopup)
{
	QEnvironmentWidget* envir = dynamic_cast<QEnvironmentWidget*> (parent);
	m_Controls = new Controls();
	m_Controls->setupUi(this);
	setWindowIcon(QIcon("resource/pulse.ico"));
	QFont f("Arial", 18);
	m_Controls->label->setFont(f);
	m_Controls->label->setText(QString::fromUtf8(name.c_str()));
	if (name == "Add Gases") {
		m_Controls->Substance->addItem(QString("Oxygen"));
		m_Controls->Substance->addItem(QString("Nitrogen"));
		m_Controls->Substance->addItem(QString("Hydrogen"));
		m_Controls->Substance->addItem(QString("Argon"));
		m_Controls->Substance->addItem(QString("Carbon Dioxide"));
	}
	else {
		m_Controls->Substance->addItem(QString("Dust"));
		m_Controls->Substance->addItem(QString("Forest Exudates"));
		m_Controls->Substance->addItem(QString("Geyesr Steam"));
		m_Controls->Substance->addItem(QString("Smoke"));
		m_Controls->Substance->addItem(QString("Particulates"));
	}

	connect(m_Controls->Add, SIGNAL(clicked()), this, SLOT(add()));
}

AddPopup::~AddPopup()
{
	delete m_Controls;
}

void AddPopup::add() {
	if (m_Controls->label->text() == "Add Gases") {
		emit insertG(m_Controls->Substance->currentText(), m_Controls->Amount->value());

		hide();
	}
	else {
		emit insertA(m_Controls->Substance->currentText(), m_Controls->Amount->value());
		hide();
	}
}


