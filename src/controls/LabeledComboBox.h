/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QSpacerItem>
#include <QComboBox>
#include <QHBoxLayout>
#include <set>
#include <QString>

class QLabeledComboBox : public QWidget
{
  Q_OBJECT
public:
  QLabeledComboBox(QWidget* parent = nullptr, QString label = "", int maxWidth = 70);
  QLabeledComboBox(QWidget *parent = nullptr, QString label = "", std::set<QString> options = {}, int maxWidth = 70);
  QLabeledComboBox(QWidget* parent = nullptr, QString label = "", std::vector<QString> options = {}, int maxWidth = 70);

  void Reset();
  void SetEnabled(bool b);

  int GetIndex();
  std::string GetText();
  QComboBox* GetComboBox();
  void SetCurrentIndex(int index);

protected:
  QLabel*                                   Label;
  QSpacerItem*                              Spacer;
  QComboBox*                                ComboBox;
  QHBoxLayout*                              Layout;
};
