/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QWidget>
#include <QHBoxLayout>
#include <QCheckBox>
#include <QLabel>
#include <QDoubleSpinBox>
#include <QSpacerItem>
#include <QComboBox>
#include <QRadioButton>

#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/properties/SEScalar.h"

template <typename Unit>
class QScalarQuantityWidget : public QScalarConvertWidget
{
 
public:
  explicit QScalarQuantityWidget(const QString& name, double min, double max, double step, const Unit& unit, ScalarOptionWidget optWidget=ScalarOptionWidget::Check, QWidget *parent = nullptr, bool seperate_label = false);

  void Reset();
  bool IsChecked();

  void SetDecimals(int prec);
  void AddUnit(const Unit& unit);
  void SetValue(const SEScalarQuantity<Unit>& s);
  void GetValue(SEScalarQuantity<Unit>& s);
  bool SetDefault(double d);// Assumed to be in default units, must be in range
  void EnableInput(bool b);
  void EnableInput(bool check, bool value);
  void EnableConverter(bool b);
  void FullDisable();
  void FullEnable();
  bool IsZero() const;

  const QRadioButton* GetRadioButton() { return m_Radio; }

protected slots:
  void CheckProperty(bool b);
  void ConvertValue();

  void UpdateMinMax();

  QHBoxLayout*    m_Layout;
  QCheckBox*      m_Check;
  QRadioButton*   m_Radio;
  QLabel*         m_Label;
  QDoubleSpinBox* m_Value;
  QSpacerItem*    m_Spacer;
  QComboBox*      m_Unit;
  
  bool            m_Convert;
  double          m_Min;
  double          m_Max;
  double          m_Step;
  double          m_LastValue;
  double          m_Default;
  int             m_LastUnitIndex;
  const Unit&     m_DefaultUnit;
  std::vector<const Unit*> m_Units;

};


#include "controls/ScalarQuantityWidget.inl"
