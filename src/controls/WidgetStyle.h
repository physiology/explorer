// Property of Kitware, Inc. and KbPort.
// PROPRIETARY: DO NOT DISTRIBUTE.

#pragma once

#include <QCommonStyle>

// ----------------------------------------------------------------------------
class WidgetStyle : public QCommonStyle
{
  Q_OBJECT

public:
  WidgetStyle(QObject* parent = nullptr);
  ~WidgetStyle();

  void drawPrimitive(
    PrimitiveElement pe, QStyleOption const* opt, QPainter* p,
    QWidget const* w = nullptr) const override;

  void drawControl(
    ControlElement ce, QStyleOption const* opt, QPainter* p,
    QWidget const* w = nullptr) const override;

  void drawComplexControl(
    ComplexControl cc, QStyleOptionComplex const* opt, QPainter* p,
    QWidget const* w = nullptr) const override;

//   SubControl hitTestComplexControl(
//     ComplexControl cc, QStyleOptionComplex const* opt,
//     QPoint const &pt, QWidget const* w = nullptr) const override;

  QRect subControlRect(
    ComplexControl cc, QStyleOptionComplex const* opt, SubControl sc,
    QWidget const* w = nullptr) const override;

  QRect subElementRect(
    SubElement se, QStyleOption const* opt,
    QWidget const* w = nullptr) const override;

  QSize sizeFromContents(
    ContentsType ct, QStyleOption const* opt, QSize const &cs,
    QWidget const* w = nullptr) const override;

  int pixelMetric(
    PixelMetric m, QStyleOption const* opt = nullptr,
    QWidget const* w = nullptr) const override;

  int styleHint(
    StyleHint sh, QStyleOption const* opt = nullptr,
    QWidget const* w = nullptr,
    QStyleHintReturn* shret = nullptr) const override;

//   QIcon standardIcon(
//     StandardPixmap standardIcon, QStyleOption const* opt = nullptr,
//     QWidget const* w = nullptr) const override;

//   QPixmap standardPixmap(
//     StandardPixmap sp, QStyleOption const* opt = nullptr,
//     QWidget const* w = nullptr) const override;

  void polish(QWidget *widget) override;
};
