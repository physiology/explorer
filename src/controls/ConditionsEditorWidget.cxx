/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "ConditionsEditorWidget.h"
#include "ui_ConditionsEditor.h"
#include <QMessageBox>

#include "CollapsableWidget.h"

#include "conditions/ConditionWidget.h"
#include "conditions/AcuteRespiratoryDistressSyndromeWidget.h"
#include "conditions/ChronicAnemiaWidget.h"
#include "conditions/ChronicObstructivePulmonaryDiseaseWidget.h"
#include "conditions/ChronicPericardialEffusionWidget.h"
#include "conditions/ChronicRenalStenosisWidget.h"
#include "conditions/ChronicVentricularSystolicDysfunctionWidget.h"
#include "conditions/ImpairedAlveolarExchangeWidget.h"
#include "conditions/PneumoniaWidget.h"
#include "conditions/PulmonaryFibrosisWidget.h"
#include "conditions/PulmonaryShuntWidget.h"

#include "pulse/engine/PulseEngine.h"
#include "pulse/engine/PulseScenario.h"
#include "pulse/cdm/engine/SEPatientConfiguration.h"
#include "pulse/cdm/engine/SEConditionManager.h"
#include "pulse/cdm/engine/SECondition.h"
#include "pulse/cdm/patient/conditions/SEAcuteRespiratoryDistressSyndrome.h"
#include "pulse/cdm/patient/conditions/SEChronicAnemia.h"
#include "pulse/cdm/patient/conditions/SEChronicObstructivePulmonaryDisease.h"
#include "pulse/cdm/patient/conditions/SEChronicPericardialEffusion.h"
#include "pulse/cdm/patient/conditions/SEChronicRenalStenosis.h"
#include "pulse/cdm/patient/conditions/SEChronicVentricularSystolicDysfunction.h"
#include "pulse/cdm/patient/conditions/SEImpairedAlveolarExchange.h"
#include "pulse/cdm/patient/conditions/SEPneumonia.h"
#include "pulse/cdm/patient/conditions/SEPulmonaryFibrosis.h"
#include "pulse/cdm/patient/conditions/SEPulmonaryShunt.h"

class QConditionsEditorWidget::Controls : public Ui::ConditionsEditorWidget
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                Pulse;
  
  QAcuteRespiratoryDistressSyndromeWidget*       AcuteRespiratoryDistressSyndrome;
  QCollapsableWidget*                            CWAcuteRespiratoryDistressSyndrome;

  QChronicAnemiaWidget*                          ChronicAnemia;
  QCollapsableWidget*                            CWChronicAnemia;

  QChronicObstructivePulmonaryDiseaseWidget*     ChronicObstructivePulmonaryDisease;
  QCollapsableWidget*                            CWChronicObstructivePulmonaryDisease;

  QChronicPericardialEffusionWidget*             ChronicPericardialEffusion;
  QCollapsableWidget*                            CWChronicPericardialEffusion;

  QChronicRenalStenosisWidget*                   ChronicRenalStenosis;
  QCollapsableWidget*                            CWChronicRenalStenosis;

  QChronicVentricularSystolicDysfunctionWidget*  ChronicVentricularSystolicDysfunction;
  QCollapsableWidget*                            CWChronicVentricularSystolicDysfunction;

  QImpairedAlveolarExchangeWidget*               ImpairedAlveolarExchange;
  QCollapsableWidget*                            CWImpairedAlveolarExchange;

  QPneumoniaWidget*                              Pneumonia;
  QCollapsableWidget*                            CWPneumonia;

  QPulmonaryFibrosisWidget*                      PulmonaryFibrosis;
  QCollapsableWidget*                            CWPulmonaryFibrosis;

  QPulmonaryShuntWidget*                         PulmonaryShunt;
  QCollapsableWidget*                            CWPulmonaryShunt;
};

QConditionsEditorWidget::QConditionsEditorWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QDockWidget(parent,flags)
{
  m_Controls = new Controls(qp);
  m_Controls->setupUi(this);

  m_Controls->CWAcuteRespiratoryDistressSyndrome = new QCollapsableWidget("ARDS", 120, this);
  m_Controls->AcuteRespiratoryDistressSyndrome = new QAcuteRespiratoryDistressSyndromeWidget(this);
  m_Controls->CWAcuteRespiratoryDistressSyndrome->setVisible(true);
  m_Controls->CWAcuteRespiratoryDistressSyndrome->layout()->addWidget(m_Controls->AcuteRespiratoryDistressSyndrome);
  m_Controls->CWAcuteRespiratoryDistressSyndrome->setContentLayout(*m_Controls->AcuteRespiratoryDistressSyndrome->layout());
  m_Controls->ConditionWidgets->layout()->addWidget(m_Controls->CWAcuteRespiratoryDistressSyndrome);

  m_Controls->CWChronicAnemia = new QCollapsableWidget("Anemia", 120, this);
  m_Controls->ChronicAnemia = new QChronicAnemiaWidget(this);
  m_Controls->CWChronicAnemia->setVisible(true);
  m_Controls->CWChronicAnemia->layout()->addWidget(m_Controls->ChronicAnemia);
  m_Controls->CWChronicAnemia->setContentLayout(*m_Controls->ChronicAnemia->layout());
  m_Controls->ConditionWidgets->layout()->addWidget(m_Controls->CWChronicAnemia);

  m_Controls->CWChronicObstructivePulmonaryDisease = new QCollapsableWidget("COPD", 120, this);
  m_Controls->ChronicObstructivePulmonaryDisease = new QChronicObstructivePulmonaryDiseaseWidget(this);
  m_Controls->CWChronicObstructivePulmonaryDisease->setVisible(true);
  m_Controls->CWChronicObstructivePulmonaryDisease->layout()->addWidget(m_Controls->ChronicObstructivePulmonaryDisease);
  m_Controls->CWChronicObstructivePulmonaryDisease->setContentLayout(*m_Controls->ChronicObstructivePulmonaryDisease->layout());
  m_Controls->ConditionWidgets->layout()->addWidget(m_Controls->CWChronicObstructivePulmonaryDisease);

  m_Controls->CWChronicPericardialEffusion = new QCollapsableWidget("Pericardial Effusion", 120, this);
  m_Controls->ChronicPericardialEffusion = new QChronicPericardialEffusionWidget(this);
  m_Controls->CWChronicPericardialEffusion->setVisible(true);
  m_Controls->CWChronicPericardialEffusion->layout()->addWidget(m_Controls->ChronicPericardialEffusion);
  m_Controls->CWChronicPericardialEffusion->setContentLayout(*m_Controls->ChronicPericardialEffusion->layout());
  m_Controls->ConditionWidgets->layout()->addWidget(m_Controls->CWChronicPericardialEffusion);

  m_Controls->CWChronicRenalStenosis = new QCollapsableWidget("Renal Stenosis", 120, this);
  m_Controls->ChronicRenalStenosis = new QChronicRenalStenosisWidget(this);
  m_Controls->CWChronicRenalStenosis->setVisible(true);
  m_Controls->CWChronicRenalStenosis->layout()->addWidget(m_Controls->ChronicRenalStenosis);
  m_Controls->CWChronicRenalStenosis->setContentLayout(*m_Controls->ChronicRenalStenosis->layout());
  m_Controls->ConditionWidgets->layout()->addWidget(m_Controls->CWChronicRenalStenosis);
  
  m_Controls->CWChronicVentricularSystolicDysfunction = new QCollapsableWidget("Ventricular Systolic Dysfunction", 120, this);
  m_Controls->ChronicVentricularSystolicDysfunction = new QChronicVentricularSystolicDysfunctionWidget(this);
  m_Controls->CWChronicVentricularSystolicDysfunction->setVisible(true);
  m_Controls->CWChronicVentricularSystolicDysfunction->layout()->addWidget(m_Controls->ChronicVentricularSystolicDysfunction);
  m_Controls->CWChronicVentricularSystolicDysfunction->setContentLayout(*m_Controls->ChronicVentricularSystolicDysfunction->layout());
  m_Controls->ConditionWidgets->layout()->addWidget(m_Controls->CWChronicVentricularSystolicDysfunction);

  m_Controls->CWImpairedAlveolarExchange = new QCollapsableWidget("Impaired Alveolar Exchange", 120, this);
  m_Controls->ImpairedAlveolarExchange = new QImpairedAlveolarExchangeWidget(this);
  m_Controls->CWImpairedAlveolarExchange->setVisible(true);
  m_Controls->CWImpairedAlveolarExchange->layout()->addWidget(m_Controls->ImpairedAlveolarExchange);
  m_Controls->CWImpairedAlveolarExchange->setContentLayout(*m_Controls->ImpairedAlveolarExchange->layout());
  m_Controls->ConditionWidgets->layout()->addWidget(m_Controls->CWImpairedAlveolarExchange);

  m_Controls->CWPneumonia = new QCollapsableWidget("Pneumonia", 120, this);
  m_Controls->Pneumonia = new QPneumoniaWidget(this);
  m_Controls->CWPneumonia->setVisible(true);
  m_Controls->CWPneumonia->layout()->addWidget(m_Controls->Pneumonia);
  m_Controls->CWPneumonia->setContentLayout(*m_Controls->Pneumonia->layout());
  m_Controls->ConditionWidgets->layout()->addWidget(m_Controls->CWPneumonia);

  m_Controls->CWPulmonaryFibrosis = new QCollapsableWidget("Pulmonary Fibrosis", 120, this);
  m_Controls->PulmonaryFibrosis = new QPulmonaryFibrosisWidget(this);
  m_Controls->CWPulmonaryFibrosis->setVisible(true);
  m_Controls->CWPulmonaryFibrosis->layout()->addWidget(m_Controls->PulmonaryFibrosis);
  m_Controls->CWPulmonaryFibrosis->setContentLayout(*m_Controls->PulmonaryFibrosis->layout());
  m_Controls->ConditionWidgets->layout()->addWidget(m_Controls->CWPulmonaryFibrosis);

  m_Controls->CWPulmonaryShunt = new QCollapsableWidget("Pulmonary Shunt", 120, this);
  m_Controls->PulmonaryShunt = new QPulmonaryShuntWidget(this);
  m_Controls->CWPulmonaryShunt->setVisible(true);
  m_Controls->CWPulmonaryShunt->layout()->addWidget(m_Controls->PulmonaryShunt);
  m_Controls->CWPulmonaryShunt->setContentLayout(*m_Controls->PulmonaryShunt->layout());
  m_Controls->ConditionWidgets->layout()->addWidget(m_Controls->CWPulmonaryShunt);
  connect(m_Controls->ClearConditions, SIGNAL(clicked()), this, SLOT(ClearConditions()));

  QSpacerItem *spacer = new QSpacerItem(40, 1000, QSizePolicy::Expanding, QSizePolicy::Expanding);
  m_Controls->ConditionWidgets->layout()->addItem(spacer);
}

QConditionsEditorWidget::~QConditionsEditorWidget()
{
  delete m_Controls;
}

void QConditionsEditorWidget::Clear()
{
  ClearConditions();
  EnableInput(true);
}


void QConditionsEditorWidget::ClearConditions()
{
  m_Controls->Pulse.GetPatientConfiguration().GetConditions().Clear();

  m_Controls->AcuteRespiratoryDistressSyndrome->Reset();
  m_Controls->ChronicObstructivePulmonaryDisease->Reset();
  m_Controls->ChronicAnemia->Reset();
  m_Controls->ChronicObstructivePulmonaryDisease->Reset();
  m_Controls->ChronicPericardialEffusion->Reset();
  m_Controls->ChronicRenalStenosis->Reset();
  m_Controls->ChronicVentricularSystolicDysfunction->Reset();
  m_Controls->ImpairedAlveolarExchange->Reset();
  m_Controls->Pneumonia->Reset();
  m_Controls->PulmonaryFibrosis->Reset();
  m_Controls->PulmonaryShunt->Reset();

  m_Controls->CWAcuteRespiratoryDistressSyndrome->expand(false);
  m_Controls->CWChronicObstructivePulmonaryDisease->expand(false);
  m_Controls->CWChronicAnemia->expand(false);
  m_Controls->CWChronicObstructivePulmonaryDisease->expand(false);
  m_Controls->CWChronicPericardialEffusion->expand(false);
  m_Controls->CWChronicRenalStenosis->expand(false);
  m_Controls->CWChronicVentricularSystolicDysfunction->expand(false);
  m_Controls->CWImpairedAlveolarExchange->expand(false);
  m_Controls->CWPneumonia->expand(false);
  m_Controls->CWPulmonaryFibrosis->expand(false);
  m_Controls->CWPulmonaryShunt->expand(false);
}

void QConditionsEditorWidget::EnableInput(bool b)
{
  EnableConverter(!b);
  m_Controls->AcuteRespiratoryDistressSyndrome->SetEnabled(b);
  m_Controls->ChronicAnemia->SetEnabled(b);
  m_Controls->ChronicObstructivePulmonaryDisease->SetEnabled(b);
  m_Controls->ChronicPericardialEffusion->SetEnabled(b);
  m_Controls->ChronicRenalStenosis->SetEnabled(b);
  m_Controls->ChronicVentricularSystolicDysfunction->SetEnabled(b);
  m_Controls->ImpairedAlveolarExchange->SetEnabled(b);
  m_Controls->Pneumonia->SetEnabled(b);
  m_Controls->PulmonaryFibrosis->SetEnabled(b);
  m_Controls->PulmonaryShunt->SetEnabled(b);
  m_Controls->ClearConditions->setEnabled(b);
}

void QConditionsEditorWidget::EnableConverter(bool b)
{
  m_Controls->AcuteRespiratoryDistressSyndrome->EnableConverter(b);
  m_Controls->ChronicAnemia->EnableConverter(b);
  m_Controls->ChronicObstructivePulmonaryDisease->EnableConverter(b);
  m_Controls->ChronicPericardialEffusion->EnableConverter(b);
  m_Controls->ChronicRenalStenosis->EnableConverter(b);
  m_Controls->ChronicVentricularSystolicDysfunction->EnableConverter(b);
  m_Controls->ImpairedAlveolarExchange->EnableConverter(b);
  m_Controls->Pneumonia->EnableConverter(b);
  m_Controls->PulmonaryFibrosis->EnableConverter(b);
  m_Controls->PulmonaryShunt->EnableConverter(b);
}

void QConditionsEditorWidget::ControlsToConditions(SEConditionManager& mgr, SESubstanceManager& subMgr)
{
  const SECondition* c;
  mgr.Clear();
  // Pull Each Condition if active
  if (m_Controls->AcuteRespiratoryDistressSyndrome->IsSelected())
  {
    c = &m_Controls->AcuteRespiratoryDistressSyndrome->GetCondition();
    if (c->IsActive())
      mgr.Copy(*c, subMgr);
  }
  if (m_Controls->ChronicAnemia->IsSelected())
  {
    c = &m_Controls->ChronicAnemia->GetCondition();
    if (c->IsActive())
      mgr.Copy(*c, subMgr);
  }
  if (m_Controls->ChronicObstructivePulmonaryDisease->IsSelected())
  {
    c = &m_Controls->ChronicObstructivePulmonaryDisease->GetCondition();
    if (c->IsActive())
      mgr.Copy(*c, subMgr);
  }
  if (m_Controls->ChronicPericardialEffusion->IsSelected())
  {
    c = &m_Controls->ChronicPericardialEffusion->GetCondition();
    if (c->IsActive())
      mgr.Copy(*c, subMgr);
  }
  if (m_Controls->ChronicRenalStenosis->IsSelected())
  {
    c = &m_Controls->ChronicRenalStenosis->GetCondition();
    if (c->IsActive())
      mgr.Copy(*c, subMgr);
  }
  if (m_Controls->ChronicVentricularSystolicDysfunction->IsSelected())
  {
    c = &m_Controls->ChronicVentricularSystolicDysfunction->GetCondition();
    if (c->IsActive())
      mgr.Copy(*c, subMgr);
  }
  if (m_Controls->ImpairedAlveolarExchange->IsSelected())
  {
    c = &m_Controls->ImpairedAlveolarExchange->GetCondition();
    if (c->IsActive())
      mgr.Copy(*c, subMgr);
  }
  if (m_Controls->Pneumonia->IsSelected())
  {
    c = &m_Controls->Pneumonia->GetCondition();
    if (c->IsActive())
      mgr.Copy(*c, subMgr);
  }
  if (m_Controls->PulmonaryFibrosis->IsSelected())
  {
    c = &m_Controls->PulmonaryFibrosis->GetCondition();
    if (c->IsActive())
      mgr.Copy(*c, subMgr);
  }
  if (m_Controls->PulmonaryShunt->IsSelected())
  {
    c = &m_Controls->PulmonaryShunt->GetCondition();
    if (c->IsActive())
      mgr.Copy(*c, subMgr);
  }
}


void QConditionsEditorWidget::ConditionsToControls(SEConditionManager const& mgr)
{
  if (mgr.HasAcuteRespiratoryDistressSyndrome())
  {
    m_Controls->AcuteRespiratoryDistressSyndrome->SetCondition(*mgr.GetAcuteRespiratoryDistressSyndrome());
    m_Controls->CWAcuteRespiratoryDistressSyndrome->expand(true);
  }
  if (mgr.HasChronicAnemia())
  {
    m_Controls->ChronicAnemia->SetCondition(*mgr.GetChronicAnemia());
    m_Controls->CWChronicAnemia->expand(true);
  }
  if (mgr.HasChronicObstructivePulmonaryDisease())
  {
    m_Controls->ChronicObstructivePulmonaryDisease->SetCondition(*mgr.GetChronicObstructivePulmonaryDisease());
    m_Controls->CWChronicObstructivePulmonaryDisease->expand(true);
  }
  if (mgr.HasChronicPericardialEffusion())
  {
    m_Controls->ChronicPericardialEffusion->SetCondition(*mgr.GetChronicPericardialEffusion());
    m_Controls->CWChronicPericardialEffusion->expand(true);
  }
  if (mgr.HasChronicRenalStenosis())
  {
    m_Controls->ChronicRenalStenosis->SetCondition(*mgr.GetChronicRenalStenosis());
    m_Controls->CWChronicRenalStenosis->expand(true);
  }
  if (mgr.HasChronicVentricularSystolicDysfunction())
  {
    m_Controls->ChronicVentricularSystolicDysfunction->SetCondition(*mgr.GetChronicVentricularSystolicDysfunction());
    m_Controls->CWChronicVentricularSystolicDysfunction->expand(true);
  }
  if (mgr.HasImpairedAlveolarExchange())
  {
    m_Controls->ImpairedAlveolarExchange->SetCondition(*mgr.GetImpairedAlveolarExchange());
    m_Controls->CWImpairedAlveolarExchange->expand(true);
  }
  if (mgr.HasPneumonia())
  {
    m_Controls->Pneumonia->SetCondition(*mgr.GetPneumonia());
    m_Controls->CWPneumonia->expand(true);
  }
  if (mgr.HasPulmonaryFibrosis())
  {
    m_Controls->PulmonaryFibrosis->SetCondition(*mgr.GetPulmonaryFibrosis());
    m_Controls->CWPulmonaryFibrosis->expand(true);
  }
  if (mgr.HasPulmonaryShunt())
  {
    m_Controls->PulmonaryShunt->SetCondition(*mgr.GetPulmonaryShunt());
    m_Controls->CWPulmonaryShunt->expand(true);
  }
}

void QConditionsEditorWidget::AtSteadyState(PhysiologyEngine& pulse)
{

}
void QConditionsEditorWidget::AtSteadyStateUpdateUI()
{
  EnableInput(false);
  // Pull out conditions and set the data in the GUI
}

