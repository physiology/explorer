/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#ifndef AddPopup_H
#define AddPopup_H

#include <qdialog.h>
#include <QMainWindow>
#include <QObject>
#include "QPulse.h"


namespace Ui {
	class AddPopup;
}

class AddPopup : public QDialog
{
	Q_OBJECT

public:
	explicit AddPopup(std::string name,QWidget * parent = nullptr);

	virtual ~AddPopup();



	Ui::AddPopup *Ui;

//protected:
signals:
	void insertG(QString substance, double amount);

signals:
	void insertA(QString substance, double amount);

protected slots:
	void add();

private:
	class Controls;
	Controls* m_Controls;
};

#endif // AddPopup_H
