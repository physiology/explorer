/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <qdialog.h>
#include <QMainWindow>
#include <QObject>

namespace Ui {
  class ScenarioSave;
}

class ScenarioSaveDialog : public QDialog
{
  Q_OBJECT
public:
  enum class Mode {Patient, State};
  ScenarioSaveDialog(Mode m,
                     std::string const& scenarioFilePath,
                     std::string const& separateFilePath,
                     QWidget * parent = nullptr);
  virtual ~ScenarioSaveDialog();

  QString GetScenarioFilename();
  bool SeparateScenario();
  QString GetSeparateFilename();

protected slots:
  void BrowseForScenarioSaveFile();
  void BrowseForSeparateSaveFile();
  void EnableSeparateFile();

private:
  class Controls;
  Controls* m_Controls;
};
