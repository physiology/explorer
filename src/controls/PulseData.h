/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/

#pragma once

#include <vector>
#include <QString>

/// Central location for static data that is related to data fields and names 
/// from pulse declare and define here and use throughout PulseExplorer. 
namespace PulseData
{

static std::vector<QString> Drugs = { "Epinephrine", "Etomidate", "Fentanyl","Furosemide",
                               "Ketamine", "Lorazepam", "Midazolam","Morphine",
                               "Naloxone", "Norepinephrine", "Oversedation", "Phenylephrine",
                              "Pralidoxime","Prednisone", "Propofol", "Rocuronium","Succinylcholine" };

}