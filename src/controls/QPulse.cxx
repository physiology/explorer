/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/

#include "QPulse.h"

#include <QThread>
#include <QPointer>
#include <QCoreApplication>
#include <QScrollBar>
#include <QMutex>
#include <QMessageBox>
#include <QFileDialog>
#include <thread>

#ifdef Q_OS_MAC
  #include <pwd.h>
  #include <unistd.h>
#endif

#include "pulse/engine/PulseEngine.h"
#include "pulse/cdm/scenario/SEScenario.h"
#include "pulse/cdm/engine/SEAdvanceTime.h"
#include "pulse/cdm/engine/SEEngineTracker.h"
#include "pulse/cdm/engine/SEPatientConfiguration.h"
#include "pulse/cdm/engine/SECondition.h"
#include "pulse/cdm/engine/SEConditionManager.h"
#include "pulse/cdm/patient/SEPatient.h"
#include "pulse/cdm/properties/SEScalar0To1.h"
#include "pulse/cdm/properties/SEScalarArea.h"
#include "pulse/cdm/properties/SEScalarFrequency.h"
#include "pulse/cdm/properties/SEScalarLength.h"
#include "pulse/cdm/properties/SEScalarMass.h"
#include "pulse/cdm/properties/SEScalarMassPerVolume.h"
#include "pulse/cdm/properties/SEScalarPower.h"
#include "pulse/cdm/properties/SEScalarPressure.h"
#include "pulse/cdm/properties/SEScalarTime.h"
#include "pulse/cdm/properties/SEScalarVolume.h"
#include "pulse/cdm/utils/FileUtils.h"
#include "pulse/cdm/utils/TimingProfile.h"

Q_DECLARE_METATYPE(std::vector<SEAction const*>);

class LoggerForward2Qt : public LoggerForward
{
public:
  LoggerForward2Qt() {}
  virtual ~LoggerForward2Qt() {}
  void ForwardDebug(const std::string& msg) override { msgs.push_back(msg); }
  void ForwardInfo(const std::string& msg)  override
  { 
    for (std::string str : IgnoreActions)
    {
      if (msg.find(str) != str.npos)
        return;
    }
    if (msg.find("[Action]") != std::string::npos)
    {
      std::string pp = "[INFO] " + msg.substr(0, msg.find("\n"));
      pp += SEAction::PrettyPrint(msg);
      msgs.push_back(pp);
    }
    else if (msg.find("[Condition]") != std::string::npos)
    {
      std::string pp = "[INFO] " + msg.substr(0, msg.find("\n"));
      pp += SECondition::PrettyPrint(msg);
      msgs.push_back(pp);
    }
    else
      msgs.push_back(msg);

    
  }
  void ForwardWarning(const std::string& msg) override { msgs.push_back(msg); }
  void ForwardError(const std::string& msg)   override { msgs.push_back("[ERROR] " + msg); }
  void ForwardFatal(const std::string& msg)   override { msgs.push_back("[ERROR] " + msg); }

  std::vector<std::string> IgnoreActions;
  std::vector<std::string> msgs;
};

class QPulse::Controls
{
public:
  Controls(QThread& thread, QTextEdit* logEdit) : Thread(thread)
  {
    Pulse = CreatePulseEngine();
    Pulse->GetLogger()->SetLogFile(QPulse::GetDataDir().toStdString()+"/PulseExplorer.log");
    if (logEdit != nullptr)
    {
      LogBox = logEdit;
      Pulse->GetLogger()->AddForward(&Log2Qt);
    }
    
  }
  virtual ~Controls()
  {
    delete Scenario;
  }

  // We only want to generate a scenario if it is actually needed
  PulseScenario* GetScenario()
  {
      if (Scenario == nullptr)
          Scenario = new PulseScenario(Pulse->GetLogger());

      return Scenario;
  }

  std::unique_ptr<PhysiologyEngine>              Pulse;
  LoggerForward2Qt                               Log2Qt;
  QThread&                                       Thread;
  QTextEdit*                                     LogBox;
  QMutex                                         Mutex;
  TimingProfile                                  Timer;
  ExplorerMode                                   Mode = ExplorerMode::Editor;
  std::atomic<bool>                              Running{false};
  std::atomic<bool>                              Paused{false};
  std::atomic<bool>                              RunInRealtime{true};
  std::atomic<bool>                              Advancing{false};
  double                                         AdvanceStep_s;

  std::string                                    EngineStateFile="";
  std::string                                    PatientFile = "";
  std::string                                    ScenarioFile = "";
  std::vector<PulseListener*>                    Listeners;
  std::map<size_t, std::vector<SEAction const*>> Actions;// Key is sim time in seconds to process the action vector
  std::vector<SEAction const*> AppliedActions;
  static QString                                 DataDir;
  static QString                                 SaveDir;

private:
  PulseScenario* Scenario = nullptr;
};

QPulse::QPulse(QThread& thread, QTextEdit* log) : QObject(), SEAdvanceHandler(true)
{
  m_Controls = new Controls(thread,log);
  m_Controls->Pulse->SetAdvanceHandler(this);


  qRegisterMetaType<std::vector<SEAction const*>>();

  connect(this, SIGNAL(AtSteadyState()), SLOT(AtSteadyStateUpdateUI()));
  connect(this, SIGNAL(Advanced(std::vector<SEAction const*>)), SLOT(PhysiologyUpdateUI(std::vector<SEAction const*>)));
  connect(this, SIGNAL(EngineError()), SLOT(EngineErrorUI()));
}

QPulse::~QPulse()
{
  Stop();
  delete m_Controls;
}

void QPulse::LogToGUI(QString msg)
{
  if(m_Controls->LogBox != nullptr)
    m_Controls->LogBox->append(msg);
}
void QPulse::ScrollLogBox()
{
  if (m_Controls->LogBox != nullptr)
  {
    QScrollBar* sb = m_Controls->LogBox->verticalScrollBar();
    sb->setValue(sb->maximum());
    m_Controls->LogBox->update();
  }
}

void QPulse::IgnoreAction(const std::string& name)
{
  m_Controls->Log2Qt.IgnoreActions.push_back(name);
}

PhysiologyEngine& QPulse::GetEngine()
{
  return *m_Controls->Pulse;
}

double QPulse::GetTimeStep_s()
{
  return m_Controls->AdvanceStep_s;
}

void QPulse::Start()
{
  Worker* worker = new Worker(*this);
  worker->moveToThread(&m_Controls->Thread);
  connect(&m_Controls->Thread, SIGNAL(started()), worker, SLOT(Work()));
  connect(&m_Controls->Thread, SIGNAL(finished()), worker, SLOT(deleteLater()));
  m_Controls->Thread.start();
}
void Worker::Work()
{
  _qpulse.AdvanceTime();
}

void QPulse::Stop()
{
  if (m_Controls->Thread.isRunning())
  {
    QEventLoop loop;
    connect(&m_Controls->Thread, &QThread::finished, &loop, &QEventLoop::quit);
    m_Controls->Running = false;
    m_Controls->Thread.quit();
    loop.exec();
  }
}

bool QPulse::IsRunning()
{
  return m_Controls->Running;
}

void QPulse::Clear()
{
  Stop();
  m_Controls->EngineStateFile = "";
  m_Controls->PatientFile = "";
  m_Controls->ScenarioFile = "";
  m_Controls->Running = false;
  m_Controls->Paused = false;
  m_Controls->RunInRealtime = true;
  m_Controls->Log2Qt.IgnoreActions.clear();
  while(m_Controls->Advancing)
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
  if (m_Controls->GetScenario() != nullptr) m_Controls->GetScenario()->Clear();
  m_Controls->Pulse->GetEngineTracker()->Clear();
}

void QPulse::Reset()
{
  Stop();
  m_Controls->Running = false;
  m_Controls->Paused = false;
  m_Controls->RunInRealtime = true;
  while (m_Controls->Advancing)
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

ExplorerMode QPulse::GetMode() { return m_Controls->Mode; }
void QPulse::SetMode(ExplorerMode m) { m_Controls->Mode = m; }

bool QPulse::IsPaused()
{
  return m_Controls->Paused;
}
bool QPulse::PlayPause()
{
  if (m_Controls->Thread.isRunning())
    m_Controls->Paused = !m_Controls->Paused;
  return m_Controls->Paused;
}

bool QPulse::ToggleRealtime()
{
  if (m_Controls->Thread.isRunning())
    m_Controls->RunInRealtime = !m_Controls->RunInRealtime;
  return m_Controls->RunInRealtime;
}


void QPulse::RegisterListener(PulseListener* l)
{
  if (l == nullptr)
    return;
  QMutexLocker lock(&m_Controls->Mutex);
  auto itr = std::find(m_Controls->Listeners.begin(), m_Controls->Listeners.end(), l);
  if (itr == m_Controls->Listeners.end())
    m_Controls->Listeners.push_back(l);
}

void QPulse::RemoveListener(PulseListener* l)
{
  QMutexLocker lock(&m_Controls->Mutex);
  auto itr = std::find(m_Controls->Listeners.begin(), m_Controls->Listeners.end(), l);
  if (itr != m_Controls->Listeners.end())
    m_Controls->Listeners.erase(itr);
}

PulseScenario& QPulse::GetScenario()
{
  return *m_Controls->GetScenario();
}

SEPatientConfiguration& QPulse::GetPatientConfiguration()
{
  return m_Controls->GetScenario()->GetPatientConfiguration();
}

PulseConfiguration& QPulse::GetPulseConfiguration()
{
  return m_Controls->GetScenario()->GetConfiguration();
}

void QPulse::AddScenarioAction(ushort time_s, SEAction const& a)
{
  m_Controls->Actions[time_s].push_back(&a);
}

void QPulse::AdvanceTime()
{
  std::stringstream ss;
  double count_s = 0;
  m_Controls->Advancing = true;
  m_Controls->Timer.Start("ui");
  if (m_Controls->EngineStateFile.empty())
  {
    GetPatientConfiguration().SetDataRoot(QPulse::Controls::DataDir.toStdString());
    bool stable = m_Controls->Pulse->InitializeEngine(GetPatientConfiguration());
    if (!stable)
    {
      m_Controls->Running = false;
      m_Controls->Advancing = false;
      emit EngineError();
      return;
    }
  }
  m_Controls->Actions.clear();
  m_Controls->AppliedActions.clear();

  // Get the time step after we initialize, it can be changed by the configuration
  m_Controls->AdvanceStep_s = m_Controls->Pulse->GetTimeStep(TimeUnit::s);
  // Notify listeners we are starting (we stabilized if we needed to)
  m_Controls->Mutex.lock();
  for (PulseListener* l : m_Controls->Listeners)
    l->AtSteadyState(*m_Controls->Pulse);
  m_Controls->Mutex.unlock();
  emit AtSteadyState();
  // Give sometime to Qt to process this signal
  // We may want to just set the pause, and at the end
  // of processing the signal, unpause it.. 
  std::this_thread::sleep_for(std::chrono::seconds(1));

  // Set up the main procesing thread
  m_Controls->Running = true;
  // Set up timing variables
  std::chrono::time_point<std::chrono::steady_clock> begin;
  std::chrono::milliseconds   timePerFrame{ int(m_Controls->AdvanceStep_s*1000) };
  std::chrono::duration<double> dt(m_Controls->AdvanceStep_s);
  ushort next_s = 0;
  while (m_Controls->Running)
  {
    begin = std::chrono::steady_clock::now();
    if (m_Controls->Paused)
    {
      std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    else
    {
      try
      {
        ushort now_s = std::floor(m_Controls->Pulse->GetSimulationTime(TimeUnit::s));
        if (now_s == next_s)
        {
          auto actions = m_Controls->Actions.find(next_s);
          if (actions != m_Controls->Actions.end())
          {
            for (SEAction const* a : actions->second)
            {
              m_Controls->Pulse->ProcessAction(*a);
            }
            // Store actions for processing here
            std::copy(actions->second.cbegin(), actions->second.cend(), std::back_inserter(m_Controls->AppliedActions));
            m_Controls->Actions.erase(next_s);
          }
          next_s++;
        }
        if (!m_Controls->Pulse->AdvanceModelTime(dt.count(), TimeUnit::s))
        {
           m_Controls->Running = false;
           emit EngineError();
        }
      }
      catch (const CommonDataModelException& ex)
      {
        m_Controls->Running = false;
        m_Controls->Pulse->GetLogger()->Info("Pulse Error : " + std::string(ex.what()));
        emit EngineError();
      }
      catch (const std::exception& e)
      {
        m_Controls->Running = false;
        m_Controls->Pulse->GetLogger()->Info("Runtime Error : " + std::string(e.what()));
        emit EngineError();
      }

      auto stepDuration = std::chrono::steady_clock::now() - begin;
      if (m_Controls->RunInRealtime && stepDuration < timePerFrame)
      {
        auto nap = timePerFrame - stepDuration;
        std::this_thread::sleep_for(nap);
        dt = std::chrono::steady_clock::now() - begin;
      }
      else // TODO (AB) Things are running slower than we want... what should we do?
      {
        dt = timePerFrame;
      }
    }
  }
  m_Controls->Advancing = false;
}

void QPulse::OnAdvance(double time_s)
{
  if (m_Controls->Running)
  {
    QMutexLocker lock(&m_Controls->Mutex);
    for (PulseListener* l : m_Controls->Listeners)
    {
        l->ProcessPhysiology(*m_Controls->Pulse);
    }
  }

 
  if (m_Controls->Timer.GetElapsedTime_s("ui") > 0.1)
  {
    emit Advanced(m_Controls->AppliedActions);// Only update the UI every 0.1 seconds
    m_Controls->AppliedActions.clear();
    m_Controls->Timer.Start("ui");// Reset our timer
  }
}

void QPulse::AtSteadyStateUpdateUI()
{
  m_Controls->Mutex.lock();
  for (PulseListener* l : m_Controls->Listeners)
    l->AtSteadyStateUpdateUI();
  m_Controls->Mutex.unlock();
}
void QPulse::PhysiologyUpdateUI(std::vector<SEAction const*> actions)
{
  m_Controls->Mutex.lock();
  if (m_Controls->LogBox != nullptr)
  {
    for (std::string msg : m_Controls->Log2Qt.msgs)
      m_Controls->LogBox->append(QString(msg.c_str()));
    m_Controls->Log2Qt.msgs.clear();
  }

  if (m_Controls->Running)
  {
    for (PulseListener* l : m_Controls->Listeners)
      l->PhysiologyUpdateUI(actions);
  }
  m_Controls->Mutex.unlock();
}
void QPulse::EngineErrorUI()
{
  m_Controls->Mutex.lock();
  if (m_Controls->LogBox != nullptr)
  {
    for (std::string msg : m_Controls->Log2Qt.msgs)
      m_Controls->LogBox->append(QString(msg.c_str()));
    m_Controls->Log2Qt.msgs.clear();
  }

  for (PulseListener* l : m_Controls->Listeners)
    l->EngineErrorUI();
  m_Controls->Mutex.unlock();
}

std::string QPulse::GetEngineStateFilename()
{
  return m_Controls->EngineStateFile;
}
void QPulse::SetEngineStateFilename(std::string const& filename)
{
  m_Controls->PatientFile = "";
  m_Controls->EngineStateFile = filename;
}

std::string QPulse::GetPatientFilename()
{
  return m_Controls->PatientFile;
}
void QPulse::SetPatientFilename(std::string const& filename)
{
  m_Controls->PatientFile = filename;
  m_Controls->EngineStateFile = "";
}

std::string QPulse::GetScenarioFilename()
{
  return m_Controls->ScenarioFile;
}
void QPulse::SetScenarioFilename(std::string const& filename)
{
  m_Controls->ScenarioFile = filename;
}

QString QPulse::Controls::DataDir = "";
QString QPulse::GetDataDir()
{
  if (QPulse::Controls::DataDir.isEmpty())
  {
#ifdef Q_OS_MAC
  // Do this logic if we are in the app
  if(!QCoreApplication::applicationDirPath().contains("Innerbuild"))
    QPulse::Controls::DataDir = QCoreApplication::applicationDirPath()+"/../Pulse";
  else
    QPulse::Controls::DataDir = ".";
#else
    QPulse::Controls::DataDir = ".";
#endif
  }
  return QPulse::Controls::DataDir;
}

QString QPulse::Controls::SaveDir = "";
QString QPulse::GetSaveDir()
{
  if (QPulse::Controls::SaveDir.isEmpty())
  {
#ifdef Q_OS_MAC
  // Do this logic if we are in the app
  if(!QCoreApplication::applicationDirPath().contains("Innerbuild"))
  {
    const char* homeDir = getenv("HOME");
    if (!homeDir)
    {
      struct passwd* pwd = getpwuid(getuid());
      if (pwd)
        homeDir = pwd->pw_dir;
    }
    std::string pulseDir = "/";// Cannot save inside the pkg/dmg
    if(homeDir)
      pulseDir = std::string(homeDir);
    QPulse::Controls::SaveDir = QString::fromStdString(pulseDir)+"/PulseExplorer/";
    CreatePath(GetSaveStatesDir().toStdString());
    CreatePath(GetSavePatientsDir().toStdString());
    CreatePath(GetSaveScenariosDir().toStdString());
    CreatePath(GetSaveEnvironmentsDir().toStdString());
  }
  else
    QPulse::Controls::SaveDir = "./";
#else
    QPulse::Controls::SaveDir = "./";
#endif
  }
  return QPulse::Controls::SaveDir;
}
QString QPulse::GetSavePatientsDir() {return QPulse::GetSaveDir()+"patients";}
QString QPulse::GetSaveScenariosDir() {return QPulse::GetSaveDir()+"scenarios";}
QString QPulse::GetSaveStatesDir() {return QPulse::GetSaveDir()+"states";}
QString QPulse::GetSaveEnvironmentsDir() {return QPulse::GetSaveDir()+"states";}

QString QPulse::AttemptRelativePath(QString filepath)
{
  std::string rp = QPulse::GetSaveDir().replace('\\', '/').toStdString();
  if (rp.find("./")==0)
  {
    // Let's try to make the separate file relative to the working directory
    std::string cwd = GetCurrentWorkingDirectory();
    std::replace(cwd.begin(), cwd.end(), '\\', '/');
    QStringList pieces = filepath.split(QString::fromStdString(cwd));
    if (pieces.size() > 1)
      filepath = "." + pieces[1];
    return filepath;
  }
  else
    return filepath;
}
