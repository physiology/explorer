/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "TimeWidget.h"
#include "ui_Time.h"


class QTimeWidget::Controls : public Ui::TimeWidget
{
public:
  Controls() {};
  SEScalarTime ProcessTime;
};

QTimeWidget::QTimeWidget(QWidget* parent, QString label, int maxWidth) : QWidget(parent)
{
  m_Controls = new Controls();
  m_Controls->setupUi(this);
  m_Controls->Label->setText(label);
  connect(m_Controls->Hours, SIGNAL(valueChanged(int)), this, SLOT(PrefixCheck()));
  connect(m_Controls->Minutes, SIGNAL(valueChanged(int)), this, SLOT(PrefixCheck()));
  connect(m_Controls->Seconds, SIGNAL(valueChanged(int)), this, SLOT(PrefixCheck()));
  Reset();
}

void QTimeWidget::Reset()
{
  m_Controls->Hours->setValue(0);
  m_Controls->Minutes->setValue(0);
  m_Controls->Seconds->setValue(0);
  m_Controls->Hours->setPrefix("0");
  m_Controls->Minutes->setPrefix("0");
  m_Controls->Seconds->setPrefix("0");
}

void QTimeWidget::SetEnabled(bool b)
{
  //ComboBox->setEnabled(b);
}

void QTimeWidget::PrefixCheck()
{
  QSpinBox* sb = (QSpinBox*)QObject::sender();
  if(sb->value() >= 10)
    sb->setPrefix("");
  else
    sb->setPrefix("0");
}

void QTimeWidget::SetTime(SEScalarTime const& t)
{
  int s = (int)t.GetValue(TimeUnit::s);
  int min = s / 60;
  int hr = min / 60;
  min = int(min % 60);
  s = int(s % 60);
  m_Controls->Hours->setPrefix(hr > 9?"":"0");
  m_Controls->Hours->setValue(hr);
  m_Controls->Minutes->setPrefix(min > 9 ? "" : "0");
  m_Controls->Minutes->setValue(min);
  m_Controls->Seconds->setPrefix(s > 9 ? "" : "0");
  m_Controls->Seconds->setValue(s);
}

void QTimeWidget::GetTime(SEScalarTime& t)
{
  double time_s = 0;
  time_s += m_Controls->Hours->value() * 3600;
  time_s += m_Controls->Minutes->value() * 60;
  time_s += m_Controls->Seconds->value();
  t.SetValue(time_s, TimeUnit::s);
}
