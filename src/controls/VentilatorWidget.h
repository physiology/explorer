/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/
#pragma once

#include <QWidget>
#include "QPulse.h"
class SEMechanicalVentilatorSettings;

namespace Ui {
class VentilatorWidget;
}

class LabeledDialWidget;

class QVentilatorWidget : public QWidget, public PulseListener
{
  Q_OBJECT
public:
  QVentilatorWidget(QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QVentilatorWidget();

  void Reset();

  void AtSteadyState(PhysiologyEngine& pulse) override;
  void ProcessPhysiology(PhysiologyEngine& pulse) override;
  void AtSteadyStateUpdateUI() override;
  void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override;
  void EngineErrorUI() override;

  void StartEngine();


signals:
  void UpdateAction(SEAction const&, SEScalarTime const&);

protected slots:
  /// Used to update the slope widget with a new range
  void UpdateSlope(double value);

  /// On apply take the current mode and input parameters and
  /// apply an action to pulse
  void ApplyAction();

  /// Disconnect the ventilator
  void DisconnectAction();

  ///@{
  /// Handle holds
  void AddHold();
  void RemoveHold();
  ///@}

  void SetMode(QString val);
  void ResetDials();

  void EnableFlowTrigger();
  void EnablePressureTrigger();
  void EnableModelTrigger();
  void ApplyTriggers();
  void ApplyDrugs();

protected:
  void SetupNumbers();
  void UpdateModeUI();
  void UpdateSettingsUI();
  void PullSupplementalSettings(SEMechanicalVentilatorSettings& ss);

  ///@{
  /// When hold is clicked, the UI should be locked for input
  void LockForHold();
  void Unlock();

private:
  struct Private;
  Private* d;
};
