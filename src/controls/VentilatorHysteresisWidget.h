/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/
#pragma once

#include <memory>

#include <QWidget>
#include "QPulse.h"

class QVentilatorHysteresisWidget : public QWidget, public PulseListener
{
  Q_OBJECT
public:
  QVentilatorHysteresisWidget(QWidget* parent, Qt::WindowFlags flags);
  virtual ~QVentilatorHysteresisWidget();

  void AtSteadyState(PhysiologyEngine& pulse) override;
  void AtSteadyStateUpdateUI() override;
  void ProcessPhysiology(PhysiologyEngine& pulse) override;
  void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override;
  void EngineErrorUI() override;

private:
  struct Private;
  Private* d;
};
