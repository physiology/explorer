/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QWidget>

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>

class QwtPulsePlotLoop : public QWidget
{
    Q_OBJECT
public:
  
  QwtPulsePlotLoop(QWidget* parent=nullptr, int max_size=300);
  virtual ~QwtPulsePlotLoop();

  void Clear();

  QwtPlot& GetPlot();
  QwtPlotCurve& GetCurve();
  QwtPlotGrid& GetGrid();

  void SetMaxPoints(size_t max);
  void SetAspectRatio(double value);
  void Append(double time, double value);
  void UpdateUI(bool pad=true);

private:
  class Data;
  Data* d;
};
