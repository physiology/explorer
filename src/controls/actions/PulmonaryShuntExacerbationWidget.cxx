/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/PulmonaryShuntExacerbationWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QPulmonaryShuntExacerbationWidget::Controls // based off of chronic anemia widget
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                                   Pulse;
  SEPulmonaryShuntExacerbation              Action;
  QScalarWidget*                            Severity;
};

QPulmonaryShuntExacerbationWidget::QPulmonaryShuntExacerbationWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->Severity = new QScalarWidget("Severity", 0, 1, 0.1, ScalarOptionWidget::None, this); //check this
  Properties()->layout()->addWidget(m_Controls->Severity);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QPulmonaryShuntExacerbationWidget::~QPulmonaryShuntExacerbationWidget()
{
  delete m_Controls;
}

void QPulmonaryShuntExacerbationWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->Severity->Reset();
}

SEAction& QPulmonaryShuntExacerbationWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QPulmonaryShuntExacerbationWidget::GetAction() const
{
  return m_Controls->Action;
}

void QPulmonaryShuntExacerbationWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->Severity->EnableInput(b);
  m_Controls->Severity->setEnabled(b);
}

void QPulmonaryShuntExacerbationWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  m_Controls->Severity->GetValue(m_Controls->Action.GetSeverity());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QPulmonaryShuntExacerbationWidget::ActionToControls(const SEPulmonaryShuntExacerbation& action)
{
  QActionWidget::ActionToControls(action);
  SEScalar data;
  data.SetValue(action.GetSeverity());
  m_Controls->Severity->SetValue(data);
}
