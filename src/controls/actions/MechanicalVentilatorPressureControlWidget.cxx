/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/
#include "controls/actions/MechanicalVentilatorPressureControlWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"
#include "controls/LabeledComboBox.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"
#include "pulse/cdm/properties/SEScalarFrequency.h"
#include "pulse/cdm/properties/SEScalarPressure.h"
#include "pulse/cdm/properties/SEScalarTime.h"
#include "pulse/cdm/properties/SEScalarVolume.h"
#include "pulse/cdm/properties/SEScalarVolumePerPressure.h"
#include "pulse/cdm/properties/SEScalarVolumePerTime.h"


class QMechanicalVentilatorPressureControlWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {
  }
  QPulse&               Pulse;
  SEMechanicalVentilatorPressureControl Action;
  SEMechanicalVentilatorPressureControl DefaultAction;
  QLabeledComboBox* State;
  QLabeledComboBox* Mode;
  QScalarWidget* FractionInspiredOxygen;
  QScalarQuantityWidget<PressureUnit>* InspiratoryPressure;
  QScalarQuantityWidget<TimeUnit>* InspiratoryPeriod;
  QScalarQuantityWidget<FrequencyUnit>* RespirationRate;
  QScalarQuantityWidget<PressureUnit>* PositiveEndExpiredPressure;
  QScalarQuantityWidget<TimeUnit>* Slope;
};

QMechanicalVentilatorPressureControlWidget::QMechanicalVentilatorPressureControlWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);

  std::vector<QString> stateOptions = { "On", "Off" };
  m_Controls->State = new QLabeledComboBox(this, "State", stateOptions);
  Properties()->layout()->addWidget(m_Controls->State);

  std::vector<QString> modeOptions = { "AC", "CMV" };
  m_Controls->Mode = new QLabeledComboBox(this, "Mode", modeOptions);
  Properties()->layout()->addWidget(m_Controls->Mode);

  m_Controls->FractionInspiredOxygen = new QScalarWidget("FiO2", .21, 1.0, 0.01, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->FractionInspiredOxygen);

  m_Controls->InspiratoryPressure = new QScalarQuantityWidget<PressureUnit>("Pinsp", 1, 100, 1, PressureUnit::cmH2O, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->InspiratoryPressure);

  m_Controls->InspiratoryPeriod = new QScalarQuantityWidget<TimeUnit>("Ti", 0.1, 60, .1, TimeUnit::s, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->InspiratoryPeriod);

  m_Controls->RespirationRate = new QScalarQuantityWidget<FrequencyUnit>("RR", 10, 60, 1, FrequencyUnit::Per_min, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->RespirationRate);

  m_Controls->PositiveEndExpiredPressure = new QScalarQuantityWidget<PressureUnit>("PEEP", 0, 50, 1, PressureUnit::cmH2O, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->PositiveEndExpiredPressure);

  // Note Slope should be in the range of 0-Ti rather than free
  m_Controls->Slope = new QScalarQuantityWidget<TimeUnit>("Slope", 0.1, 60, 1, TimeUnit::s, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->Slope);


  Properties()->layout()->addWidget(GetProcessTimeCtrl());


  m_Controls->DefaultAction.SetConnection(eSwitch::On);
  m_Controls->DefaultAction.GetFractionInspiredOxygen().SetValue(.21);
  m_Controls->DefaultAction.GetInspiratoryPressure().SetValue(13.0, PressureUnit::cmH2O);
  m_Controls->DefaultAction.GetInspiratoryPeriod().SetValue(1.0, TimeUnit::s);
  m_Controls->DefaultAction.GetRespirationRate().SetValue(12, FrequencyUnit::Per_min);
  m_Controls->DefaultAction.GetPositiveEndExpiratoryPressure().SetValue(5.0, PressureUnit::cmH2O);
  m_Controls->DefaultAction.GetSlope().SetValue(0.2, TimeUnit::s);

  Reset();
}

QMechanicalVentilatorPressureControlWidget::~QMechanicalVentilatorPressureControlWidget()
{
  delete m_Controls;
}

void QMechanicalVentilatorPressureControlWidget::Reset()
{
  QActionWidget::Reset();

  m_Controls->Action.Clear();
  ActionToControls(m_Controls->DefaultAction);
}

SEAction& QMechanicalVentilatorPressureControlWidget::GetAction()
{
  return m_Controls->Action;
}

const SEAction& QMechanicalVentilatorPressureControlWidget::GetAction() const
{
  return m_Controls->Action;
}

void QMechanicalVentilatorPressureControlWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->State->setEnabled(b);
  m_Controls->Mode->setEnabled(b);
  m_Controls->FractionInspiredOxygen->EnableInput(b);
  m_Controls->InspiratoryPressure->EnableInput(b);
  m_Controls->InspiratoryPeriod->EnableInput(b);
  m_Controls->RespirationRate->EnableInput(b);
  m_Controls->PositiveEndExpiredPressure->EnableInput(b);
  m_Controls->Slope->EnableInput(b);
}

void QMechanicalVentilatorPressureControlWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();

  m_Controls->Action.SetConnection((m_Controls->State->GetIndex() == 0) ? eSwitch::On : eSwitch::Off);
  m_Controls->Action.SetMode((m_Controls->Mode->GetIndex() == 0) ?
      eMechanicalVentilator_PressureControlMode::AssistedControl : eMechanicalVentilator_PressureControlMode::ContinuousMandatoryVentilation);
  m_Controls->FractionInspiredOxygen->GetValue(m_Controls->Action.GetFractionInspiredOxygen());
  m_Controls->InspiratoryPressure->GetValue(m_Controls->Action.GetInspiratoryPressure());
  m_Controls->InspiratoryPeriod->GetValue(m_Controls->Action.GetInspiratoryPeriod());
  m_Controls->RespirationRate->GetValue(m_Controls->Action.GetRespirationRate());
  m_Controls->PositiveEndExpiredPressure->GetValue(m_Controls->Action.GetPositiveEndExpiratoryPressure());
  m_Controls->Slope->GetValue(m_Controls->Action.GetSlope());

  emit UpdateAction(m_Controls->Action, GetProcessTime());
}
void QMechanicalVentilatorPressureControlWidget::ActionToControls(const SEMechanicalVentilatorPressureControl& action)
{
  QActionWidget::ActionToControls(action);
  m_Controls->Action.Copy(action, m_Controls->Pulse.GetScenario().GetSubstanceManager());
  m_Controls->State->SetCurrentIndex((action.GetConnection() == eSwitch::On) ? 0 : 1);
  m_Controls->Mode->SetCurrentIndex((action.GetMode() == eMechanicalVentilator_PressureControlMode::AssistedControl) ? 0 : 1);
  m_Controls->FractionInspiredOxygen->SetValue(m_Controls->Action.GetFractionInspiredOxygen());
  m_Controls->InspiratoryPressure->SetValue(m_Controls->Action.GetInspiratoryPressure());
  m_Controls->InspiratoryPeriod->SetValue(m_Controls->Action.GetInspiratoryPeriod());
  m_Controls->RespirationRate->SetValue(m_Controls->Action.GetRespirationRate());
  m_Controls->PositiveEndExpiredPressure->SetValue(m_Controls->Action.GetPositiveEndExpiratoryPressure());
  m_Controls->Slope->SetValue(m_Controls->Action.GetSlope());
}
