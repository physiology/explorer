/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QWidget>
#include "controls/actions/ActionWidget.h"
#include "pulse/cdm/patient/actions/SEImpairedAlveolarExchangeExacerbation.h"

class QImpairedAlveolarExchangeExacerbationWidget : public QActionWidget
{
  Q_OBJECT
public:
  using ActionType = SEImpairedAlveolarExchangeExacerbation;

  QImpairedAlveolarExchangeExacerbationWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QImpairedAlveolarExchangeExacerbationWidget();

  virtual void Reset() override;
  virtual void SetEnabled(bool b) override;
  virtual const SEAction& GetAction() const override;

  virtual void ControlsToAction() override;
  virtual void ActionToControls(const SEImpairedAlveolarExchangeExacerbation& action);

protected slots:
  void EnableSeverity();
  void EnableImpairedFraction();
  void EnableImpairedSurfaceArea();

protected:
  virtual SEAction& GetAction() override;
private:
  class Controls;
  Controls* m_Controls;
  
};
