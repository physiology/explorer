/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/ChestOcclusiveDressingWidget.h"
#include "controls/LabeledComboBox.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QChestOcclusiveDressingWidget::Controls // based off of chronic anemia widget
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                  Pulse;
  SEChestOcclusiveDressing Action;
  QLabeledComboBox*        Side;
  QLabeledComboBox*        State;
};

QChestOcclusiveDressingWidget::QChestOcclusiveDressingWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  std::vector<QString> sideOptions = {"Left", "Right"};
  std::vector<QString> stateOptions = { "On", "Off" };
  m_Controls->Side = new QLabeledComboBox(this, "Side", sideOptions);
  m_Controls->State = new QLabeledComboBox(this, "State", stateOptions);
  Properties()->layout()->addWidget(m_Controls->Side);
  Properties()->layout()->addWidget(m_Controls->State);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QChestOcclusiveDressingWidget::~QChestOcclusiveDressingWidget()
{
  delete m_Controls;
}

void QChestOcclusiveDressingWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
}

SEAction& QChestOcclusiveDressingWidget::GetAction()
{
  return m_Controls->Action;
}

const SEAction& QChestOcclusiveDressingWidget::GetAction() const
{
  return m_Controls->Action;
}

void QChestOcclusiveDressingWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->Side->SetEnabled(b);
  m_Controls->State->SetEnabled(b);
}

void QChestOcclusiveDressingWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  (m_Controls->Side->GetIndex() == 0) ?
    m_Controls->Action.SetSide(eSide::Left) :
    m_Controls->Action.SetSide(eSide::Right);
  (m_Controls->State->GetIndex() == 0) ?
    m_Controls->Action.SetState(eSwitch::On) :
    m_Controls->Action.SetState(eSwitch::Off);
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}
void QChestOcclusiveDressingWidget::ActionToControls(const SEChestOcclusiveDressing& action)
{
  QActionWidget::ActionToControls(action);
  QComboBox* sideBox = m_Controls->Side->GetComboBox();
  switch (action.GetSide()) {
  case eSide::Left:
    sideBox->setCurrentIndex(sideBox->findText("Left"));
    break;
  case eSide::Right: 
    sideBox->setCurrentIndex(sideBox->findText("Right"));
    break;
  default:
    sideBox->setCurrentIndex(sideBox->findText("Null"));
    break;
  }
  QComboBox* stateBox = m_Controls->State->GetComboBox();
  switch (action.GetState()) {
  case eSwitch::On:
    stateBox->setCurrentIndex(stateBox->findText("On"));
    break;
  case eSwitch::Off: 
    stateBox->setCurrentIndex(stateBox->findText("Off"));
    break;
  default:
    stateBox->setCurrentIndex(stateBox->findText("Off"));
    break;
  }
}
