/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/AcuteRespiratoryDistressSyndromeExacerbationWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QAcuteRespiratoryDistressSyndromeExacerbationWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&             Pulse;
  SEAcuteRespiratoryDistressSyndromeExacerbation Action;
  QScalarWidget*      LeftLungSeverity;
  QScalarWidget*      RightLungSeverity;
};

QAcuteRespiratoryDistressSyndromeExacerbationWidget::QAcuteRespiratoryDistressSyndromeExacerbationWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->LeftLungSeverity = new QScalarWidget("Left Lung Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  layout()->addWidget(m_Controls->LeftLungSeverity);
  m_Controls->RightLungSeverity = new QScalarWidget("Right Lung Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  layout()->addWidget(m_Controls->RightLungSeverity);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QAcuteRespiratoryDistressSyndromeExacerbationWidget::~QAcuteRespiratoryDistressSyndromeExacerbationWidget()
{
  delete m_Controls;
}

void QAcuteRespiratoryDistressSyndromeExacerbationWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->LeftLungSeverity->Reset();
  m_Controls->RightLungSeverity->Reset();
}

SEAction& QAcuteRespiratoryDistressSyndromeExacerbationWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QAcuteRespiratoryDistressSyndromeExacerbationWidget::GetAction() const
{
  return m_Controls->Action;
}

void QAcuteRespiratoryDistressSyndromeExacerbationWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->LeftLungSeverity->EnableInput(b);
  m_Controls->RightLungSeverity->EnableInput(b);
}

void QAcuteRespiratoryDistressSyndromeExacerbationWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  m_Controls->LeftLungSeverity->GetValue(m_Controls->Action.GetSeverity(eLungCompartment::LeftLung));
  m_Controls->RightLungSeverity->GetValue(m_Controls->Action.GetSeverity(eLungCompartment::RightLung));
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QAcuteRespiratoryDistressSyndromeExacerbationWidget::ActionToControls(const SEAcuteRespiratoryDistressSyndromeExacerbation& action)
{
  QActionWidget::ActionToControls(action);
  SEScalar data;
  data.SetValue(action.GetSeverity(eLungCompartment::LeftLung));
  m_Controls->LeftLungSeverity->SetValue(data);
  data.SetValue(action.GetSeverity(eLungCompartment::RightLung));
  m_Controls->RightLungSeverity->SetValue(data);
}
