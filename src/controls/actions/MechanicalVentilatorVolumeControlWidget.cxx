/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/
#include "controls/actions/MechanicalVentilatorVolumeControlWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"
#include "controls/LabeledComboBox.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"
#include "pulse/cdm/properties/SEScalarFrequency.h"
#include "pulse/cdm/properties/SEScalarPressure.h"
#include "pulse/cdm/properties/SEScalarTime.h"
#include "pulse/cdm/properties/SEScalarVolume.h"
#include "pulse/cdm/properties/SEScalarVolumePerPressure.h"
#include "pulse/cdm/properties/SEScalarVolumePerTime.h"


class QMechanicalVentilatorVolumeControlWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {
  }
  QPulse&               Pulse;
  SEMechanicalVentilatorVolumeControl Action;
  SEMechanicalVentilatorVolumeControl DefaultAction;
  QLabeledComboBox* State;
  QScalarWidget* FractionInspiredOxygen;
  QScalarQuantityWidget<VolumeUnit>* TidalVolume;
  QScalarQuantityWidget<TimeUnit>* InspiratoryPeriod;
  QScalarQuantityWidget<FrequencyUnit>* RespirationRate;
  QScalarQuantityWidget<PressureUnit>* PositiveEndExpiredPressure;
  QScalarQuantityWidget<VolumePerTimeUnit>* Flow;
};

QMechanicalVentilatorVolumeControlWidget::QMechanicalVentilatorVolumeControlWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{

  // Still need to put in correct defaults
  m_Controls = new Controls(qp);

  std::vector<QString> stateOptions = { "On", "Off" };
  m_Controls->State = new QLabeledComboBox(this, "State", stateOptions);
  Properties()->layout()->addWidget(m_Controls->State);

  m_Controls->FractionInspiredOxygen = new QScalarWidget("FiO2", .21, 1.0, 0.01, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->FractionInspiredOxygen);

  m_Controls->TidalVolume = new QScalarQuantityWidget<VolumeUnit>("VT", 100, 2000, 5, VolumeUnit::mL, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->TidalVolume);

  m_Controls->InspiratoryPeriod = new QScalarQuantityWidget<TimeUnit>("Ti", 0.1, 60, .1, TimeUnit::s, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->InspiratoryPeriod);

  m_Controls->RespirationRate = new QScalarQuantityWidget<FrequencyUnit>("RR", 10, 60, 1, FrequencyUnit::Per_min, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->RespirationRate);

  m_Controls->PositiveEndExpiredPressure = new QScalarQuantityWidget<PressureUnit>("PEEP", 0, 50, 1, PressureUnit::cmH2O, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->PositiveEndExpiredPressure);

  m_Controls->Flow = new QScalarQuantityWidget<VolumePerTimeUnit>("Flow", 1, 100, 1, VolumePerTimeUnit::L_Per_min, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->Flow);

  m_Controls->DefaultAction.SetConnection(eSwitch::On);
  m_Controls->DefaultAction.GetFractionInspiredOxygen().SetValue(0.21);
  m_Controls->DefaultAction.GetTidalVolume().SetValue(600, VolumeUnit::mL);
  m_Controls->DefaultAction.GetInspiratoryPeriod().SetValue(1.0, TimeUnit::s);
  m_Controls->DefaultAction.GetRespirationRate().SetValue(12, FrequencyUnit::Per_min);
  m_Controls->DefaultAction.GetPositiveEndExpiratoryPressure().SetValue(5.0, PressureUnit::cmH2O);
  m_Controls->DefaultAction.GetFlow().SetValue(50, VolumePerTimeUnit::L_Per_min);

  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QMechanicalVentilatorVolumeControlWidget::~QMechanicalVentilatorVolumeControlWidget()
{
  delete m_Controls;
}

void QMechanicalVentilatorVolumeControlWidget::Reset()
{

  QActionWidget::Reset();
  m_Controls->Action.Clear();
  ActionToControls(m_Controls->DefaultAction);
}

SEAction& QMechanicalVentilatorVolumeControlWidget::GetAction()
{
  return m_Controls->Action;
}

const SEAction& QMechanicalVentilatorVolumeControlWidget::GetAction() const
{
  return m_Controls->Action;
}

void QMechanicalVentilatorVolumeControlWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->State->setEnabled(b);
  m_Controls->FractionInspiredOxygen->EnableInput(b);
  m_Controls->TidalVolume->EnableInput(b);
  m_Controls->InspiratoryPeriod->EnableInput(b);
  m_Controls->RespirationRate->EnableInput(b);
  m_Controls->PositiveEndExpiredPressure->EnableInput(b);
  m_Controls->Flow->EnableInput(b);
}

void QMechanicalVentilatorVolumeControlWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();

  m_Controls->Action.SetConnection((m_Controls->State->GetIndex() == 0) ? eSwitch::On : eSwitch::Off);
  m_Controls->FractionInspiredOxygen->GetValue(m_Controls->Action.GetFractionInspiredOxygen());
  m_Controls->TidalVolume->GetValue(m_Controls->Action.GetTidalVolume());
  m_Controls->InspiratoryPeriod->GetValue(m_Controls->Action.GetInspiratoryPeriod());
  m_Controls->RespirationRate->GetValue(m_Controls->Action.GetRespirationRate());
  m_Controls->PositiveEndExpiredPressure->GetValue(m_Controls->Action.GetPositiveEndExpiratoryPressure());
  m_Controls->Flow->GetValue(m_Controls->Action.GetFlow());

  emit UpdateAction(m_Controls->Action, GetProcessTime());
}
void QMechanicalVentilatorVolumeControlWidget::ActionToControls(const SEMechanicalVentilatorVolumeControl& action)
{
  QActionWidget::ActionToControls(action);
  m_Controls->Action.Copy(action, m_Controls->Pulse.GetScenario().GetSubstanceManager());
  m_Controls->State->SetCurrentIndex((action.GetConnection() == eSwitch::On) ? 0 : 1);
  m_Controls->FractionInspiredOxygen->SetValue(m_Controls->Action.GetFractionInspiredOxygen());
  m_Controls->TidalVolume->SetValue(m_Controls->Action.GetTidalVolume());
  m_Controls->InspiratoryPeriod->SetValue(m_Controls->Action.GetInspiratoryPeriod());
  m_Controls->RespirationRate->SetValue(m_Controls->Action.GetRespirationRate());
  m_Controls->PositiveEndExpiredPressure->SetValue(m_Controls->Action.GetPositiveEndExpiratoryPressure());
  m_Controls->Flow->SetValue(m_Controls->Action.GetFlow());
}
