/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/MechanicalVentilatorHoldWidget.h"
#include "controls/LabeledComboBox.h"
#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QMechanicalVentilatorHoldWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&               Pulse;
  QLabeledComboBox*     State;
  SEMechanicalVentilatorHold             Action;
};

QMechanicalVentilatorHoldWidget::QMechanicalVentilatorHoldWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  
  std::vector<QString> stateOptions = { "On", "Off" };
  m_Controls->State = new QLabeledComboBox(this, "State", stateOptions);
  Properties()->layout()->addWidget(m_Controls->State);

  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QMechanicalVentilatorHoldWidget::~QMechanicalVentilatorHoldWidget()
{
  delete m_Controls;
}

void QMechanicalVentilatorHoldWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->State->SetCurrentIndex(0);
  m_Controls->Action.Clear();
}

SEAction& QMechanicalVentilatorHoldWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QMechanicalVentilatorHoldWidget::GetAction() const
{
  return m_Controls->Action;
}

void QMechanicalVentilatorHoldWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->State->setEnabled(b);
}

void QMechanicalVentilatorHoldWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  m_Controls->Action.SetState((m_Controls->State->GetIndex() == 0) ? eSwitch::On : eSwitch::Off);
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QMechanicalVentilatorHoldWidget::ActionToControls(const SEMechanicalVentilatorHold& action)
{
  QActionWidget::ActionToControls(action);
  m_Controls->State->SetCurrentIndex((action.GetState() == eSwitch::On) ? 0 : 1);
}
