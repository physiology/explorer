/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/ChronicObstructivePulmonaryDiseaseExacerbationWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QChronicObstructivePulmonaryDiseaseExacerbationWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                                          Pulse;
  SEChronicObstructivePulmonaryDiseaseExacerbation Action;
  QScalarWidget*                                   BronchitisSeverity;
  QScalarWidget*                                   EmphysemaLeftLungSeverity;
  QScalarWidget*                                   EmphysemaRightLungSeverity;
};

QChronicObstructivePulmonaryDiseaseExacerbationWidget::QChronicObstructivePulmonaryDiseaseExacerbationWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->BronchitisSeverity = new QScalarWidget("Bronchitis Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->BronchitisSeverity);
  m_Controls->EmphysemaLeftLungSeverity = new QScalarWidget("Emphysema Left Lung Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->EmphysemaLeftLungSeverity);
  m_Controls->EmphysemaRightLungSeverity = new QScalarWidget("Emphysema Right Lung Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->EmphysemaRightLungSeverity);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QChronicObstructivePulmonaryDiseaseExacerbationWidget::~QChronicObstructivePulmonaryDiseaseExacerbationWidget()
{
  delete m_Controls;
}

void QChronicObstructivePulmonaryDiseaseExacerbationWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->BronchitisSeverity->Reset();
  m_Controls->EmphysemaLeftLungSeverity->Reset();
  m_Controls->EmphysemaRightLungSeverity->Reset();
}

SEAction& QChronicObstructivePulmonaryDiseaseExacerbationWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QChronicObstructivePulmonaryDiseaseExacerbationWidget::GetAction() const
{
  return m_Controls->Action;
}

void QChronicObstructivePulmonaryDiseaseExacerbationWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->BronchitisSeverity->EnableInput(b);
  m_Controls->EmphysemaLeftLungSeverity->EnableInput(b);
  m_Controls->EmphysemaRightLungSeverity->EnableInput(b);
}

void QChronicObstructivePulmonaryDiseaseExacerbationWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  m_Controls->BronchitisSeverity->GetValue(m_Controls->Action.GetBronchitisSeverity());
  m_Controls->EmphysemaLeftLungSeverity->GetValue(m_Controls->Action.GetEmphysemaSeverity(eLungCompartment::LeftLung));
  m_Controls->EmphysemaRightLungSeverity->GetValue(m_Controls->Action.GetEmphysemaSeverity(eLungCompartment::RightLung));
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QChronicObstructivePulmonaryDiseaseExacerbationWidget::ActionToControls(const SEChronicObstructivePulmonaryDiseaseExacerbation& action)
{
  QActionWidget::ActionToControls(action);
  SEScalar data;
  data.SetValue(action.GetBronchitisSeverity());
  m_Controls->BronchitisSeverity->SetValue(data);
  data.SetValue(action.GetEmphysemaSeverity(eLungCompartment::LeftLung));
  m_Controls->EmphysemaLeftLungSeverity->SetValue(data);
  data.SetValue(action.GetEmphysemaSeverity(eLungCompartment::LeftLung));
  m_Controls->EmphysemaRightLungSeverity->SetValue(data);
}
