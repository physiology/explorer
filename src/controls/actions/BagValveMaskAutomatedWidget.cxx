/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/BagValveMaskAutomatedWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"
#include "pulse/cdm/properties/SEScalarFrequency.h"
#include "pulse/cdm/properties/SEScalarPressure.h"
#include "pulse/cdm/properties/SEScalarVolume.h"

class QBagValveMaskAutomatedWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse& Pulse;
  SEBagValveMaskAutomated               Action;
  QScalarQuantityWidget<PressureUnit>*  SqueezePressure;
  QScalarQuantityWidget<VolumeUnit>*    SqueezeVolume;
  QScalarQuantityWidget<FrequencyUnit>* BreathFrequency;
  QScalarWidget*                        InspiratoryExpiratoryRatio;
};

QBagValveMaskAutomatedWidget::QBagValveMaskAutomatedWidget(QPulse& qp, QWidget* parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);

  m_Controls->SqueezePressure = new QScalarQuantityWidget<PressureUnit>("SqueezePressure", 0, 50, 1, PressureUnit::cmH2O, ScalarOptionWidget::Radio, this);
  Properties()->layout()->addWidget(m_Controls->SqueezePressure);

  m_Controls->SqueezeVolume = new QScalarQuantityWidget<VolumeUnit>("SqueezeVolume", 0, 200, 1, VolumeUnit::mL, ScalarOptionWidget::Radio, this);
  Properties()->layout()->addWidget(m_Controls->SqueezeVolume);

  connect(m_Controls->SqueezePressure->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableSqueezePressure()));
  connect(m_Controls->SqueezeVolume->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableSqueezeVolume()));

  m_Controls->BreathFrequency = new QScalarQuantityWidget<FrequencyUnit>("BreathFrequency", 0, 60, 5, FrequencyUnit::Per_min, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->BreathFrequency);

  m_Controls->InspiratoryExpiratoryRatio = new QScalarWidget("InspiratoryExpiratoryRatio", 0, 1, .1, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->InspiratoryExpiratoryRatio);

  Properties()->layout()->addWidget(GetProcessTimeCtrl());

  Reset();
}

QBagValveMaskAutomatedWidget::~QBagValveMaskAutomatedWidget()
{
  delete m_Controls;
}

void QBagValveMaskAutomatedWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->SqueezePressure->Reset();
  m_Controls->SqueezeVolume->Reset();
  m_Controls->BreathFrequency->Reset();
  m_Controls->InspiratoryExpiratoryRatio->Reset();
}

SEAction& QBagValveMaskAutomatedWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QBagValveMaskAutomatedWidget::GetAction() const
{
  return m_Controls->Action;
}

void QBagValveMaskAutomatedWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  if (!b)
  {
    m_Controls->SqueezePressure->FullDisable();
    m_Controls->SqueezeVolume->FullDisable();
  }
  else
  {
    m_Controls->SqueezePressure->FullEnable();
    m_Controls->SqueezeVolume->FullEnable();
    m_Controls->SqueezePressure->EnableInput(!b);
    m_Controls->SqueezeVolume->EnableInput(b);
  }
  m_Controls->BreathFrequency->EnableInput(b);
  m_Controls->BreathFrequency->setEnabled(b);

  m_Controls->InspiratoryExpiratoryRatio->EnableInput(b);
  m_Controls->InspiratoryExpiratoryRatio->setEnabled(b);
}

void QBagValveMaskAutomatedWidget::EnableSqueezePressure()
{
  bool b = m_Controls->SqueezePressure->GetRadioButton()->isChecked();
  m_Controls->SqueezePressure->EnableInput(b);
  m_Controls->SqueezeVolume->EnableInput(!b);
}
void QBagValveMaskAutomatedWidget::EnableSqueezeVolume()
{
  bool b = m_Controls->SqueezeVolume->GetRadioButton()->isChecked();
  m_Controls->SqueezePressure->EnableInput(!b);
  m_Controls->SqueezeVolume->EnableInput(b);
}

void QBagValveMaskAutomatedWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  if (m_Controls->SqueezePressure->IsChecked())
  {
    m_Controls->Action.GetSqueezeVolume().Invalidate();
    m_Controls->SqueezePressure->GetValue(m_Controls->Action.GetSqueezePressure());
  }
  else
  {
    m_Controls->Action.GetSqueezePressure().Invalidate();
    m_Controls->SqueezeVolume->GetValue(m_Controls->Action.GetSqueezeVolume());
  }
  m_Controls->BreathFrequency->GetValue(m_Controls->Action.GetBreathFrequency());
  m_Controls->InspiratoryExpiratoryRatio->GetValue(m_Controls->Action.GetInspiratoryExpiratoryRatio());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QBagValveMaskAutomatedWidget::ActionToControls(const SEBagValveMaskAutomated& action)
{
  QActionWidget::ActionToControls(action);

  if (action.HasSqueezePressure())
  {
    m_Controls->SqueezePressure->EnableInput(true);
    m_Controls->SqueezeVolume->EnableInput(false);
    SEScalarPressure SqueezePressure;
    SqueezePressure.SetValue(action.GetSqueezePressure(PressureUnit::cmH2O), PressureUnit::cmH2O);
    m_Controls->SqueezePressure->SetValue(SqueezePressure);
  }
  else if (action.HasSqueezeVolume())
  {
    m_Controls->SqueezePressure->EnableInput(false);
    m_Controls->SqueezeVolume->EnableInput(true);
    SEScalarVolume SqueezeVolume;
    SqueezeVolume.SetValue(action.GetSqueezeVolume(VolumeUnit::mL), VolumeUnit::mL);
    m_Controls->SqueezeVolume->SetValue(SqueezeVolume);
  }
  SEScalarFrequency freq;
  freq.SetValue(action.GetBreathFrequency(FrequencyUnit::Per_min), FrequencyUnit::Per_min);
  m_Controls->BreathFrequency->SetValue(freq);


  SEScalar0To1 ier;
  ier.SetValue(action.GetInspiratoryExpiratoryRatio());
  m_Controls->InspiratoryExpiratoryRatio->SetValue(ier);
}
