/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/DyspneaWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QDyspneaWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&        Pulse;
  SEDyspnea      Action;
  QScalarWidget* RespirationRateSeverity;
  QScalarWidget* TidalVolumeSeverity;
};

QDyspneaWidget::QDyspneaWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->RespirationRateSeverity = new QScalarWidget("RespirationRateSeverity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->RespirationRateSeverity);
  m_Controls->TidalVolumeSeverity = new QScalarWidget("TidalVolumeSeverity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->TidalVolumeSeverity);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QDyspneaWidget::~QDyspneaWidget()
{
  delete m_Controls;
}

void QDyspneaWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->RespirationRateSeverity->Reset();
  m_Controls->TidalVolumeSeverity->Reset();
}

SEAction& QDyspneaWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QDyspneaWidget::GetAction() const
{
  return m_Controls->Action;
}

void QDyspneaWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->RespirationRateSeverity->EnableInput(b);
  m_Controls->TidalVolumeSeverity->EnableInput(b);
}

void QDyspneaWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  m_Controls->RespirationRateSeverity->GetValue(m_Controls->Action.GetRespirationRateSeverity());
  m_Controls->TidalVolumeSeverity->GetValue(m_Controls->Action.GetTidalVolumeSeverity());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QDyspneaWidget::ActionToControls(const SEDyspnea& action)
{
  QActionWidget::ActionToControls(action);
  SEScalar data;
  data.SetValue(action.GetRespirationRateSeverity());
  m_Controls->RespirationRateSeverity->SetValue(data);
  data.SetValue(action.GetTidalVolumeSeverity());
  m_Controls->TidalVolumeSeverity->SetValue(data);
}
