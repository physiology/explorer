/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/UrinateWidget.h"
#include "controls/LabeledComboBox.h"
#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QUrinateWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&               Pulse;
  SEUrinate             Action;
};

QUrinateWidget::QUrinateWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QUrinateWidget::~QUrinateWidget()
{
  delete m_Controls;
}

void QUrinateWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
}

SEAction& QUrinateWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QUrinateWidget::GetAction() const
{
  return m_Controls->Action;
}

void QUrinateWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
}

void QUrinateWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QUrinateWidget::ActionToControls(const SEUrinate& action)
{
  QActionWidget::ActionToControls(action);
}
