/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/PericardialEffusionWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalarVolumePerTime.h"

class QPericardialEffusionWidget::Controls // based off of chronic anemia widget
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                                   Pulse;
  SEPericardialEffusion                     Action;
  QScalarQuantityWidget<VolumePerTimeUnit>* EffusionRate; // check this
};

QPericardialEffusionWidget::QPericardialEffusionWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->EffusionRate = new QScalarQuantityWidget<VolumePerTimeUnit>("Effusion Rate", 0, 1000, 0.1, VolumePerTimeUnit::mL_Per_min, ScalarOptionWidget::None, this);
  m_Controls->EffusionRate->AddUnit(VolumePerTimeUnit::mL_Per_min);
  m_Controls->EffusionRate->AddUnit(VolumePerTimeUnit::L_Per_s);
  m_Controls->EffusionRate->AddUnit(VolumePerTimeUnit::L_Per_min);
  Properties()->layout()->addWidget(m_Controls->EffusionRate);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QPericardialEffusionWidget::~QPericardialEffusionWidget()
{
  delete m_Controls;
}

void QPericardialEffusionWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->EffusionRate->Reset();
}

SEAction& QPericardialEffusionWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QPericardialEffusionWidget::GetAction() const
{
  return m_Controls->Action;
}

void QPericardialEffusionWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->EffusionRate->EnableInput(b);
  m_Controls->EffusionRate->setEnabled(b);
}

void QPericardialEffusionWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  m_Controls->EffusionRate->GetValue(m_Controls->Action.GetEffusionRate());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QPericardialEffusionWidget::ActionToControls(const SEPericardialEffusion& action)
{
  QActionWidget::ActionToControls(action);
  SEScalarVolumePerTime rateData;
  rateData.SetValue(action.GetEffusionRate(VolumePerTimeUnit::mL_Per_min), VolumePerTimeUnit::mL_Per_min);
  m_Controls->EffusionRate->SetValue(rateData);
}
