/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/
#include "controls/actions/MechanicalVentilatorContinuousPositiveAirwayPressureWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"
#include "controls/LabeledComboBox.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"
#include "pulse/cdm/properties/SEScalarFrequency.h"
#include "pulse/cdm/properties/SEScalarPressure.h"
#include "pulse/cdm/properties/SEScalarTime.h"
#include "pulse/cdm/properties/SEScalarVolume.h"
#include "pulse/cdm/properties/SEScalarVolumePerPressure.h"
#include "pulse/cdm/properties/SEScalarVolumePerTime.h"


class QMechanicalVentilatorContinuousPositiveAirwayPressureWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {
  }
  QPulse&               Pulse;
  ActionType Action;
  ActionType DefaultAction;
  QLabeledComboBox* State;
  QScalarWidget* FractionInspiredOxygen;
  QScalarQuantityWidget<PressureUnit>* PositiveEndExpiredPressure;
  QScalarQuantityWidget<PressureUnit>* DeltaPressureSupport;
  QScalarQuantityWidget<TimeUnit>* Slope;
};

QMechanicalVentilatorContinuousPositiveAirwayPressureWidget::QMechanicalVentilatorContinuousPositiveAirwayPressureWidget(
    QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{

  // Still need to put in correct defaults
  m_Controls = new Controls(qp);

  std::vector<QString> stateOptions = { "On", "Off" };
  m_Controls->State = new QLabeledComboBox(this, "State", stateOptions);
  Properties()->layout()->addWidget(m_Controls->State);

  m_Controls->FractionInspiredOxygen = new QScalarWidget("FiO2", .21, 1.0, 0.01, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->FractionInspiredOxygen);

  m_Controls->PositiveEndExpiredPressure = new QScalarQuantityWidget<PressureUnit>("PEEP", 0, 50, 1, PressureUnit::cmH2O, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->PositiveEndExpiredPressure);

  m_Controls->DeltaPressureSupport = new QScalarQuantityWidget<PressureUnit>("VT", 1, 100, 1, PressureUnit::cmH2O, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->DeltaPressureSupport);

  m_Controls->Slope = new QScalarQuantityWidget<TimeUnit>("Flow", 1, 100, 1, TimeUnit::s, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->Slope);

  
  m_Controls->DefaultAction.SetConnection(eSwitch::On);
  m_Controls->DefaultAction.GetFractionInspiredOxygen().SetValue(0.21);
  m_Controls->DefaultAction.GetPositiveEndExpiratoryPressure().SetValue(5.0, PressureUnit::cmH2O);
  m_Controls->DefaultAction.GetDeltaPressureSupport().SetValue(8, PressureUnit::cmH2O);
  m_Controls->DefaultAction.GetSlope().SetValue(0.2, TimeUnit::s);

  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QMechanicalVentilatorContinuousPositiveAirwayPressureWidget::~QMechanicalVentilatorContinuousPositiveAirwayPressureWidget()
{
  delete m_Controls;
}

void QMechanicalVentilatorContinuousPositiveAirwayPressureWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();

  ActionToControls(m_Controls->DefaultAction);
}

SEAction& QMechanicalVentilatorContinuousPositiveAirwayPressureWidget::GetAction()
{
  return m_Controls->Action;
}

const SEAction& QMechanicalVentilatorContinuousPositiveAirwayPressureWidget::GetAction() const
{
  return m_Controls->Action;
}

void QMechanicalVentilatorContinuousPositiveAirwayPressureWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->State->setEnabled(b);
  m_Controls->FractionInspiredOxygen->EnableInput(b);
  m_Controls->DeltaPressureSupport->EnableInput(b);
  m_Controls->PositiveEndExpiredPressure->EnableInput(b);
  m_Controls->Slope->EnableInput(b);
}

void QMechanicalVentilatorContinuousPositiveAirwayPressureWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();

  m_Controls->Action.SetConnection((m_Controls->State->GetIndex() == 0) ? eSwitch::On : eSwitch::Off);
  m_Controls->FractionInspiredOxygen->GetValue(m_Controls->Action.GetFractionInspiredOxygen());
  m_Controls->DeltaPressureSupport->GetValue(m_Controls->Action.GetDeltaPressureSupport());
  m_Controls->PositiveEndExpiredPressure->GetValue(m_Controls->Action.GetPositiveEndExpiratoryPressure());
  m_Controls->Slope->GetValue(m_Controls->Action.GetSlope());

  emit UpdateAction(m_Controls->Action, GetProcessTime());
}
void QMechanicalVentilatorContinuousPositiveAirwayPressureWidget::ActionToControls(
    const SEMechanicalVentilatorContinuousPositiveAirwayPressure& action)
{
  QActionWidget::ActionToControls(action);
  m_Controls->Action.Copy(action, m_Controls->Pulse.GetScenario().GetSubstanceManager());
  m_Controls->State->SetCurrentIndex((action.GetConnection() == eSwitch::On) ? 0 : 1);
  m_Controls->FractionInspiredOxygen->SetValue(m_Controls->Action.GetFractionInspiredOxygen());
  m_Controls->DeltaPressureSupport->SetValue(m_Controls->Action.GetDeltaPressureSupport());
  m_Controls->PositiveEndExpiredPressure->SetValue(m_Controls->Action.GetPositiveEndExpiratoryPressure());
  m_Controls->Slope->SetValue(m_Controls->Action.GetSlope());
}
