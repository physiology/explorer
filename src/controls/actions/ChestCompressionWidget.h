/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QWidget>
#include "controls/actions/ActionWidget.h"
#include "pulse/cdm/patient/actions/SEChestCompression.h"

class QChestCompressionWidget : public QActionWidget
{
  Q_OBJECT
public:
  using ActionType = SEChestCompression;

  QChestCompressionWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QChestCompressionWidget();

  virtual void Reset() override;
  virtual void SetEnabled(bool b) override;
  virtual const SEAction& GetAction() const override;

  virtual void ControlsToAction() override;
  virtual void ActionToControls(const SEChestCompression& action);

protected slots:
  void EnableForce();
  void EnableDepth();
protected:
  virtual SEAction& GetAction() override;
private:
  class Controls;
  Controls* m_Controls;
};
