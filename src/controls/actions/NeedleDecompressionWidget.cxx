/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/NeedleDecompressionWidget.h"
#include "controls/LabeledComboBox.h"
#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"

class QNeedleDecompressionWidget::Controls // based off of chronic anemia widget
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&               Pulse;
  SENeedleDecompression Action;
  QLabeledComboBox*     Side;
  QLabeledComboBox*     State;

};

QNeedleDecompressionWidget::QNeedleDecompressionWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  std::vector<QString> sideOptions = { "Left", "Right" };
  std::vector<QString> stateOptions = { "On", "Off" };
  m_Controls->Side = new QLabeledComboBox(this, "Side", sideOptions);
  m_Controls->State = new QLabeledComboBox(this, "State", stateOptions);
  Properties()->layout()->addWidget(m_Controls->Side);
  Properties()->layout()->addWidget(m_Controls->State);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QNeedleDecompressionWidget::~QNeedleDecompressionWidget()
{
  delete m_Controls;
}

void QNeedleDecompressionWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
}

SEAction& QNeedleDecompressionWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QNeedleDecompressionWidget::GetAction() const
{
  return m_Controls->Action;
}

void QNeedleDecompressionWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->Side->SetEnabled(b);
  m_Controls->State->SetEnabled(b);
}

void QNeedleDecompressionWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  (m_Controls->Side->GetIndex() == 0) ?
    m_Controls->Action.SetSide(eSide::Left) :
    m_Controls->Action.SetSide(eSide::Right);
  (m_Controls->State->GetIndex() == 0) ?
    m_Controls->Action.SetState(eSwitch::On) :
    m_Controls->Action.SetState(eSwitch::Off);
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QNeedleDecompressionWidget::ActionToControls(const SENeedleDecompression& action)
{
  QActionWidget::ActionToControls(action);
  QComboBox* sideBox = m_Controls->Side->GetComboBox();
  QComboBox* stateBox = m_Controls->State->GetComboBox();

  switch (action.GetSide())
  {
  case eSide::Right:
    sideBox->setCurrentIndex(sideBox->findText("Right"));
    break;
  default:
    sideBox->setCurrentIndex(sideBox->findText("Left"));
    break;
  }
  switch (action.GetState())
  {
  case eSwitch::On:
    stateBox->setCurrentIndex(stateBox->findText("On"));
    break;
  default:
    stateBox->setCurrentIndex(stateBox->findText("Off"));
    break;
  }
}
