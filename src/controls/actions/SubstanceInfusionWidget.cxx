/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/LabeledComboBox.h"
#include "controls/PulseData.h"
#include "controls/actions/SubstanceInfusionWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/engine/PulseEngine.h"
#include "pulse/engine/PulseScenario.h"
#include "pulse/cdm/properties/SEScalarVolumePerTime.h"
#include "pulse/cdm/properties/SEScalarMassPerVolume.h"
#include "pulse/cdm/substance/SESubstance.h"
#include "pulse/cdm/substance/SESubstanceManager.h"

class QSubstanceInfusionWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                                   Pulse;
  SESubstanceInfusion*                      Action = nullptr;
  std::map<std::string, SESubstance*>       Substances;
  QScalarQuantityWidget<VolumePerTimeUnit>* Rate;
  QScalarQuantityWidget<MassPerVolumeUnit>* Concentration;
  QLabeledComboBox*                         Drugs;
};

QSubstanceInfusionWidget::QSubstanceInfusionWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->Rate = new QScalarQuantityWidget<VolumePerTimeUnit>("Rate", 0, 100, 1, VolumePerTimeUnit::mL_Per_s, ScalarOptionWidget::None, this); //check this
  m_Controls->Concentration = new QScalarQuantityWidget<MassPerVolumeUnit>("Concentration", 0, 1000, 1, MassPerVolumeUnit::mg_Per_mL, ScalarOptionWidget::None, this);
  m_Controls->Concentration->AddUnit(MassPerVolumeUnit::mg_Per_L);
  m_Controls->Concentration->AddUnit(MassPerVolumeUnit::ug_Per_mL);
  m_Controls->Concentration->AddUnit(MassPerVolumeUnit::g_Per_mL);
  m_Controls->Concentration->AddUnit(MassPerVolumeUnit::g_Per_L);
  m_Controls->Concentration->AddUnit(MassPerVolumeUnit::kg_Per_L);

  const auto& drugs = PulseData::Drugs;

  m_Controls->Drugs = new QLabeledComboBox(this, "Drugs", drugs, 120);
  for (QString d : drugs)
  {
    SESubstance* drug = qp.GetScenario().GetSubstanceManager().GetSubstance(d.toStdString());
    m_Controls->Substances[drug->GetName()] = drug;
  }

  Properties()->layout()->addWidget(m_Controls->Drugs);
  Properties()->layout()->addWidget(m_Controls->Rate);
  Properties()->layout()->addWidget(m_Controls->Concentration);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QSubstanceInfusionWidget::~QSubstanceInfusionWidget()
{
  SAFE_DELETE(m_Controls->Action);
  delete m_Controls;
}

void QSubstanceInfusionWidget::Reset()
{
  QActionWidget::Reset();
  SAFE_DELETE(m_Controls->Action);
}

SEAction& QSubstanceInfusionWidget::GetAction()
{
  return *m_Controls->Action;
}
const SEAction& QSubstanceInfusionWidget::GetAction() const
{
  return *m_Controls->Action;
}

void QSubstanceInfusionWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->Rate->setEnabled(b);
  m_Controls->Rate->EnableInput(b);
  m_Controls->Concentration->setEnabled(b);
  m_Controls->Concentration->EnableInput(b);
  m_Controls->Drugs->SetEnabled(b);
}

void QSubstanceInfusionWidget::ControlsToAction()
{
  if(m_Controls->Action != nullptr)
    SAFE_DELETE(m_Controls->Action);
  m_Controls->Action = new SESubstanceInfusion(*m_Controls->Substances[m_Controls->Drugs->GetText()]);
  m_Controls->Rate->GetValue(m_Controls->Action->GetRate());
  m_Controls->Concentration->GetValue(m_Controls->Action->GetConcentration());
  QActionWidget::ControlsToAction();
  emit UpdateAction(*m_Controls->Action, GetProcessTime());
}

void QSubstanceInfusionWidget::ActionToControls(const SESubstanceInfusion& action)
{
  QActionWidget::ActionToControls(action);
  SEScalarMassPerVolume concentrationData;
  concentrationData.SetValue(action.GetConcentration(MassPerVolumeUnit::ug_Per_mL), MassPerVolumeUnit::ug_Per_mL);
  m_Controls->Concentration->SetValue(concentrationData);

  SEScalarVolumePerTime rateData;
  rateData.SetValue(action.GetRate(VolumePerTimeUnit::mL_Per_s), VolumePerTimeUnit::mL_Per_s);
  m_Controls->Rate->SetValue(rateData);

  QComboBox* drugsBox = m_Controls->Drugs->GetComboBox();
  drugsBox->setCurrentIndex(drugsBox->findText(QString::fromStdString(action.GetSubstance().GetName())));
}
