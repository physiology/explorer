/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/AsthmaAttackWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QAsthmaAttackWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&        Pulse;
  SEAsthmaAttack Action;
  QScalarWidget* Severity;
};

QAsthmaAttackWidget::QAsthmaAttackWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->Severity = new QScalarWidget("Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->Severity);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QAsthmaAttackWidget::~QAsthmaAttackWidget()
{
  delete m_Controls;
}

void QAsthmaAttackWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->Severity->Reset();
}

SEAction& QAsthmaAttackWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QAsthmaAttackWidget::GetAction() const
{
  return m_Controls->Action;
}

void QAsthmaAttackWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->Severity->EnableInput(b);
}

void QAsthmaAttackWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  m_Controls->Severity->GetValue(m_Controls->Action.GetSeverity());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QAsthmaAttackWidget::ActionToControls(const SEAsthmaAttack& action)
{
  QActionWidget::ActionToControls(action);
   SEScalar data;
   data.SetValue(action.GetSeverity());
   m_Controls->Severity->SetValue(data);
}
