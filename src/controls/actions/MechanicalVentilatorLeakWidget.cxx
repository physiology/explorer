/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/
#include "controls/actions/MechanicalVentilatorLeakWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"
#include "cdm/system/equipment/mechanical_ventilator/actions/SEMechanicalVentilatorLeak.h"

class QMechanicalVentilatorLeakWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {
  }
  QPulse& Pulse;
  SEMechanicalVentilatorLeak Action;
  QScalarWidget* Severity;
};

QMechanicalVentilatorLeakWidget::QMechanicalVentilatorLeakWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->Severity = new QScalarWidget("Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->Severity);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QMechanicalVentilatorLeakWidget::~QMechanicalVentilatorLeakWidget()
{
  delete m_Controls;
}

void QMechanicalVentilatorLeakWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->Severity->Reset();
}

SEAction& QMechanicalVentilatorLeakWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QMechanicalVentilatorLeakWidget::GetAction() const
{
  return m_Controls->Action;
}

void QMechanicalVentilatorLeakWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->Severity->EnableInput(b);
}

void QMechanicalVentilatorLeakWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  m_Controls->Severity->GetValue(m_Controls->Action.GetSeverity());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QMechanicalVentilatorLeakWidget::ActionToControls(const SEMechanicalVentilatorLeak& action)
{
  QActionWidget::ActionToControls(action);
  SEScalar data;
  data.SetValue(action.GetSeverity());
  m_Controls->Severity->SetValue(data);
}
