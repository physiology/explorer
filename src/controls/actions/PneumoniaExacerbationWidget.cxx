/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/PneumoniaExacerbationWidget.h"
#include "controls/LabeledComboBox.h"
#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QPneumoniaExacerbationWidget::Controls // based off of chronic anemia widget
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                      Pulse;
  SEPneumoniaExacerbation      Action;
  QScalarWidget*               LeftLungSeverity;
  QScalarWidget*               RightLungSeverity;
};

QPneumoniaExacerbationWidget::QPneumoniaExacerbationWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->LeftLungSeverity = new QScalarWidget("Left Lung Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  m_Controls->RightLungSeverity = new QScalarWidget("Right Lung Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->LeftLungSeverity);
  Properties()->layout()->addWidget(m_Controls->RightLungSeverity);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QPneumoniaExacerbationWidget::~QPneumoniaExacerbationWidget()
{
  delete m_Controls;
}

void QPneumoniaExacerbationWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
}

SEAction& QPneumoniaExacerbationWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QPneumoniaExacerbationWidget::GetAction() const
{
  return m_Controls->Action;
}

void QPneumoniaExacerbationWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->LeftLungSeverity->EnableInput(b);
  m_Controls->LeftLungSeverity->setEnabled(b);
  m_Controls->RightLungSeverity->EnableInput(b);
  m_Controls->RightLungSeverity->setEnabled(b);
}

void QPneumoniaExacerbationWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  m_Controls->LeftLungSeverity->GetValue(m_Controls->Action.GetSeverity(eLungCompartment::LeftLung));
  m_Controls->RightLungSeverity->GetValue(m_Controls->Action.GetSeverity(eLungCompartment::RightLung));
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QPneumoniaExacerbationWidget::ActionToControls(const SEPneumoniaExacerbation& action)
{
  QActionWidget::ActionToControls(action);
  SEScalar data;

  data.SetValue(action.GetSeverity(eLungCompartment::LeftLung));
  m_Controls->LeftLungSeverity->SetValue(data);

  data.SetValue(action.GetSeverity(eLungCompartment::RightLung));
  m_Controls->RightLungSeverity->SetValue(data);
}
