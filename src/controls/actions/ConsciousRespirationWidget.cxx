/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "ConsciousRespirationWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "cdm/CommonDataModel.h"
#include "cdm/patient/actions/SEConsciousRespiration.h"
#include "cdm/properties/SEScalar0To1.h"
#include <QString>
#include <set>
#include "controls/LabeledComboBox.h"

class QConsciousRespirationWidget::Controls // based off of chronic anemia widget
{
public:
  Controls() {}
  SEConsciousRespiration Action;
  QScalarWidget* Severity; // check this
  
  QWidget*                                  Side;
  QLabel*                                   SideLabel;
  QSpacerItem*                              SideSpacer;
  QComboBox*                                SideValue;
  QHBoxLayout*                              SideLayout;

  QWidget*                                  Type;
  QLabel*                                   TypeLabel;
  QSpacerItem*                              TypeSpacer;
  QComboBox*                                TypeValue;
  QHBoxLayout*                              TypeLayout;
};

QConsciousRespirationWidget::QConsciousRespiration(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(parent, flags)
{
  m_Controls = new Controls();
  m_Controls->Severity = new QScalarWidget("Severity", 0, 1, 0.1, false, this); //check this

  std::set<QString> sideOptions = { "Left", "Right" };
  std::set<QString> typeOptions = { "Open", "Closed" };

  m_Controls->Side = new QLabeledComboBox(this, "Side", sideOptions);
  m_Controls->Type = new QLabeledComboBox(this, "Type", typeOptions);

  layout()->addWidget(m_Controls->Severity);
  layout()->addWidget(m_Controls->Side);
  layout()->addWidget(m_Controls->Type);

  reset();
}

QConsciousRespirationWidget::~QConsciousRespirationWidget()
{
  delete m_Controls;
}

void QConsciousRespirationWidget::reset()
{
  QActionWidget::reset();
  m_Controls->Action.Clear();
  m_Controls->Severity->reset(); // check this
  m_Controls->Side->reset();
  m_Controls->Type->reset();
}

const SEAction& QConsciousRespirationWidget::getAction()
{
  m_Controls->Action.Clear(); 
  m_Controls->Severity->getValue(m_Controls->Action.GetSeverity());
  m_Controls->Side->getValue(m_Controls->Action.GetSide());
  m_Controls->Type->getValue(m_Controls->Action.GetType());
  return m_Controls->Action;
}

void QConsciousRespirationWidget::setAction(const SEAction& c)
{
  m_Controls->Action.Copy((SEConsciousRespiration&)(c)); 
  m_Controls->Severity->setValue(m_Controls->Action.GetSeverity());
  m_Controls->Side->setValue(m_Controls->Action.GetSide());
  m_Controls->Type->setValue(m_Controls->Action.GetType());
  
  QActionWidget::enableAction(c.IsActive());
}

void QConsciousRespirationWidget::setEnabled(bool b)
{
  m_Controls->Severity->enableInput(b);
}