/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QWidget>
#include "controls/actions/ActionWidget.h"
#include "pulse/cdm/engine/SESerializeState.h"

class QSerializeStateWidget : public QActionWidget
{
  Q_OBJECT
public:
  using ActionType = SESerializeState;

  QSerializeStateWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QSerializeStateWidget();

  virtual void Reset() override;
  virtual void SetEnabled(bool b) override;
  virtual const SEAction& GetAction() const override;

  virtual void ControlsToAction() override;
  virtual void ActionToControls(const SESerializeState& action);

  virtual void ProcessPhysiology(PhysiologyEngine& pulse) override;

protected slots:
  void Browse();
protected:
  virtual SEAction& GetAction() override;
private:
  class Controls;
  Controls* m_Controls;
  
};
