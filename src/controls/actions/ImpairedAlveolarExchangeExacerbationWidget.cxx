/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/ImpairedAlveolarExchangeExacerbationWidget.h"
#include "controls/LabeledComboBox.h"
#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"
#include "pulse/cdm/properties/SEScalarArea.h"

class QImpairedAlveolarExchangeExacerbationWidget::Controls // based off of chronic anemia widget
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                                Pulse;
  SEImpairedAlveolarExchangeExacerbation Action;
  QScalarWidget*                         Severity;
  QScalarWidget*                         ImpairedFraction;
  QScalarWidget*                         ImpairedSurfaceArea;
};

QImpairedAlveolarExchangeExacerbationWidget::QImpairedAlveolarExchangeExacerbationWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->Severity = new QScalarWidget("Severity", 0, 1, 0.1, ScalarOptionWidget::Radio, this);
  m_Controls->ImpairedFraction = new QScalarWidget("Impaired Fraction", 0, 1, 0.1, ScalarOptionWidget::Radio, this);
  m_Controls->ImpairedSurfaceArea = new QScalarWidget("Impaired Surface Area", 0, 1, 0.1, ScalarOptionWidget::Radio, this);
  Properties()->layout()->addWidget(m_Controls->Severity);
  Properties()->layout()->addWidget(m_Controls->ImpairedFraction);
  Properties()->layout()->addWidget(m_Controls->ImpairedSurfaceArea);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  connect(m_Controls->Severity->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableSeverity()));
  connect(m_Controls->ImpairedFraction->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableImpairedFraction()));
  connect(m_Controls->ImpairedSurfaceArea->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableImpairedSurfaceArea()));
  Reset();
}

QImpairedAlveolarExchangeExacerbationWidget::~QImpairedAlveolarExchangeExacerbationWidget()
{
  delete m_Controls;
}

void QImpairedAlveolarExchangeExacerbationWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
}

SEAction& QImpairedAlveolarExchangeExacerbationWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QImpairedAlveolarExchangeExacerbationWidget::GetAction() const
{
  return m_Controls->Action;
}

void QImpairedAlveolarExchangeExacerbationWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  if (!b)
  {
    m_Controls->Severity->FullDisable();
    m_Controls->ImpairedFraction->FullDisable();
    m_Controls->ImpairedSurfaceArea->FullDisable();
  }
  else
  {
    m_Controls->Severity->FullEnable();
    m_Controls->Severity->EnableInput(b);
    m_Controls->Severity->setEnabled(b);
    m_Controls->ImpairedFraction->FullEnable();
    m_Controls->ImpairedFraction->EnableInput(!b);
    m_Controls->ImpairedFraction->setEnabled(b);
    m_Controls->ImpairedSurfaceArea->FullEnable();
    m_Controls->ImpairedSurfaceArea->EnableInput(!b);
    m_Controls->ImpairedSurfaceArea->setEnabled(b);
  }
}

void QImpairedAlveolarExchangeExacerbationWidget::EnableSeverity()
{
  bool b = m_Controls->Severity->IsChecked();
  m_Controls->Severity->EnableInput(b);
  m_Controls->ImpairedFraction->EnableInput(!b);
  m_Controls->ImpairedSurfaceArea->EnableInput(!b);
}
void QImpairedAlveolarExchangeExacerbationWidget::EnableImpairedFraction()
{
  bool b = m_Controls->ImpairedFraction->IsChecked();
  m_Controls->Severity->EnableInput(!b);
  m_Controls->ImpairedFraction->EnableInput(b);
  m_Controls->ImpairedSurfaceArea->EnableInput(!b);
}
void QImpairedAlveolarExchangeExacerbationWidget::EnableImpairedSurfaceArea()
{
  bool b = m_Controls->ImpairedSurfaceArea->IsChecked();
  m_Controls->Severity->EnableInput(!b);
  m_Controls->ImpairedFraction->EnableInput(!b);
  m_Controls->ImpairedSurfaceArea->EnableInput(b);
}

void QImpairedAlveolarExchangeExacerbationWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  if (m_Controls->Severity->IsChecked())
    m_Controls->Severity->GetValue(m_Controls->Action.GetSeverity());
  else if (m_Controls->ImpairedFraction->IsChecked())
    m_Controls->ImpairedFraction->GetValue(m_Controls->Action.GetImpairedFraction());
  else
    m_Controls->ImpairedSurfaceArea->GetValue(m_Controls->Action.GetImpairedSurfaceArea());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QImpairedAlveolarExchangeExacerbationWidget::ActionToControls(const SEImpairedAlveolarExchangeExacerbation& action)
{
  QActionWidget::ActionToControls(action);
  SEScalar data;
  data.SetValue(action.GetSeverity());
  m_Controls->Severity->SetValue(data);

  data.SetValue(action.GetImpairedFraction());
  m_Controls->ImpairedFraction->SetValue(data);

  SEScalarArea area;
  area.SetValue(action.GetImpairedSurfaceArea(AreaUnit::cm2), AreaUnit::cm2);
  m_Controls->ImpairedSurfaceArea->SetValue(area);
}
