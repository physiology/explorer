/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/TensionPneumothoraxWidget.h"
#include "controls/LabeledComboBox.h"
#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QTensionPneumothoraxWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                Pulse;
  SETensionPneumothorax  Action;
  QScalarWidget*         Severity;
  QLabeledComboBox*      Side;
  QLabeledComboBox*      Type;

};

QTensionPneumothoraxWidget::QTensionPneumothoraxWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->Severity = new QScalarWidget("Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);

  std::vector<QString> sideOptions = {"Left", "Right"};
  std::vector<QString> typeOptions = { "Open", "Closed" };
  m_Controls->Side = new QLabeledComboBox(this, "Side", sideOptions);
  m_Controls->Type = new QLabeledComboBox(this, "Type", typeOptions);

  Properties()->layout()->addWidget(m_Controls->Severity);
  Properties()->layout()->addWidget(m_Controls->Side);
  Properties()->layout()->addWidget(m_Controls->Type);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QTensionPneumothoraxWidget::~QTensionPneumothoraxWidget()
{
  delete m_Controls;
}

void QTensionPneumothoraxWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->Severity->Reset();
}

SEAction& QTensionPneumothoraxWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QTensionPneumothoraxWidget::GetAction() const
{
  return m_Controls->Action;
}

void QTensionPneumothoraxWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->Severity->EnableInput(b);
  m_Controls->Severity->setEnabled(b);
  m_Controls->Side->SetEnabled(b);
  m_Controls->Type->SetEnabled(b);
}

void QTensionPneumothoraxWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  m_Controls->Severity->GetValue(m_Controls->Action.GetSeverity());
  (m_Controls->Side->GetText() == "Left") ?
    m_Controls->Action.SetSide(eSide::Left) :
    m_Controls->Action.SetSide(eSide::Right);
  (m_Controls->Type->GetText() == "Open") ?
    m_Controls->Action.SetType(eGate::Open) :
    m_Controls->Action.SetType(eGate::Closed);
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QTensionPneumothoraxWidget::ActionToControls(const SETensionPneumothorax& action)
{
  QActionWidget::ActionToControls(action);
  SEScalar data;
  data.SetValue(action.GetSeverity());
  m_Controls->Severity->SetValue(data);

  QComboBox* sideBox = m_Controls->Side->GetComboBox();
  switch (action.GetSide()) {
  case eSide::Left:
    sideBox->setCurrentIndex(sideBox->findText("Left"));
    break;
  default:
    sideBox->setCurrentIndex(sideBox->findText("Right"));
    break;
  }

  QComboBox* typeBox = m_Controls->Type->GetComboBox();
  switch (action.GetType())
  {
  case eGate::Open:
    typeBox->setCurrentIndex(typeBox->findText("Open"));
    break;
  default:
    typeBox->setCurrentIndex(typeBox->findText("Closed"));
    break;
  }
}
