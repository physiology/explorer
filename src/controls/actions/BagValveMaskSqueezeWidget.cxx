/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/BagValveMaskSqueezeWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalarPressure.h"
#include "pulse/cdm/properties/SEScalarTime.h"
#include "pulse/cdm/properties/SEScalarVolume.h"

class QBagValveMaskSqueezeWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse& Pulse;
  SEBagValveMaskSqueeze                Action;
  QScalarQuantityWidget<PressureUnit>* SqueezePressure;
  QScalarQuantityWidget<VolumeUnit>*   SqueezeVolume;
  QScalarQuantityWidget<TimeUnit>*     ExpiratoryPeriod;
  QScalarQuantityWidget<TimeUnit>*     InspiratoryPeriod;
};

QBagValveMaskSqueezeWidget::QBagValveMaskSqueezeWidget(QPulse& qp, QWidget* parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);

  m_Controls->SqueezePressure = new QScalarQuantityWidget<PressureUnit>("SqueezePressure", 0, 50, 1, PressureUnit::cmH2O, ScalarOptionWidget::Radio, this);
  Properties()->layout()->addWidget(m_Controls->SqueezePressure);

  m_Controls->SqueezeVolume = new QScalarQuantityWidget<VolumeUnit>("SqueezeVolume", 0, 200, 1, VolumeUnit::mL, ScalarOptionWidget::Radio, this);
  Properties()->layout()->addWidget(m_Controls->SqueezeVolume);

  connect(m_Controls->SqueezePressure->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableSqueezePressure()));
  connect(m_Controls->SqueezeVolume->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableSqueezeVolume()));

  m_Controls->ExpiratoryPeriod = new QScalarQuantityWidget<TimeUnit>("ExpiratoryPeriod", 0, 5, .1, TimeUnit::s, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->ExpiratoryPeriod);

  m_Controls->InspiratoryPeriod = new QScalarQuantityWidget<TimeUnit>("InspiratoryPeriod", 0, 5, .1, TimeUnit::s, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->InspiratoryPeriod);

  Properties()->layout()->addWidget(GetProcessTimeCtrl());

  Reset();
}

QBagValveMaskSqueezeWidget::~QBagValveMaskSqueezeWidget()
{
  delete m_Controls;
}

void QBagValveMaskSqueezeWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->SqueezePressure->Reset();
  m_Controls->SqueezeVolume->Reset();
  m_Controls->ExpiratoryPeriod->Reset();
  m_Controls->InspiratoryPeriod->Reset();
}

SEAction& QBagValveMaskSqueezeWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QBagValveMaskSqueezeWidget::GetAction() const
{
  return m_Controls->Action;
}

void QBagValveMaskSqueezeWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  if (!b)
  {
    m_Controls->SqueezePressure->FullDisable();
    m_Controls->SqueezeVolume->FullDisable();
  }
  else
  {
    m_Controls->SqueezePressure->FullEnable();
    m_Controls->SqueezeVolume->FullEnable();
    m_Controls->SqueezePressure->EnableInput(!b);
    m_Controls->SqueezeVolume->EnableInput(b);
  }
  m_Controls->ExpiratoryPeriod->EnableInput(b);
  m_Controls->ExpiratoryPeriod->setEnabled(b);

  m_Controls->InspiratoryPeriod->EnableInput(b);
  m_Controls->InspiratoryPeriod->setEnabled(b);
}

void QBagValveMaskSqueezeWidget::EnableSqueezePressure()
{
  bool b = m_Controls->SqueezePressure->GetRadioButton()->isChecked();
  m_Controls->SqueezePressure->EnableInput(b);
  m_Controls->SqueezeVolume->EnableInput(!b);
}
void QBagValveMaskSqueezeWidget::EnableSqueezeVolume()
{
  bool b = m_Controls->SqueezeVolume->GetRadioButton()->isChecked();
  m_Controls->SqueezePressure->EnableInput(!b);
  m_Controls->SqueezeVolume->EnableInput(b);
}

void QBagValveMaskSqueezeWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  if (m_Controls->SqueezePressure->IsChecked())
  {
    m_Controls->Action.GetSqueezeVolume().Invalidate();
    m_Controls->SqueezePressure->GetValue(m_Controls->Action.GetSqueezePressure());
  }
  else
  {
    m_Controls->Action.GetSqueezePressure().Invalidate();
    m_Controls->SqueezeVolume->GetValue(m_Controls->Action.GetSqueezeVolume());
  }
  m_Controls->ExpiratoryPeriod->GetValue(m_Controls->Action.GetExpiratoryPeriod());
  m_Controls->InspiratoryPeriod->GetValue(m_Controls->Action.GetInspiratoryPeriod());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QBagValveMaskSqueezeWidget::ActionToControls(const SEBagValveMaskSqueeze& action)
{
  QActionWidget::ActionToControls(action);

  if (action.HasSqueezePressure())
  {
    m_Controls->SqueezePressure->EnableInput(true);
    m_Controls->SqueezeVolume->EnableInput(false);
    SEScalarPressure SqueezePressure;
    SqueezePressure.SetValue(action.GetSqueezePressure(PressureUnit::cmH2O), PressureUnit::cmH2O);
    m_Controls->SqueezePressure->SetValue(SqueezePressure);
  }
  else if (action.HasSqueezeVolume())
  {
    m_Controls->SqueezePressure->EnableInput(false);
    m_Controls->SqueezeVolume->EnableInput(true);
    SEScalarVolume SqueezeVolume;
    SqueezeVolume.SetValue(action.GetSqueezeVolume(VolumeUnit::mL), VolumeUnit::mL);
    m_Controls->SqueezeVolume->SetValue(SqueezeVolume);
  }
  SEScalarTime eTime;
  eTime.SetValue(action.GetExpiratoryPeriod(TimeUnit::s), TimeUnit::s);
  m_Controls->ExpiratoryPeriod->SetValue(eTime);


  SEScalarTime iTime;
  iTime.SetValue(action.GetInspiratoryPeriod(TimeUnit::s), TimeUnit::s);
  m_Controls->InspiratoryPeriod->SetValue(iTime);
}
