/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QWidget>
#include "controls/QPulse.h"
#include "controls/TimeWidget.h"
#include "pulse/cdm/properties/SEScalarTime.h"

class SEAction;

namespace Ui {
  class ActionWidget;
}

class QActionWidget : public QWidget, public PulseListener
{
  Q_OBJECT
public:
  QActionWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QActionWidget();

  virtual void Reset();
  virtual void SetEnabled(bool b);
  virtual void ShowProcessTimeCtrl(bool b);
  virtual void SetProcessTime(const SEScalarTime& t);
  virtual QWidget* Properties();
  virtual const SEAction& GetAction() const = 0;

  virtual void AtSteadyState(PhysiologyEngine& pulse) override;
  virtual void AtSteadyStateUpdateUI() override;// Main Window will call this to update UI Components
  virtual void ProcessPhysiology(PhysiologyEngine& pulse) override;
  virtual void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override;// Main Window will call this to update UI Components
  void EngineErrorUI() override {};// Main Window will call this to update UI Components

  virtual void ControlsToAction();
  virtual void ActionToControls(const SEAction& action);
  
signals:
  void UpdateAction(SEAction const& action, SEScalarTime const& pTime);

protected slots:
  void ApplyAction();
  bool ProcessAction();

  virtual SEAction& GetAction() = 0;
  SEScalarTime& GetProcessTime();
  QTimeWidget* GetProcessTimeCtrl();

private:
  class Controls;
  Controls* m_Controls;
};
