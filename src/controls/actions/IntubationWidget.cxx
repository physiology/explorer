/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/IntubationWidget.h"
#include "controls/LabeledComboBox.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"
#include "pulse/cdm/properties/SEScalarPressureTimePerVolume.h"

class QIntubationWidget::Controls // based off of chronic anemia widget
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&             Pulse;
  SEIntubation        Action;
  QLabeledComboBox*   Type;
  QScalarQuantityWidget<PressureTimePerVolumeUnit>* AirwayResistance;
  QScalarWidget* Severity;
};

QIntubationWidget::QIntubationWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  std::vector<QString> typeOptions = { "Off", "Esophageal", "LeftMainstem", "RightMainstem", "Tracheal"};
  m_Controls->Type = new QLabeledComboBox(this, "Type", typeOptions, 85);

  m_Controls->Severity = new QScalarWidget("Severity", 0, 1, 0.1, ScalarOptionWidget::Radio, this);
  Properties()->layout()->addWidget(m_Controls->Severity);

  m_Controls->AirwayResistance = new QScalarQuantityWidget<PressureTimePerVolumeUnit>("AirwayResistance", 0, 500, 10, PressureTimePerVolumeUnit::cmH2O_s_Per_mL, ScalarOptionWidget::Radio, this);
  m_Controls->AirwayResistance->AddUnit(PressureTimePerVolumeUnit::cmH2O_s_Per_L);
  Properties()->layout()->addWidget(m_Controls->AirwayResistance);

  connect(m_Controls->Severity->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableSeverity()));
  connect(m_Controls->AirwayResistance->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableAirwayResistance()));
  
  Properties()->layout()->addWidget(m_Controls->Type);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QIntubationWidget::~QIntubationWidget()
{
  delete m_Controls;
}

void QIntubationWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->Severity->Reset();
  m_Controls->AirwayResistance->Reset();
}

SEAction& QIntubationWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QIntubationWidget::GetAction() const
{
  return m_Controls->Action;
}

void QIntubationWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  if (!b)
  {
    m_Controls->Severity->FullDisable();
    m_Controls->AirwayResistance->FullDisable();
  }
  else
  {
    m_Controls->Severity->FullEnable();
    m_Controls->AirwayResistance->FullEnable();
    m_Controls->Severity->EnableInput(b);
    m_Controls->AirwayResistance->EnableInput(!b);
  }
  m_Controls->Type->SetEnabled(b);
}

void QIntubationWidget::EnableSeverity()
{
  bool b = m_Controls->Severity->GetRadioButton()->isChecked();
  m_Controls->Severity->EnableInput(b);
  m_Controls->AirwayResistance->EnableInput(!b);
}
void QIntubationWidget::EnableAirwayResistance()
{
  bool b = m_Controls->AirwayResistance->GetRadioButton()->isChecked();
  m_Controls->Severity->EnableInput(!b);
  m_Controls->AirwayResistance->EnableInput(b);
}

void QIntubationWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  if (m_Controls->Severity->IsChecked())
  {
    m_Controls->Action.GetAirwayResistance().Invalidate();
    m_Controls->Severity->GetValue(m_Controls->Action.GetSeverity());
  }
  else
  {
    m_Controls->Action.GetSeverity().Invalidate();
    m_Controls->AirwayResistance->GetValue(m_Controls->Action.GetAirwayResistance());
  }
  m_Controls->Action.SetType((eIntubation_Type)m_Controls->Type->GetIndex());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QIntubationWidget::ActionToControls(const SEIntubation& action)
{
  QActionWidget::ActionToControls(action);
  QComboBox* typeBox = m_Controls->Type->GetComboBox();
  switch (action.GetType())
  {
  case eIntubation_Type::Esophageal:
    typeBox->setCurrentIndex(typeBox->findText("Esophageal"));
    break;
  case eIntubation_Type::LeftMainstem:
    typeBox->setCurrentIndex(typeBox->findText("LeftMainstem"));
    break;
  case eIntubation_Type::RightMainstem:
    typeBox->setCurrentIndex(typeBox->findText("RightMainstem"));
    break;
  case eIntubation_Type::Tracheal:
    typeBox->setCurrentIndex(typeBox->findText("Tracheal"));
    break;
  default:
    typeBox->setCurrentIndex(typeBox->findText("Off"));
    break;
  }
  if (action.HasSeverity())
  {
    m_Controls->Severity->EnableInput(true);
    m_Controls->AirwayResistance->EnableInput(false);
    SEScalar0To1 Severity;
    Severity.SetValue(action.GetSeverity());
    m_Controls->Severity->SetValue(Severity);
  }
  else if (action.HasAirwayResistance())
  {
    m_Controls->Severity->EnableInput(false);
    m_Controls->AirwayResistance->EnableInput(true);
    SEScalarPressureTimePerVolume AirwayResistance;
    AirwayResistance.SetValue(action.GetAirwayResistance(PressureTimePerVolumeUnit::cmH2O_s_Per_mL), PressureTimePerVolumeUnit::cmH2O_s_Per_mL);
    m_Controls->AirwayResistance->SetValue(AirwayResistance);
  }
}
