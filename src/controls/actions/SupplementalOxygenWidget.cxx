/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/SupplementalOxygenWidget.h"
#include "controls/LabeledComboBox.h"
#include "controls/ScalarQuantityWidget.h"
#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalarVolume.h"
#include "pulse/cdm/properties/SEScalarVolumePerTime.h"

class QSupplementalOxygenWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                                   Pulse;
  SESupplementalOxygen                      Action;
  QLabeledComboBox*                         Devices;
  QScalarQuantityWidget<VolumePerTimeUnit>* Flow;
  QScalarQuantityWidget<VolumeUnit>*        Volume;
};

QSupplementalOxygenWidget::QSupplementalOxygenWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);

  std::vector<QString> devices = {"None", "NasalCannula", "SimpleMask", "NonRebreatherMask" };
  m_Controls->Devices = new QLabeledComboBox(this, "Devices", devices, 120);
  m_Controls->Flow = new QScalarQuantityWidget<VolumePerTimeUnit>("Flow", 0, 100, 1, VolumePerTimeUnit::L_Per_min, ScalarOptionWidget::None, this);
  m_Controls->Volume = new QScalarQuantityWidget<VolumeUnit>("Volume", 0, 5000, 1, VolumeUnit::L, ScalarOptionWidget::None, this);

  Properties()->layout()->addWidget(m_Controls->Devices);
  Properties()->layout()->addWidget(m_Controls->Flow);
  Properties()->layout()->addWidget(m_Controls->Volume);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QSupplementalOxygenWidget::~QSupplementalOxygenWidget()
{
  delete m_Controls;
}

void QSupplementalOxygenWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->Flow->Reset();
  m_Controls->Volume->Reset();
}

SEAction& QSupplementalOxygenWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QSupplementalOxygenWidget::GetAction() const
{
  return m_Controls->Action;
}

void QSupplementalOxygenWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->Flow->EnableInput(b);
  m_Controls->Flow->setEnabled(b);
  m_Controls->Volume->EnableInput(b);
  m_Controls->Volume->setEnabled(b);
  m_Controls->Devices->SetEnabled(b);
}

void QSupplementalOxygenWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  m_Controls->Flow->GetValue(m_Controls->Action.GetFlow());
  m_Controls->Volume->GetValue(m_Controls->Action.GetVolume());
  m_Controls->Action.SetDevice((eSupplementalOxygen_Device)m_Controls->Devices->GetIndex());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QSupplementalOxygenWidget::ActionToControls(const SESupplementalOxygen& action)
{
  QActionWidget::ActionToControls(action);
  SEScalarVolumePerTime flowData;
  flowData.SetValue(action.GetFlow(VolumePerTimeUnit::L_Per_min), VolumePerTimeUnit::L_Per_min);
  m_Controls->Flow->SetValue(flowData);

  SEScalarVolume volumeData;
  volumeData.SetValue(action.GetVolume(VolumeUnit::L), VolumeUnit::L);
  m_Controls->Volume->SetValue(volumeData);

  QComboBox* deviceBox = m_Controls->Devices->GetComboBox();
  switch (action.GetDevice())
  {
  case eSupplementalOxygen_Device::NasalCannula:
    deviceBox->setCurrentIndex(deviceBox->findText("NasalCannula"));
    break;
  case eSupplementalOxygen_Device::NonRebreatherMask:
    deviceBox->setCurrentIndex(deviceBox->findText("NonRebreatherMask"));
    break;
  case eSupplementalOxygen_Device::SimpleMask:
    deviceBox->setCurrentIndex(deviceBox->findText("SimpleMask"));
    break;
  default:
    deviceBox->setCurrentIndex(deviceBox->findText("None"));
    break;
  }

}
