/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/TubeThoracostomyWidget.h"
#include "controls/LabeledComboBox.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalarVolumePerTime.h"

class QTubeThoracostomyWidget::Controls // based off of chronic anemia widget
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                                   Pulse;
  SETubeThoracostomy                        Action;
  QLabeledComboBox*                         Side;
  QScalarQuantityWidget<VolumePerTimeUnit>* FlowRate;

};

QTubeThoracostomyWidget::QTubeThoracostomyWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  std::vector<QString> sideOptions = { "Left", "Right" };
  m_Controls->Side = new QLabeledComboBox(this, "Side", sideOptions);
  Properties()->layout()->addWidget(m_Controls->Side);

  m_Controls->FlowRate = new QScalarQuantityWidget<VolumePerTimeUnit>("Flow Rate", 0, 1000, 0.1, VolumePerTimeUnit::mL_Per_s, ScalarOptionWidget::None, this);
  m_Controls->FlowRate->AddUnit(VolumePerTimeUnit::mL_Per_min);
  m_Controls->FlowRate->AddUnit(VolumePerTimeUnit::L_Per_s);
  m_Controls->FlowRate->AddUnit(VolumePerTimeUnit::L_Per_min);
  Properties()->layout()->addWidget(m_Controls->FlowRate);

  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QTubeThoracostomyWidget::~QTubeThoracostomyWidget()
{
  delete m_Controls;
}

void QTubeThoracostomyWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->FlowRate->Reset();
}

SEAction& QTubeThoracostomyWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QTubeThoracostomyWidget::GetAction() const
{
  return m_Controls->Action;
}

void QTubeThoracostomyWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->Side->SetEnabled(b);
  m_Controls->FlowRate->EnableInput(b);
}

void QTubeThoracostomyWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  (m_Controls->Side->GetIndex() == 0) ?
    m_Controls->Action.SetSide(eSide::Left) :
    m_Controls->Action.SetSide(eSide::Right);
  m_Controls->FlowRate->GetValue(m_Controls->Action.GetFlowRate());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QTubeThoracostomyWidget::ActionToControls(const SETubeThoracostomy& action)
{
  QActionWidget::ActionToControls(action);
  QComboBox* sideBox = m_Controls->Side->GetComboBox();

  switch (action.GetSide())
  {
  case eSide::Right:
    sideBox->setCurrentIndex(sideBox->findText("Right"));
    break;
  default:
    sideBox->setCurrentIndex(sideBox->findText("Left"));
    break;
  }

  SEScalarVolumePerTime rateQuantity;
  rateQuantity.SetValue(action.GetFlowRate(VolumePerTimeUnit::mL_Per_min), VolumePerTimeUnit::mL_Per_min);
  m_Controls->FlowRate->SetValue(rateQuantity);
}
