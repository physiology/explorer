/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QWidget>
#include "actions/ActionWidget.h"

class QConsciousRespirationWidget : public QActionWidget
{
  Q_OBJECT
public:
  QConsciousRespirationWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QConsciousRespirationWidget();

  virtual void reset();
  virtual const SEAction& getAction();
  virtual void setAction(const SEAction& c);
  virtual void setEnabled(bool b);
  
private:
  class Controls;
  Controls* m_Controls;
  
};