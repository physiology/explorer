/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QWidget>
#include "controls/actions/ActionWidget.h"
#include "pulse/cdm/system/equipment/mechanical_ventilator/actions/SEMechanicalVentilatorHold.h"

class QMechanicalVentilatorHoldWidget : public QActionWidget
{
  Q_OBJECT
public:
  using ActionType = SEMechanicalVentilatorHold;

  QMechanicalVentilatorHoldWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QMechanicalVentilatorHoldWidget();

  virtual void Reset() override;
  virtual void SetEnabled(bool b) override;
  virtual const SEAction& GetAction() const override;

  virtual void ControlsToAction() override;
  virtual void ActionToControls(const SEMechanicalVentilatorHold& action);

protected:
  virtual SEAction& GetAction() override;

private:
  class Controls;
  Controls* m_Controls;
  
};
