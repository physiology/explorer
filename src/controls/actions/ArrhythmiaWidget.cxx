/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/ArrhythmiaWidget.h"
#include "controls/LabeledComboBox.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QArrhythmiaWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&           Pulse;
  SEArrhythmia      Action;
  QLabeledComboBox* Type;
};

QArrhythmiaWidget::QArrhythmiaWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  std::vector<QString> typeOptions =
  {
    "NormalSinus",
    "SinusBradycardia",
    "SinusTachycardia",
    "SinusPulselessElectricalActivity",
    "Asystole",
    "CoarseVentricularFibrillation",
    "FineVentricularFibrillation",
    "PulselessVentricularTachycardia",
    "StableVentricularTachycardia",
    "UnstableVentricularTachycardia"
  };
  m_Controls->Type = new QLabeledComboBox(this, "Type", typeOptions, 200);
  m_Controls->Type->setMinimumWidth(180);
  Properties()->layout()->addWidget(m_Controls->Type);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QArrhythmiaWidget::~QArrhythmiaWidget()
{
  delete m_Controls;
}

void QArrhythmiaWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
}

SEAction& QArrhythmiaWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QArrhythmiaWidget::GetAction() const
{
  return m_Controls->Action;
}

void QArrhythmiaWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->Type->SetEnabled(b);
}

void QArrhythmiaWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  m_Controls->Action.SetRhythm((eHeartRhythm)m_Controls->Type->GetIndex());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QArrhythmiaWidget::ActionToControls(const SEArrhythmia& action)
{
  QActionWidget::ActionToControls(action);
  action.GetRhythm();
  QComboBox* typeBox = m_Controls->Type->GetComboBox();
  typeBox->setCurrentIndex(typeBox->findText(QString::fromStdString(eHeartRhythm_Name(action.GetRhythm()))));
}
