/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QWidget>
#include "controls/actions/ActionWidget.h"
#include "pulse/cdm/patient/actions/SETensionPneumothorax.h"

class QTensionPneumothoraxWidget : public QActionWidget
{
  Q_OBJECT
public:
  using ActionType = SETensionPneumothorax;

  QTensionPneumothoraxWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QTensionPneumothoraxWidget();

  virtual void Reset() override;
  virtual void SetEnabled(bool b) override;
  virtual const SEAction& GetAction() const override;

  virtual void ControlsToAction() override;
  virtual void ActionToControls(const SETensionPneumothorax& action);

protected:
  virtual SEAction& GetAction() override;

private:
  class Controls;
  Controls* m_Controls;
  
};
