/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QWidget>
#include "controls/actions/ActionWidget.h"
#include "pulse/cdm/patient/actions/SEHemothorax.h"

class QHemothoraxWidget : public QActionWidget
{
  Q_OBJECT
public:
  using ActionType = SEHemothorax;

  QHemothoraxWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QHemothoraxWidget();

  virtual void Reset() override;
  virtual void SetEnabled(bool b) override;
  virtual const SEAction& GetAction() const override;

  virtual void ControlsToAction() override;
  virtual void ActionToControls(const SEHemothorax& action);

protected:
  virtual SEAction& GetAction() override;

protected slots:
  void EnableFlowRate();
  void EnableSeverity();

private:
  class Controls;
  Controls* m_Controls;
  
};
