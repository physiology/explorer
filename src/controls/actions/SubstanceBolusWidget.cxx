/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/LabeledComboBox.h"
#include "controls/PulseData.h"
#include "controls/ScalarQuantityWidget.h"
#include "controls/actions/SubstanceBolusWidget.h"

#include "pulse/engine/PulseEngine.h"
#include "pulse/engine/PulseScenario.h"
#include "pulse/cdm/properties/SEScalarVolume.h"
#include "pulse/cdm/properties/SEScalarMassPerVolume.h"
#include "pulse/cdm/substance/SESubstance.h"
#include "pulse/cdm/substance/SESubstanceManager.h"

class QSubstanceBolusWidget::Controls // based off of chronic anemia widget
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                                   Pulse;
  SESubstanceBolus*                         Action = nullptr;
  std::map<std::string, SESubstance*>       Substances;
  QScalarQuantityWidget<VolumeUnit>*        Dose;
  QScalarQuantityWidget<MassPerVolumeUnit>* Concentration;
  QLabeledComboBox*                         AdministrationRoute;
  QLabeledComboBox*                         Drugs;
};

QSubstanceBolusWidget::QSubstanceBolusWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->Dose = new QScalarQuantityWidget<VolumeUnit>("Dose", 0, 1000, 1, VolumeUnit::mL, ScalarOptionWidget::None, this);
  m_Controls->Concentration = new QScalarQuantityWidget<MassPerVolumeUnit>("Concentration", 0, 10000, 1, MassPerVolumeUnit::ug_Per_mL, ScalarOptionWidget::None, this);
  m_Controls->Concentration->AddUnit(MassPerVolumeUnit::mg_Per_mL);
  m_Controls->Concentration->AddUnit(MassPerVolumeUnit::mg_Per_L);
  m_Controls->Concentration->AddUnit(MassPerVolumeUnit::g_Per_mL);
  m_Controls->Concentration->AddUnit(MassPerVolumeUnit::g_Per_L);
  m_Controls->Concentration->AddUnit(MassPerVolumeUnit::kg_Per_L);

  std::vector<QString> adminOptions = { "Intravenous", "Intraarterial","Intramuscular" };
  m_Controls->AdministrationRoute = new QLabeledComboBox(this, "AdministrationRoute", adminOptions, 85);

  const auto& drugs = PulseData::Drugs;
  m_Controls->Drugs = new QLabeledComboBox(this, "Drugs", drugs, 120);
  for (QString d : drugs)
  {
    SESubstance* drug = qp.GetScenario().GetSubstanceManager().GetSubstance(d.toStdString());
    m_Controls->Substances[drug->GetName()] = drug;
  }
 
  SetEnabled(false);

  Properties()->layout()->addWidget(m_Controls->Drugs);
  Properties()->layout()->addWidget(m_Controls->Dose);
  Properties()->layout()->addWidget(m_Controls->AdministrationRoute);
  Properties()->layout()->addWidget(m_Controls->Concentration);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QSubstanceBolusWidget::~QSubstanceBolusWidget()
{
  SAFE_DELETE(m_Controls->Action);
  delete m_Controls;
}

void QSubstanceBolusWidget::Reset()
{
  QActionWidget::Reset();
  SAFE_DELETE(m_Controls->Action);
}

SEAction& QSubstanceBolusWidget::GetAction()
{
  return *m_Controls->Action;
}
const SEAction& QSubstanceBolusWidget::GetAction() const
{
  return *m_Controls->Action;
}

void QSubstanceBolusWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->Dose->setEnabled(b);
  m_Controls->Dose->EnableInput(b);
  m_Controls->Concentration->setEnabled(b);
  m_Controls->Concentration->EnableInput(b);
  m_Controls->AdministrationRoute->SetEnabled(b);
  m_Controls->Drugs->SetEnabled(b);
}

void QSubstanceBolusWidget::ControlsToAction()
{
  if (m_Controls->Action != nullptr)
    SAFE_DELETE(m_Controls->Action);
  m_Controls->Action = new SESubstanceBolus(*m_Controls->Substances[m_Controls->Drugs->GetText()]);
  switch (m_Controls->AdministrationRoute->GetIndex())
  {
  case 0:
    m_Controls->Action->SetAdminRoute(eSubstanceAdministration_Route::Intravenous);
    break;
  case 1:
    m_Controls->Action->SetAdminRoute(eSubstanceAdministration_Route::Intraarterial);
    break;
  case 2:
    m_Controls->Action->SetAdminRoute(eSubstanceAdministration_Route::Intramuscular);
    break;
  }
  m_Controls->Dose->GetValue(m_Controls->Action->GetDose());
  m_Controls->Concentration->GetValue(m_Controls->Action->GetConcentration());
  QActionWidget::ControlsToAction();
  emit UpdateAction(*m_Controls->Action, GetProcessTime());
}


void QSubstanceBolusWidget::ActionToControls(const SESubstanceBolus& action)
{
  QActionWidget::ActionToControls(action);
  QComboBox* adminBox = m_Controls->AdministrationRoute->GetComboBox();
  switch (action.GetAdminRoute())
  {
  case eSubstanceAdministration_Route::Intravenous:
    adminBox->setCurrentIndex(adminBox->findText("Intravenous"));
    break;
  case eSubstanceAdministration_Route::Intraarterial:
    adminBox->setCurrentIndex(adminBox->findText("Intraarterial"));
    break;
  default:
    adminBox->setCurrentIndex(adminBox->findText("Intramuscular"));
    break;
  }
  SEScalarMassPerVolume concentrationData;
  concentrationData.SetValue(action.GetConcentration(MassPerVolumeUnit::ug_Per_mL), MassPerVolumeUnit::ug_Per_mL);
  m_Controls->Concentration->SetValue(concentrationData);

  SEScalarVolume doseData;
  doseData.SetValue(action.GetDose(VolumeUnit::mL), VolumeUnit::mL);
  m_Controls->Dose->SetValue(doseData);

  QComboBox* drugsBox = m_Controls->Drugs->GetComboBox();
  drugsBox->setCurrentIndex(drugsBox->findText(QString::fromStdString(action.GetSubstance().GetName())));
}
