/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/ExerciseWidget.h"
#include "controls/ScalarWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"

class QExerciseWidget::Controls // based off of chronic anemia widget
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&        Pulse;
  SEExercise     Action;
  QScalarWidget* Intensity; // check this
};

QExerciseWidget::QExerciseWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->Intensity = new QScalarWidget("Intensity", 0, 1, 0.01, ScalarOptionWidget::None, this); //check this
  Properties()->layout()->addWidget(m_Controls->Intensity);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QExerciseWidget::~QExerciseWidget()
{
  delete m_Controls;
}

void QExerciseWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->Intensity->Reset();
}

SEAction& QExerciseWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QExerciseWidget::GetAction() const
{
  return m_Controls->Action;
}

void QExerciseWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->Intensity->EnableInput(b);
}

void QExerciseWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  m_Controls->Intensity->GetValue(m_Controls->Action.GetIntensity());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QExerciseWidget::ActionToControls(const SEExercise& action)
{
  QActionWidget::ActionToControls(action);
  SEScalar data;
  data.SetValue(action.GetIntensity());
  m_Controls->Intensity->SetValue(data);
}
