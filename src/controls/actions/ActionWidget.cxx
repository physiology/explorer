/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "ActionWidget.h"
#include "ui_Action.h"
#include <QMessageBox>

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/engine/SEAction.h"

class QActionWidget::Controls : public Ui::ActionWidget
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                          Pulse;
  bool                             ProcessAction = false;
  SEScalarTime                     ProcessTime;
  QTimeWidget*                     ProcessTimeCtrl;
};

QActionWidget::QActionWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QWidget(parent,flags)
{
  m_Controls = new Controls(qp);
  m_Controls->setupUi(this);
  m_Controls->ProcessTimeCtrl = new QTimeWidget(this, "Process Time (hh:mm:ss)");

  connect(m_Controls->Apply, SIGNAL(clicked()), this, SLOT(ApplyAction()));
}

QActionWidget::~QActionWidget()
{
  delete m_Controls;
}

void QActionWidget::Reset()
{
  m_Controls->ProcessTimeCtrl->Reset();
}

void QActionWidget::SetEnabled(bool b)
{
  m_Controls->Apply->setEnabled(b);
  m_Controls->ProcessTimeCtrl->SetEnabled(b);
  m_Controls->CommentText->setEnabled(b);
}

void QActionWidget::ShowProcessTimeCtrl(bool b)
{
  b ? m_Controls->ProcessTimeCtrl->show() :
    m_Controls->ProcessTimeCtrl->hide();
}

void QActionWidget::SetProcessTime(const SEScalarTime& t)
{
  m_Controls->ProcessTimeCtrl->SetTime(t);
}

bool QActionWidget::ProcessAction()
{
  return m_Controls->ProcessAction;
}

void QActionWidget::ApplyAction()
{
  ControlsToAction();
}

void QActionWidget::ControlsToAction()
{
  GetAction().SetComment(m_Controls->CommentText->toPlainText().toStdString());
  m_Controls->ProcessAction = m_Controls->Pulse.IsRunning();
  if (m_Controls->ProcessAction)
    m_Controls->ProcessTime.Invalidate();
  else
    m_Controls->ProcessTimeCtrl->GetTime(m_Controls->ProcessTime);
}

void QActionWidget::ActionToControls(const SEAction& action)
{
  m_Controls->CommentText->setText(QString::fromStdString(action.GetComment()));
}

SEScalarTime& QActionWidget::GetProcessTime()
{
  return m_Controls->ProcessTime;
}
QTimeWidget* QActionWidget::GetProcessTimeCtrl()
{
  return m_Controls->ProcessTimeCtrl;
}

QWidget* QActionWidget::Properties()
{
  return m_Controls->Properties;
}

void QActionWidget::AtSteadyState(PhysiologyEngine& pulse)
{
  // Nothing expected here
}
void QActionWidget::AtSteadyStateUpdateUI()
{
  // Nothing expected here
}

void QActionWidget::ProcessPhysiology(PhysiologyEngine& pulse)
{// This is called from a thread, you should NOT update UI here
  // This is where we pull data from pulse, and push any actions to it
  if (!m_Controls->ProcessAction)
    return;

  const SEAction& action = GetAction();
  if (action.IsValid())
  {
    pulse.ProcessAction(action);
    m_Controls->ProcessAction = false;
  }
}
void QActionWidget::PhysiologyUpdateUI(const std::vector<SEAction const*>& actions)
{// This is called from a slot, you can update UI here

  // Nothing is expected to be here
}
