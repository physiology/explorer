/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/ChestCompressionAutomatedWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"
#include "pulse/cdm/properties/SEScalarForce.h"
#include "pulse/cdm/properties/SEScalarFrequency.h"
#include "pulse/cdm/properties/SEScalarLength.h"

class QChestCompressionAutomatedWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse& Pulse;
  SEChestCompressionAutomated           Action;
  QScalarQuantityWidget<ForceUnit>*     Force;
  QScalarQuantityWidget<LengthUnit>*    Depth;
  QScalarQuantityWidget<FrequencyUnit>* CompressionFrequency;
  QScalarWidget*                        AppliedForceFraction;
};

QChestCompressionAutomatedWidget::QChestCompressionAutomatedWidget(QPulse& qp, QWidget* parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);

  m_Controls->Force = new QScalarQuantityWidget<ForceUnit>("Force", 0, 500, 10, ForceUnit::N, ScalarOptionWidget::Radio, this);
  Properties()->layout()->addWidget(m_Controls->Force);

  m_Controls->Depth = new QScalarQuantityWidget<LengthUnit>("Depth", 0, 10, 1, LengthUnit::cm, ScalarOptionWidget::Radio, this);
  m_Controls->Depth->AddUnit(LengthUnit::in);
  Properties()->layout()->addWidget(m_Controls->Depth);

  connect(m_Controls->Force->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableForce()));
  connect(m_Controls->Depth->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableDepth()));

  m_Controls->CompressionFrequency = new QScalarQuantityWidget<FrequencyUnit>("CompressionFrequency", 0, 150, 5, FrequencyUnit::Per_min, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->CompressionFrequency);

  m_Controls->AppliedForceFraction = new QScalarWidget("AppliedForceFraction", 0, 1, .1, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->AppliedForceFraction);

  Properties()->layout()->addWidget(GetProcessTimeCtrl());

  Reset();
}

QChestCompressionAutomatedWidget::~QChestCompressionAutomatedWidget()
{
  delete m_Controls;
}

void QChestCompressionAutomatedWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->Force->Reset();
  m_Controls->Depth->Reset();
  m_Controls->CompressionFrequency->Reset();
  m_Controls->AppliedForceFraction->Reset();
}

SEAction& QChestCompressionAutomatedWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QChestCompressionAutomatedWidget::GetAction() const
{
  return m_Controls->Action;
}

void QChestCompressionAutomatedWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  if (!b)
  {
    m_Controls->Force->FullDisable();
    m_Controls->Depth->FullDisable();
  }
  else
  {
    m_Controls->Force->FullEnable();
    m_Controls->Depth->FullEnable();
    m_Controls->Force->EnableInput(!b);
    m_Controls->Depth->EnableInput(b);
  }
  m_Controls->CompressionFrequency->EnableInput(b);
  m_Controls->CompressionFrequency->setEnabled(b);

  m_Controls->AppliedForceFraction->EnableInput(b);
  m_Controls->AppliedForceFraction->setEnabled(b);
}

void QChestCompressionAutomatedWidget::EnableForce()
{
  bool b = m_Controls->Force->GetRadioButton()->isChecked();
  m_Controls->Force->EnableInput(b);
  m_Controls->Depth->EnableInput(!b);
}
void QChestCompressionAutomatedWidget::EnableDepth()
{
  bool b = m_Controls->Depth->GetRadioButton()->isChecked();
  m_Controls->Force->EnableInput(!b);
  m_Controls->Depth->EnableInput(b);
}

void QChestCompressionAutomatedWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  if (m_Controls->Force->IsChecked())
  {
    m_Controls->Action.GetDepth().Invalidate();
    m_Controls->Force->GetValue(m_Controls->Action.GetForce());
  }
  else
  {
    m_Controls->Action.GetForce().Invalidate();
    m_Controls->Depth->GetValue(m_Controls->Action.GetDepth());
  }
  m_Controls->CompressionFrequency->GetValue(m_Controls->Action.GetCompressionFrequency());
  m_Controls->AppliedForceFraction->GetValue(m_Controls->Action.GetAppliedForceFraction());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QChestCompressionAutomatedWidget::ActionToControls(const SEChestCompressionAutomated& action)
{
  QActionWidget::ActionToControls(action);

  if (action.HasForce())
  {
    m_Controls->Force->EnableInput(true);
    m_Controls->Depth->EnableInput(false);
    SEScalarForce force;
    force.SetValue(action.GetForce(ForceUnit::N), ForceUnit::N);
    m_Controls->Force->SetValue(force);
  }
  else if (action.HasDepth())
  {
    m_Controls->Force->EnableInput(false);
    m_Controls->Depth->EnableInput(true);
    SEScalarLength Depth;
    Depth.SetValue(action.GetDepth(LengthUnit::cm), LengthUnit::cm);
    m_Controls->Depth->SetValue(Depth);
  }
  SEScalarFrequency freq;
  freq.SetValue(action.GetCompressionFrequency(FrequencyUnit::Per_min), FrequencyUnit::Per_min);
  m_Controls->CompressionFrequency->SetValue(freq);

  SEScalar0To1 aff;
  aff.SetValue(action.GetAppliedForceFraction());
  m_Controls->AppliedForceFraction->SetValue(aff);
}
