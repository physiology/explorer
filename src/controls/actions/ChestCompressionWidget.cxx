/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/ChestCompressionWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"
#include "pulse/cdm/properties/SEScalarForce.h"
#include "pulse/cdm/properties/SEScalarLength.h"
#include "pulse/cdm/properties/SEScalarTime.h"

class QChestCompressionWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                           Pulse;
  SEChestCompression                Action;
  QScalarQuantityWidget<ForceUnit>* Force;
  QScalarQuantityWidget<LengthUnit>* Depth;
  QScalarQuantityWidget<TimeUnit>*  CompressionPeriod;
};

QChestCompressionWidget::QChestCompressionWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);

  m_Controls->Force = new QScalarQuantityWidget<ForceUnit>("Force", 0, 500, 10, ForceUnit::N, ScalarOptionWidget::Radio, this);
  Properties()->layout()->addWidget(m_Controls->Force);

  m_Controls->Depth = new QScalarQuantityWidget<LengthUnit>("Depth", 0, 10, 1, LengthUnit::cm, ScalarOptionWidget::Radio, this);
  m_Controls->Depth->AddUnit(LengthUnit::in);
  Properties()->layout()->addWidget(m_Controls->Depth);

  connect(m_Controls->Force->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableForce()));
  connect(m_Controls->Depth->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableDepth()));

  m_Controls->CompressionPeriod = new QScalarQuantityWidget<TimeUnit>("CompressionPeriod", 0, 5, .1, TimeUnit::s, ScalarOptionWidget::None, this);
  Properties()->layout()->addWidget(m_Controls->CompressionPeriod);

  Properties()->layout()->addWidget(GetProcessTimeCtrl());

  Reset();
}

QChestCompressionWidget::~QChestCompressionWidget()
{
  delete m_Controls;
}

void QChestCompressionWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->Force->Reset();
  m_Controls->Depth->Reset();
  m_Controls->CompressionPeriod->Reset();
}

SEAction& QChestCompressionWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QChestCompressionWidget::GetAction() const
{
  return m_Controls->Action;
}

void QChestCompressionWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  if (!b)
  {
    m_Controls->Force->FullDisable();
    m_Controls->Depth->FullDisable();
  }
  else
  {
    m_Controls->Force->FullEnable();
    m_Controls->Depth->FullEnable();
    m_Controls->Force->EnableInput(!b);
    m_Controls->Depth->EnableInput(b);
  }
  m_Controls->CompressionPeriod->EnableInput(b);
  m_Controls->CompressionPeriod->setEnabled(b);
}

void QChestCompressionWidget::EnableForce()
{
  bool b = m_Controls->Force->GetRadioButton()->isChecked();
  m_Controls->Force->EnableInput(b);
  m_Controls->Depth->EnableInput(!b);
}
void QChestCompressionWidget::EnableDepth()
{
  bool b = m_Controls->Depth->GetRadioButton()->isChecked();
  m_Controls->Force->EnableInput(!b);
  m_Controls->Depth->EnableInput(b);
}

void QChestCompressionWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  if (m_Controls->Force->IsChecked())
  {
    m_Controls->Action.GetDepth().Invalidate();
    m_Controls->Force->GetValue(m_Controls->Action.GetForce());
  }
  else
  {
    m_Controls->Action.GetForce().Invalidate();
    m_Controls->Depth->GetValue(m_Controls->Action.GetDepth());
  }
  m_Controls->CompressionPeriod->GetValue(m_Controls->Action.GetCompressionPeriod());
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QChestCompressionWidget::ActionToControls(const SEChestCompression& action)
{
  QActionWidget::ActionToControls(action);

  if (action.HasForce())
  {
    m_Controls->Force->EnableInput(true);
    m_Controls->Depth->EnableInput(false);
    SEScalarForce force;
    force.SetValue(action.GetForce(ForceUnit::N), ForceUnit::N);
    m_Controls->Force->SetValue(force);
  }
  else if (action.HasDepth())
  {
    m_Controls->Force->EnableInput(false);
    m_Controls->Depth->EnableInput(true);
    SEScalarLength Depth;
    Depth.SetValue(action.GetDepth(LengthUnit::cm), LengthUnit::cm);
    m_Controls->Depth->SetValue(Depth);
  }
  SEScalarTime time;
  time.SetValue(action.GetCompressionPeriod(TimeUnit::s), TimeUnit::s);
  m_Controls->CompressionPeriod->SetValue(time);
}
