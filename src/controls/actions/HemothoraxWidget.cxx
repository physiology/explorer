/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/HemothoraxWidget.h"
#include "controls/LabeledComboBox.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/PhysiologyEngine.h"
#include "pulse/cdm/properties/SEScalar0To1.h"
#include "pulse/cdm/properties/SEScalarVolumePerTime.h"

class QHemothoraxWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                Pulse;
  SEHemothorax  Action;
  QLabeledComboBox*                         Side;
  QScalarWidget*                            Severity;
  QScalarQuantityWidget<VolumePerTimeUnit>* FlowRate;

};

QHemothoraxWidget::QHemothoraxWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->Severity = new QScalarWidget("Severity", 0, 1, 0.1, ScalarOptionWidget::None, this);

  std::vector<QString> sideOptions = {"Left", "Right"};
  m_Controls->Side = new QLabeledComboBox(this, "Side", sideOptions);
  Properties()->layout()->addWidget(m_Controls->Side);

  m_Controls->Severity = new QScalarWidget("Severity", 0, 1, 0.1, ScalarOptionWidget::Radio, this);
  layout()->addWidget(m_Controls->Severity);
  Properties()->layout()->addWidget(m_Controls->Severity);

  m_Controls->FlowRate = new QScalarQuantityWidget<VolumePerTimeUnit>("Flow Rate", 0, 1000, 0.1, VolumePerTimeUnit::mL_Per_s, ScalarOptionWidget::Radio, this);
  m_Controls->FlowRate->AddUnit(VolumePerTimeUnit::mL_Per_min);
  m_Controls->FlowRate->AddUnit(VolumePerTimeUnit::L_Per_s);
  m_Controls->FlowRate->AddUnit(VolumePerTimeUnit::L_Per_min);
  Properties()->layout()->addWidget(m_Controls->FlowRate);

  connect(m_Controls->Severity->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableSeverity()));
  connect(m_Controls->FlowRate->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableFlowRate()));

  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QHemothoraxWidget::~QHemothoraxWidget()
{
  delete m_Controls;
}

void QHemothoraxWidget::Reset()
{
  QActionWidget::Reset();
  m_Controls->Action.Clear();
  m_Controls->Severity->Reset();
  m_Controls->FlowRate->Reset();
}

SEAction& QHemothoraxWidget::GetAction()
{
  return m_Controls->Action;
}
const SEAction& QHemothoraxWidget::GetAction() const
{
  return m_Controls->Action;
}

void QHemothoraxWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->Side->SetEnabled(b);

  if (!b)
  {
    m_Controls->FlowRate->FullDisable();
    m_Controls->Severity->FullDisable();
  }
  else
  {
    m_Controls->FlowRate->FullEnable();
    m_Controls->Severity->FullEnable();
    m_Controls->FlowRate->EnableInput(!b);
    m_Controls->Severity->EnableInput(b);
  }
}

void QHemothoraxWidget::EnableFlowRate()
{
  bool b = m_Controls->FlowRate->GetRadioButton()->isChecked();
  m_Controls->FlowRate->EnableInput(b);
  m_Controls->Severity->EnableInput(!b);
}
void QHemothoraxWidget::EnableSeverity()
{
  bool b = m_Controls->Severity->GetRadioButton()->isChecked();
  m_Controls->FlowRate->EnableInput(!b);
  m_Controls->Severity->EnableInput(b);
}

void QHemothoraxWidget::ControlsToAction()
{
  QActionWidget::ControlsToAction();
  m_Controls->Severity->GetValue(m_Controls->Action.GetSeverity());
  (m_Controls->Side->GetText() == "Left") ?
    m_Controls->Action.SetSide(eSide::Left) :
    m_Controls->Action.SetSide(eSide::Right);
  if (m_Controls->Severity->IsChecked())
  {
    m_Controls->Action.GetFlowRate().Invalidate();
    m_Controls->Severity->GetValue(m_Controls->Action.GetSeverity());
  }
  else
  {
    m_Controls->Action.GetSeverity().Invalidate();
    m_Controls->FlowRate->GetValue(m_Controls->Action.GetFlowRate());
  }
  emit UpdateAction(m_Controls->Action, GetProcessTime());
}

void QHemothoraxWidget::ActionToControls(const SEHemothorax& action)
{
  QActionWidget::ActionToControls(action);
  SEScalar data;
  data.SetValue(action.GetSeverity());
  m_Controls->Severity->SetValue(data);

  QComboBox* sideBox = m_Controls->Side->GetComboBox();
  switch (action.GetSide()) {
  case eSide::Left:
    sideBox->setCurrentIndex(sideBox->findText("Left"));
    break;
  default:
    sideBox->setCurrentIndex(sideBox->findText("Right"));
    break;
  }

  if (action.HasSeverity())
  {
    m_Controls->FlowRate->EnableInput(false);
    m_Controls->Severity->EnableInput(true);
    SEScalar0To1 severity;
    severity.SetValue(action.GetSeverity());
    m_Controls->Severity->SetValue(severity);
  }
  else if (action.HasFlowRate())
  {
    m_Controls->FlowRate->EnableInput(true);
    m_Controls->Severity->EnableInput(false);
    SEScalarVolumePerTime rateQuantity;
    rateQuantity.SetValue(action.GetFlowRate(VolumePerTimeUnit::mL_Per_s), VolumePerTimeUnit::mL_Per_s);
    m_Controls->FlowRate->SetValue(rateQuantity);
  }
}
