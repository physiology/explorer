/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "controls/actions/SubstanceCompoundInfusionWidget.h"
#include "controls/ScalarQuantityWidget.h"
#include "controls/LabeledComboBox.h"

#include "pulse/engine/PulseEngine.h"
#include "pulse/engine/PulseScenario.h"
#include "pulse/cdm/substance/SESubstanceCompound.h"
#include "pulse/cdm/substance/SESubstanceManager.h"
#include "pulse/cdm/properties/SEScalarVolume.h"
#include "pulse/cdm/properties/SEScalarVolumePerTime.h"

class QSubstanceCompoundInfusionWidget::Controls
{
public:
  Controls(QPulse& qp) : Pulse(qp) {}
  QPulse&                                     Pulse;
  SESubstanceCompoundInfusion*                Action = nullptr;
  std::map<std::string, SESubstanceCompound*> SubstanceCompounds;
  QScalarQuantityWidget<VolumeUnit>*          BagVolume;
  QScalarQuantityWidget<VolumePerTimeUnit>*   Rate;
  QLabeledComboBox*                           Compounds;
};

QSubstanceCompoundInfusionWidget::QSubstanceCompoundInfusionWidget(QPulse& qp, QWidget *parent, Qt::WindowFlags flags) : QActionWidget(qp, parent, flags)
{
  m_Controls = new Controls(qp);
  m_Controls->BagVolume = new QScalarQuantityWidget<VolumeUnit>("Bag Volume", 0, 1000, 1, VolumeUnit::mL, ScalarOptionWidget::None, this); //check this
  m_Controls->BagVolume->AddUnit(VolumeUnit::L);

  m_Controls->Rate = new QScalarQuantityWidget<VolumePerTimeUnit>("Rate", 0, 1000, 1, VolumePerTimeUnit::mL_Per_s, ScalarOptionWidget::None, this);
  m_Controls->Rate->AddUnit(VolumePerTimeUnit::L_Per_min);
  m_Controls->Rate->AddUnit(VolumePerTimeUnit::mL_Per_min);
  m_Controls->Rate->AddUnit(VolumePerTimeUnit::L_Per_s);


  std::vector<QString> compounds = { "Saline", "Blood","PackedRBC" };
  m_Controls->Compounds = new QLabeledComboBox(this, "Compounds", compounds, 120);
  for (QString c : compounds)
  {
    SESubstanceCompound* compound = qp.GetScenario().GetSubstanceManager().GetCompound(c.toStdString());
    m_Controls->SubstanceCompounds[compound->GetName()] = compound;
  }
  Properties()->layout()->addWidget(m_Controls->Compounds);
  Properties()->layout()->addWidget(m_Controls->BagVolume);
  Properties()->layout()->addWidget(m_Controls->Rate);
  Properties()->layout()->addWidget(GetProcessTimeCtrl());
  Reset();
}

QSubstanceCompoundInfusionWidget::~QSubstanceCompoundInfusionWidget()
{
  SAFE_DELETE(m_Controls->Action);
  delete m_Controls;
}

void QSubstanceCompoundInfusionWidget::Reset()
{
  QActionWidget::Reset();
  SAFE_DELETE(m_Controls->Action);
}

SEAction& QSubstanceCompoundInfusionWidget::GetAction()
{
  return *m_Controls->Action;
}
const SEAction& QSubstanceCompoundInfusionWidget::GetAction() const
{
  return *m_Controls->Action;
}

void QSubstanceCompoundInfusionWidget::SetEnabled(bool b)
{
  QActionWidget::SetEnabled(b);
  m_Controls->Rate->setEnabled(b);
  m_Controls->Rate->EnableInput(b);
  m_Controls->BagVolume->setEnabled(b);
  m_Controls->BagVolume->EnableInput(b);
  m_Controls->Compounds->SetEnabled(b);
}

void QSubstanceCompoundInfusionWidget::ControlsToAction()
{
  if(m_Controls->Action != nullptr)
    SAFE_DELETE(m_Controls->Action);
  m_Controls->Action = new SESubstanceCompoundInfusion(*m_Controls->SubstanceCompounds[m_Controls->Compounds->GetText()]);
  m_Controls->Rate->GetValue(m_Controls->Action->GetRate());
  m_Controls->BagVolume->GetValue(m_Controls->Action->GetBagVolume());
  QActionWidget::ControlsToAction();
  emit UpdateAction(*m_Controls->Action, GetProcessTime());
}

void QSubstanceCompoundInfusionWidget::ActionToControls(const SESubstanceCompoundInfusion& action)
{
  QActionWidget::ActionToControls(action);
  SEScalarVolumePerTime rateData;
  rateData.SetValue(action.GetRate(VolumePerTimeUnit::mL_Per_s), VolumePerTimeUnit::mL_Per_s);
  m_Controls->Rate->SetValue(rateData);

  SEScalarVolume bagVolumeData;
  bagVolumeData.SetValue(action.GetBagVolume(VolumeUnit::mL), VolumeUnit::mL);
  m_Controls->BagVolume->SetValue(bagVolumeData);

  QComboBox* compoundBox = m_Controls->Compounds->GetComboBox();
  compoundBox->setCurrentIndex(compoundBox->findText(QString::fromStdString(action.GetSubstanceCompound().GetName())));
}
