/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/

#include "VentilatorWidget.h"
#include "ui_Ventilator.h"

#include <QLayout>
#include <QGraphicsLayout>

#include "pulse/cdm/engine/SEActionManager.h"
#include "pulse/cdm/engine/SEDataRequestManager.h"
#include "pulse/cdm/engine/SEEngineTracker.h"
#include "pulse/cdm/engine/SEEquipmentActionCollection.h"
#include "pulse/cdm/properties/SEScalar0To1.h"
#include "pulse/cdm/properties/SEScalarFrequency.h"
#include "pulse/cdm/properties/SEScalarPressure.h"
#include "pulse/cdm/properties/SEScalarMassPerVolume.h"
#include "pulse/cdm/properties/SEScalarTime.h"
#include "pulse/cdm/properties/SEScalarVolume.h"
#include "pulse/cdm/properties/SEScalarVolumePerPressure.h"
#include "pulse/cdm/properties/SEScalarVolumePerTime.h"
#include "pulse/cdm/system/equipment/mechanical_ventilator/SEMechanicalVentilator.h"
#include "pulse/cdm/system/equipment/mechanical_ventilator/actions/SEMechanicalVentilatorConfiguration.h"
#include "pulse/cdm/system/equipment/mechanical_ventilator/actions/SEMechanicalVentilatorContinuousPositiveAirwayPressure.h"
#include "pulse/cdm/system/equipment/mechanical_ventilator/actions/SEMechanicalVentilatorHold.h"
#include "pulse/cdm/system/equipment/mechanical_ventilator/actions/SEMechanicalVentilatorLeak.h"
#include "pulse/cdm/system/equipment/mechanical_ventilator/actions/SEMechanicalVentilatorPressureControl.h"
#include "pulse/cdm/system/equipment/mechanical_ventilator/actions/SEMechanicalVentilatorVolumeControl.h"
#include "pulse/cdm/system/physiology/SERespiratorySystem.h"
#include "pulse/engine/PulseEngine.h"

#include "QwtPulsePlot.h"
#include "LabeledComboBox.h"
#include "NumberWidget.h"
#include "QMutex"
#include "VentilatorHysteresisWidget.h"
#include "controls/ScalarWidget.h"
#include "controls/ScalarQuantityWidget.h"

struct QVentilatorWidget::Private : public Ui::VentilatorWidget
{
  Private(QVentilatorWidget* parent) : parent(parent)
  {
    setupUi(parent);
    SetupInitialValues();
    hysteresis = new QVentilatorHysteresisWidget(parent, Qt::WindowType::Window);
    hysteresis->hide();
    connect(hysteresisButton, &QPushButton::released, [this]()
    {
      if (hysteresis->isVisible()) hysteresis->hide();
      else hysteresis->show();
    });
  }

  const SESubstanceManager* subMgr;
  // Mode
  QVector<LabeledDialWidget*> dials;
  QVector<NumberWidget*> numbers;
  LabeledDialWidget* slopeWidget = nullptr;
  LabeledDialWidget* tiWidget = nullptr;
  QVentilatorWidget* parent = nullptr;
  QVentilatorHysteresisWidget* hysteresis = nullptr;
  // Triggers
  QScalarQuantityWidget<VolumePerTimeUnit>* inspirationPatientTriggerFlow;
  QScalarQuantityWidget<PressureUnit>* inspirationPatientTriggerPressure;
  QWidget*      modelTrigger;
  QRadioButton* modelRadio;
  QLabel*       modelTriggerLabel;
  QSpacerItem*  modelTriggerSpacer;
  QLabel*       modelTriggerValue;
  QHBoxLayout*  modelTriggerLayout;
  // Drugs
  QScalarQuantityWidget<MassPerVolumeUnit>* albuterolConcentration;
  const SESubstance* albuterol = nullptr;
  QScalarWidget* desfluraneVolumeFraction;
  const SESubstance* desflurane = nullptr;

  static const int PC = 0;
  static const int VC = 1;
  static const int CPAP = 2;

  struct ActionSet {
    SEMechanicalVentilatorPressureControl pc_ac;
    SEMechanicalVentilatorPressureControl pc_cmv;
    SEMechanicalVentilatorVolumeControl vc_ac;
    SEMechanicalVentilatorVolumeControl vc_cmv;
    SEMechanicalVentilatorContinuousPositiveAirwayPressure cpap;
  };

  ActionSet initialValues;
  ActionSet currentValues;

  // These are used when loading a state and updating the UI at steady state
  const SEMechanicalVentilatorMode* stateMode = nullptr;
  const SEMechanicalVentilatorHold* stateHold = nullptr;
  SEMechanicalVentilatorSettings* stateSettings = nullptr;
  // For passing setting updates over to the engine as we run
  std::vector<std::unique_ptr<SEAction>> unique_ptr_actions;
  // More detailed configuration properties to append to the selected mode

  int modeFromAction = -1;

  QMutex actionMutex;

  void Reset()
  {
    for (auto number : numbers)
    {
      number->setValue(0);
    }

    modeBox->setCurrentIndex(0);

    for (auto dial : dials)
    {
      dial->hide();
    }

    plot_0->clear();
    plot_0->setYRange(-0.1, 20.);
    min_0->setText("0");
    max_0->setText("20");
    plot_0->setMaxPoints(50 * 15);    // 15 Seconds
    plot_0->setScaleMode(PulsePainterPlot::ScaleMode::Expanding);
    plot_0->setUpdateMode(PulsePainterPlot::UpdateMode::InPlace);
    plot_1->clear();
    plot_1->setYRange(-30, 30);
    min_1->setText("-30");
    max_1->setText("30");
    plot_1->setMaxPoints(50 * 15);    // 15 Seconds
    plot_1->setScaleMode(PulsePainterPlot::ScaleMode::Expanding);
    plot_1->setUpdateMode(PulsePainterPlot::UpdateMode::InPlace);
    plot_2->clear();
    plot_2->setYRange(-100.0, 500);
    plot_2->setMaxPoints(50 * 15);    // 15 Seconds
    plot_2->setScaleMode(PulsePainterPlot::ScaleMode::Expanding);
    plot_2->setUpdateMode(PulsePainterPlot::UpdateMode::InPlace);
    min_2->setText("-100");
    max_2->setText("500");

    currentValues.pc_ac.Copy(initialValues.pc_ac, *subMgr);
    currentValues.pc_cmv.Copy(initialValues.pc_cmv, *subMgr);
    currentValues.vc_ac.Copy(initialValues.vc_ac, *subMgr);
    currentValues.vc_cmv.Copy(initialValues.vc_cmv, *subMgr);
    currentValues.cpap.Copy(initialValues.cpap, *subMgr);

    EnableTriggers(false);
    EnableDrugs(false);
  }

  void ResetDials()
  {
    SetMode(modeBox->currentText(), true);
  }

  void AddAction(std::unique_ptr<SEAction> p)
  {
    QMutexLocker lock(&actionMutex);
    unique_ptr_actions.push_back(std::move(p));
  }

  void ApplyActions(PhysiologyEngine& pulse)
  {
    QMutexLocker lock(&actionMutex);
    for (auto& action : unique_ptr_actions)
    {
      pulse.ProcessAction(*action);
    }
    unique_ptr_actions.clear();
  }

  void SetupInitialValues()
  { // Try to keep this in sync with whats in the Pulse HowTo_MechanicalVentilator.cpp
    {
      auto& pc_cmv = initialValues.pc_cmv;
      pc_cmv.SetConnection(eSwitch::On);
      pc_cmv.GetFractionInspiredOxygen().SetValue(.21);
      pc_cmv.GetInspiratoryPressure().SetValue(23.0, PressureUnit::cmH2O);
      pc_cmv.GetInspiratoryPeriod().SetValue(1.1, TimeUnit::s);
      pc_cmv.GetRespirationRate().SetValue(12, FrequencyUnit::Per_min);
      pc_cmv.GetPositiveEndExpiratoryPressure().SetValue(5.0, PressureUnit::cmH2O);
      pc_cmv.GetSlope().SetValue(0.2, TimeUnit::s);
      auto& pc_ac = initialValues.pc_ac;
      pc_ac.Copy(pc_cmv, *subMgr);
      pc_ac.SetMode(eMechanicalVentilator_PressureControlMode::AssistedControl);
      pc_cmv.SetMode(eMechanicalVentilator_PressureControlMode::ContinuousMandatoryVentilation);
    }
    {
      auto& vc_cmv = initialValues.vc_cmv;
      vc_cmv.SetConnection(eSwitch::On);
      vc_cmv.GetFractionInspiredOxygen().SetValue(0.21);
      vc_cmv.GetTidalVolume().SetValue(540, VolumeUnit::mL);
      vc_cmv.GetInspiratoryPeriod().SetValue(1.0, TimeUnit::s);
      vc_cmv.GetRespirationRate().SetValue(12, FrequencyUnit::Per_min);
      vc_cmv.GetPositiveEndExpiratoryPressure().SetValue(5.0, PressureUnit::cmH2O);
      vc_cmv.GetFlow().SetValue(60, VolumePerTimeUnit::L_Per_min); auto& pc_ac = initialValues.pc_ac;
      auto& vc_ac = initialValues.vc_ac;
      vc_ac.Copy(vc_cmv, *subMgr);
      vc_ac.SetMode(eMechanicalVentilator_VolumeControlMode::AssistedControl);
      vc_cmv.SetMode(eMechanicalVentilator_VolumeControlMode::ContinuousMandatoryVentilation);
    }
    {
      auto& cpap = initialValues.cpap;
      cpap.SetConnection(eSwitch::On);
      cpap.GetFractionInspiredOxygen().SetValue(0.21);
      cpap.GetPositiveEndExpiratoryPressure().SetValue(5.0, PressureUnit::cmH2O);
      cpap.GetDeltaPressureSupport().SetValue(10, PressureUnit::cmH2O);
      cpap.GetSlope().SetValue(0.2, TimeUnit::s);
    }
  }

  enum UIState {
    Default,
    Off,                // Engine is off
    VentilatorNone,     // Panel State for "None"
    Enabled,
    Disconnected,
    Connected,
    Hold
  };

  void ClearSlope()
  {
    if (tiWidget == nullptr) return;
    disconnect(tiWidget, &LabeledDialWidget::valueChanged, parent, &QVentilatorWidget::UpdateSlope);
    tiWidget = nullptr;
    slopeWidget = nullptr;
  }

  // The range of the slope is dependent on another widgets range, set up and connect the two
  void SetupSlope(LabeledDialWidget* ti, LabeledDialWidget* slope)
  {
    ClearSlope();
    slopeWidget = slope;
    tiWidget = ti;
    connect(tiWidget, &LabeledDialWidget::valueChanged, parent, &QVentilatorWidget::UpdateSlope);
  }

  // maxIndex the index of the last dial to show, dials with index higher will be hidden in the UI
  void ShowDials(int maxIndex)
  {
    for (int i = 0; i < maxIndex && i < dials.size(); ++i)
    {
      dials[i]->show();
    }
  }

  void SetMode(QString mode, bool resetValues = false)
  {
    ClearSlope();

    for (auto dial : dials)
    {
      dial->hide();
    }

    if (mode == "NONE")
    {
      SetUIState(VentilatorNone);
      return;
    }
    else if (mode == "PC-CMV" || mode == "PC-AC")
    {
      SEMechanicalVentilatorPressureControl* initial;
      SEMechanicalVentilatorPressureControl* current;
      if (mode == "PC-AC")
      {
        EnableTriggers(true);
        initial = &initialValues.pc_ac;
        current = &currentValues.pc_ac;
      }
      else
      {
        EnableTriggers(false);
        initial = &initialValues.pc_cmv;
        current = &currentValues.pc_cmv;
      }
      if (resetValues)
        current->Copy(*initial, *subMgr);
      
      dial_0->setup("FiO2", "",0.21, 1.0, 100, 0.01, &current->GetFractionInspiredOxygen());
      dial_1->setup("Pinsp", "cmH2O", 1, 100, 1, 1, &current->GetInspiratoryPressure());
      dial_2->setup("Ti", "s", 0.1, 60, 10, 0.1, &current->GetInspiratoryPeriod());
      dial_3->setup("RR", "bpm", 10, 60, 1, 1.0, &current->GetRespirationRate());
      dial_4->setup("PEEP", "cmH2O", 0, 50, 1, 1.0, &current->GetPositiveEndExpiratoryPressure());
      dial_5->setup("Slope", "s", 0, dial_2->getValue(), 10, 0.1, &current->GetSlope());
      SetupSlope(dial_2, dial_5);
      ShowDials(6);
      if (mode == "PC-AC")
      {
        // Update Trigger
        if (current->HasInspirationPatientTriggerFlow())
        {
          EnableFlowTrigger();
          inspirationPatientTriggerFlow->SetValue(current->GetInspirationPatientTriggerFlow());
        }
        else if (current->HasInspirationPatientTriggerPressure())
        {
          EnablePressureTrigger();
          inspirationPatientTriggerPressure->SetValue(current->GetInspirationPatientTriggerPressure());
        }
        else
        {
          EnableModelTrigger();
        }
      }
    }
    else if (mode == "VC-CMV" || mode == "VC-AC")
    {
      SEMechanicalVentilatorVolumeControl* initial;
      SEMechanicalVentilatorVolumeControl* current;
      if (mode == "VC-AC")
      {
        EnableTriggers(true);
        initial = &initialValues.vc_ac;
        current = &currentValues.vc_ac;
      }
      else
      {
        EnableTriggers(false);
        initial = &initialValues.vc_cmv;
        current = &currentValues.vc_cmv;
      }
      if (resetValues)
        current->Copy(*initial, *subMgr);
      dial_0->setup("FiO2", "", 0.21, 1.0, 100, 0.01, &current->GetFractionInspiredOxygen());
      dial_1->setup("VT", "mL", 100, 2000, 1, 10.0, &current->GetTidalVolume());
      dial_2->setup("Ti", "s", 0.1, 60, 10, 0.1, &current->GetInspiratoryPeriod());
      dial_3->setup("RR", "bpm", 10, 60, 1, 1.0, &current->GetRespirationRate());
      dial_4->setup("PEEP", "cmH2O", 0, 50, 1, 1.0, &current->GetPositiveEndExpiratoryPressure());
      dial_5->setup("Flow", "L/min", 1, 100, 1, 1.0, &current->GetFlow());
      ShowDials(6);
      if (mode == "VC-AC")
      {
        // Update Trigger
        if (current->HasInspirationPatientTriggerFlow())
        {
          EnableFlowTrigger();
          inspirationPatientTriggerFlow->SetValue(current->GetInspirationPatientTriggerFlow());
        }
        else if (current->HasInspirationPatientTriggerPressure())
        {
          EnablePressureTrigger();
          inspirationPatientTriggerPressure->SetValue(current->GetInspirationPatientTriggerPressure());
        }
        else
        {
          EnableModelTrigger();
        }
      }
    }
    else if (mode == "CPAP")
    {
      EnableTriggers(true);
      if (resetValues) currentValues.cpap.Copy(initialValues.cpap, *subMgr);
      auto& cpap = currentValues.cpap;
      dial_0->setup("FiO2", "", 0.21, 1.0, 100, 0.01, &cpap.GetFractionInspiredOxygen());
      dial_1->setup("PEEP", "cmH2O", 0, 50, 1, 1.0, &cpap.GetPositiveEndExpiratoryPressure());
      dial_2->setup("deltaPsupp", "cmH2O", 1, 100, 1, 1.0, &cpap.GetDeltaPressureSupport());
      dial_3->setup("Slope", "s", 0, 1, 100, 0.1, &cpap.GetSlope());
      ShowDials(4);
      // Update Trigger
      if (cpap.HasInspirationPatientTriggerFlow())
      {
        EnableFlowTrigger();
        inspirationPatientTriggerFlow->SetValue(cpap.GetInspirationPatientTriggerFlow());
      }
      else if (cpap.HasInspirationPatientTriggerPressure())
      {
        EnablePressureTrigger();
        inspirationPatientTriggerPressure->SetValue(cpap.GetInspirationPatientTriggerPressure());
      }
      else
      {
        EnableModelTrigger();
      }
    }
    else {
      qWarning() << "Mode " << mode << " not found.";
    }

    applyButton->setEnabled(true);
    SetUIState((applyButton->text() == "Connect") ? Disconnected : Connected);
  }

  void SetUIState(UIState state)
  {
    switch (state)
    {
    case Default:
      Reset();
      SetUIState(Off);
      applyButton->setEnabled(false);
      break;
    case Enabled:
      modeBox->setEnabled(true);
      resetButton->setEnabled(true);
      SetUIState((applyButton->text() == "Connect") ? Disconnected : Connected);
      break;
    case Off:
    case VentilatorNone:
      if (state == Off)
      {
        modeBox->setEnabled(false);
        applyButton->setEnabled(false);
      }
      else if (state == VentilatorNone)
      {
        modeBox->setEnabled(true);
      }
      resetButton->setEnabled(true);
      holdButton->setEnabled(false);
      disconnectButton->setEnabled(false);
      resetButton->setEnabled(false);
      hysteresisButton->setEnabled(false);
      // Triggers
      EnableTriggers(false);
      // Drugs
      EnableDrugs(false);
      break;
    case Connected:
    case Disconnected:
      if (state == Connected)
      {
        applyButton->setText("Apply");
        disconnectButton->setEnabled(true);
        EnableDrugs(true);
      }
      else if (state == Disconnected)
      {
        applyButton->setText("Connect");
        disconnectButton->setEnabled(false);
        EnableDrugs(false);
      }
      resetButton->setEnabled(true);
      holdButton->setEnabled(true);
      hysteresisButton->setEnabled(true);
      break;
    }
  }

  void EnableTriggers(bool b)
  {
    if (b && modelTrigger->isEnabled())
      return; // We are already enabled
    if (!b && !modelTrigger->isEnabled())
      return // We are already disabled

    inspirationPatientTriggerFlow->Reset();
    inspirationPatientTriggerPressure->Reset();
    inspirationPatientTriggerFlow->EnableInput(false, false);
    inspirationPatientTriggerPressure->EnableInput(false, false);

    if (!b)
    {
      inspirationPatientTriggerFlow->FullDisable();
      inspirationPatientTriggerPressure->FullDisable();
      modelTrigger->setEnabled(false);
      apply_triggers->setEnabled(false);
      modelRadio->setChecked(false);
      modelTriggerValue->setText("N/A");
    }
    else
    {
      inspirationPatientTriggerFlow->FullEnable();
      inspirationPatientTriggerFlow->EnableInput(false);
      inspirationPatientTriggerPressure->FullEnable();
      inspirationPatientTriggerPressure->EnableInput(false);
      modelTrigger->setEnabled(true);
      modelRadio->setChecked(true);
      apply_triggers->setEnabled(true);
      modelTriggerValue->setText("On");
    }
  }

  void EnableFlowTrigger()
  {
    inspirationPatientTriggerFlow->EnableInput(true);
    inspirationPatientTriggerPressure->EnableInput(false);
    modelRadio->setChecked(false);
    modelTriggerValue->setEnabled(false);

  }
  void EnablePressureTrigger()
  {
    inspirationPatientTriggerFlow->EnableInput(false);
    inspirationPatientTriggerPressure->EnableInput(true);
    modelRadio->setChecked(false);
    modelTriggerValue->setEnabled(false);
  }
  void EnableModelTrigger()
  {
    inspirationPatientTriggerFlow->EnableInput(false);
    inspirationPatientTriggerPressure->EnableInput(false);
    modelRadio->setChecked(true);
    modelTriggerValue->setEnabled(true);
  }

  void PullTrigger(SEScalarVolumePerTime& flowTrigger, SEScalarPressure& pressureTrigger)
  {
    // Pull from Triggers Tab
    if (apply_triggers->isEnabled())
    {
      if (inspirationPatientTriggerFlow->IsChecked())
      {
        pressureTrigger.Invalidate();
        inspirationPatientTriggerFlow->GetValue(flowTrigger);
        if (flowTrigger.IsZero())
          flowTrigger.Invalidate();
        if (!flowTrigger.IsValid())
        {
          // Default back to model trigger
          inspirationPatientTriggerFlow->EnableInput(true, false);
          EnableModelTrigger();
        }
      }
      else if (inspirationPatientTriggerPressure->IsChecked())
      {
        flowTrigger.Invalidate();
        inspirationPatientTriggerPressure->GetValue(pressureTrigger);
        if (pressureTrigger.IsZero())
          pressureTrigger.Invalidate();
        if (!pressureTrigger.IsValid())
        {
          // Default back to model trigger
          inspirationPatientTriggerPressure->EnableInput(true, false);
          EnableModelTrigger();
        }
      }
      else
      {
        flowTrigger.Invalidate();
        pressureTrigger.Invalidate();
        EnableModelTrigger();
      }
    }
  }

  void EnableDrugs(bool b)
  {
    albuterolConcentration->setEnabled(b);
    desfluraneVolumeFraction->setEnabled(b);
    apply_drugs->setEnabled(b);
  }
};

QVentilatorWidget::QVentilatorWidget(QWidget *parent, Qt::WindowFlags flags) :
  QWidget(parent, flags),
  d(new Private(this))
{
  d->dials = {
    d->dial_0,
    d->dial_1,
    d->dial_2,
    d->dial_3,
    d->dial_4,
    d->dial_5
  };

  d->numbers = {
    d->number_0, d->number_1, d->number_2, d->number_3, d->number_4,
    d->number_5, d->number_6, d->number_7
  };

  SetupNumbers();

  // Mode Controls
  connect(d->modeBox, &QComboBox::currentTextChanged, this, &QVentilatorWidget::SetMode);
  connect(d->applyButton, &QPushButton::clicked, this, &QVentilatorWidget::ApplyAction);
  connect(d->disconnectButton, &QPushButton::clicked, this, &QVentilatorWidget::DisconnectAction);
  connect(d->holdButton, &QPushButton::pressed, this, &QVentilatorWidget::AddHold);
  connect(d->holdButton, &QPushButton::released, this, &QVentilatorWidget::RemoveHold);
  connect(d->resetButton, &QPushButton::released, this, &QVentilatorWidget::ResetDials);
  // Triggers
  d->inspirationPatientTriggerFlow = new QScalarQuantityWidget<VolumePerTimeUnit>("Flow", 0, 10000, 1, VolumePerTimeUnit::L_Per_min, ScalarOptionWidget::Radio, this);
  d->inspirationPatientTriggerFlow->AddUnit(VolumePerTimeUnit::L_Per_s);
  d->inspirationPatientTriggerFlow->AddUnit(VolumePerTimeUnit::mL_Per_s);
  d->inspirationPatientTriggerFlow->AddUnit(VolumePerTimeUnit::mL_Per_min);
  d->inspirationPatientTriggerFlow->SetDefault(2);
  d->inspirationPatientTriggerFlow->EnableInput(false, false);
  d->trigger_layout->layout()->addWidget(d->inspirationPatientTriggerFlow);
  d->inspirationPatientTriggerPressure = new QScalarQuantityWidget<PressureUnit>("Pressure", -10000, 10000, 1, PressureUnit::cmH2O, ScalarOptionWidget::Radio, this);
  d->inspirationPatientTriggerPressure->AddUnit(PressureUnit::mmHg);
  d->inspirationPatientTriggerPressure->AddUnit(PressureUnit::Pa);
  d->inspirationPatientTriggerPressure->AddUnit(PressureUnit::psi);
  d->inspirationPatientTriggerPressure->SetDefault(1);
  d->inspirationPatientTriggerPressure->EnableInput(false, false);
  d->trigger_layout->layout()->addWidget(d->inspirationPatientTriggerPressure);
  d->modelTriggerLayout = new QHBoxLayout();
  d->modelTriggerLayout->setSpacing(6);
  d->modelTriggerLayout->setContentsMargins(-1, 3, -1, 3);
  QSizePolicy SsizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  SsizePolicy.setHorizontalStretch(0);
  SsizePolicy.setVerticalStretch(0);
  d->modelRadio = new QRadioButton(this);
  d->modelTriggerLayout->addWidget(d->modelRadio);
  d->modelTriggerLabel = new QLabel("Respiratory Model", this);
  SsizePolicy.setHeightForWidth(d->modelTriggerLabel->sizePolicy().hasHeightForWidth());
  d->modelTriggerLabel->setSizePolicy(SsizePolicy);
  d->modelTriggerLayout->addWidget(d->modelTriggerLabel);
  d->modelTriggerSpacer = new QSpacerItem(40, 30, QSizePolicy::Expanding, QSizePolicy::Maximum);
  d->modelTriggerLayout->addItem(d->modelTriggerSpacer);
  d->modelTriggerValue = new QLabel("On", this);
  d->modelTriggerLayout->addWidget(d->modelTriggerValue);
  d->modelTrigger = new QWidget(this);
  d->modelTrigger->setLayout(d->modelTriggerLayout);
  d->trigger_layout->layout()->addWidget(d->modelTrigger);
  connect(d->inspirationPatientTriggerFlow->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnableFlowTrigger()));
  connect(d->inspirationPatientTriggerPressure->GetRadioButton(), SIGNAL(clicked()), this, SLOT(EnablePressureTrigger()));
  connect(d->modelRadio, SIGNAL(clicked()), this, SLOT(EnableModelTrigger()));
  connect(d->apply_triggers, &QPushButton::clicked, this, &QVentilatorWidget::ApplyTriggers);
  // Drugs
  d->albuterolConcentration = new QScalarQuantityWidget<MassPerVolumeUnit>("Albuterol\nConcentration", 0, 10000, 1, MassPerVolumeUnit::g_Per_L, ScalarOptionWidget::Check, this);
  d->albuterolConcentration->AddUnit(MassPerVolumeUnit::g_Per_dL);
  d->albuterolConcentration->AddUnit(MassPerVolumeUnit::g_Per_mL);
  d->albuterolConcentration->AddUnit(MassPerVolumeUnit::mg_Per_L);
  d->albuterolConcentration->AddUnit(MassPerVolumeUnit::mg_Per_dL);
  d->albuterolConcentration->AddUnit(MassPerVolumeUnit::mg_Per_mL);
  d->albuterolConcentration->AddUnit(MassPerVolumeUnit::ug_Per_L);
  d->albuterolConcentration->AddUnit(MassPerVolumeUnit::ug_Per_mL);
  d->drugs_layout->layout()->addWidget(d->albuterolConcentration);
  d->desfluraneVolumeFraction = new QScalarWidget("Desflurane\nVolume Fraction", 0, 1, 0.05, ScalarOptionWidget::Check, this);
  d->drugs_layout->layout()->addWidget(d->desfluraneVolumeFraction);
  connect(d->apply_drugs, &QPushButton::clicked, this, &QVentilatorWidget::ApplyDrugs);

  Reset();
}

QVentilatorWidget::~QVentilatorWidget()
{
  delete d;
}

void QVentilatorWidget::Reset()
{
  d->SetUIState(QVentilatorWidget::Private::Default);
  d->EnableTriggers(false);
  d->EnableDrugs(false);
}

void QVentilatorWidget::AtSteadyState(PhysiologyEngine& pulse)
{
  // Called when we load a state too, so copy any data or actions
  d->subMgr = &pulse.GetSubstanceManager();
  d->albuterol = d->subMgr->GetSubstance("Albuterol");
  d->desflurane = d->subMgr->GetSubstance("Desflurane");

  if (pulse.GetActionManager().GetEquipmentActions().HasMechanicalVentilatorContinuousPositiveAirwayPressure())
  {
    d->stateMode = pulse.GetActionManager().GetEquipmentActions().GetMechanicalVentilatorContinuousPositiveAirwayPressure();
  }
  else if (pulse.GetActionManager().GetEquipmentActions().HasMechanicalVentilatorPressureControl())
  {
    d->stateMode = pulse.GetActionManager().GetEquipmentActions().GetMechanicalVentilatorPressureControl();
  }
  else if (pulse.GetActionManager().GetEquipmentActions().HasMechanicalVentilatorVolumeControl())
  {
    d->stateMode = pulse.GetActionManager().GetEquipmentActions().GetMechanicalVentilatorVolumeControl();
  }

  if (pulse.GetActionManager().GetEquipmentActions().HasMechanicalVentilatorHold())
  {
    d->stateHold = pulse.GetActionManager().GetEquipmentActions().GetMechanicalVentilatorHold();
    // TODO Log("Removing the hold from the loaded state");
  }

  d->stateSettings = const_cast<SEMechanicalVentilatorSettings*>(pulse.GetMechanicalVentilator()->GetSettings());

  d->hysteresis->AtSteadyState(pulse);

//    PulsePhysiology Name, Unit, UI Abbreviation
//    HS would like to set up things this way but that still has a few issues
//     auto& dm = pulse.GetEngineTracker()->GetDataRequestManager();
//    static std::vector<std::tuple<std::string, CCompoundUnit, QString>> items =
//    {
//      {"PeakInspiratoryPressure", PressureUnit::cmH2O, "PIP"},
//      {"MeanAirwayPressure", PressureUnit::cmH2O, "Pmean"},
//      {"RespirationRate", FrequencyUnit::Per_min, "RR"},
//      {"TidalVolume", VolumeUnit::mL, "VT"},
//      {"TotalPulmonaryVentilation", VolumePerTimeUnit::L_Per_min, "MVe"},
//      {"EndTidalCarbonDioxidePressure", PressureUnit::mmHg, "etCO2"},
//      {"PulmonaryCompliance", {}, "cDyn"},
//    };
//
//    int i = 0;
//    for (auto& item : items)
//    {
//      const std::string& physName = std::get<0>(item);
//      const CCompoundUnit& unit = std::get<1>(item);
//      const QString& label = std::get<2>(item);
//
//      if (physName.empty()) continue;
//
//      d->numbers[i]->setDataRequest(&dm.CreatePhysiologyDataRequest(physName, unit));
//
//      d->numbers[i]->setName(label);
//      d->numbers[i]->setUnit(QString(unit.GetString().c_str()));
//      ++i;
//    }

}

void QVentilatorWidget::AtSteadyStateUpdateUI()
{
  if (d->stateMode)
  {
    d->SetUIState(Private::Connected);
    UpdateModeUI();
  }
  if (d->stateHold)
  {// Just force the hold release, this would be a wierd edge case to support
    // The use can just enable the hold manually
    RemoveHold();
  }
  d->hysteresis->AtSteadyStateUpdateUI();
  if (d->stateSettings)
    UpdateSettingsUI();
}


void QVentilatorWidget::ProcessPhysiology(PhysiologyEngine& pulse)
{
  // This is called from a thread, you should NOT update UI here
  // This is where we pull data from pulse, and push any actions to it

  d->hysteresis->ProcessPhysiology(pulse);

  auto ventilator = pulse.GetMechanicalVentilator();

  d->number_0->setWithoutUpdate(ventilator->GetPeakInspiratoryPressure(PressureUnit::cmH2O));
  d->number_1->setWithoutUpdate(ventilator->GetTidalVolume(VolumeUnit::mL));
  d->number_2->setWithoutUpdate(ventilator->GetMeanAirwayPressure(PressureUnit::cmH2O));
  d->number_3->setWithoutUpdate(ventilator->GetTotalPulmonaryVentilation(VolumePerTimeUnit::L_Per_min));
  d->number_4->setWithoutUpdate(ventilator->GetRespirationRate(FrequencyUnit::Per_min));
  d->number_5->setWithoutUpdate(ventilator->GetEndTidalCarbonDioxidePressure(PressureUnit::mmHg));
  d->number_6->setWithoutUpdate(ventilator->GetStaticRespiratoryCompliance(VolumePerPressureUnit::mL_Per_cmH2O));
  d->number_7->setWithoutUpdate(ventilator->GetInspiratoryExpiratoryRatio());

  double time_s = pulse.GetSimulationTime(TimeUnit::s);
  d->plot_0->append(time_s, ventilator->GetAirwayPressure(PressureUnit::cmH2O));
  d->plot_1->append(time_s, ventilator->GetInspiratoryFlow(VolumePerTimeUnit::L_Per_min));
  d->plot_2->append(time_s, ventilator->GetTotalLungVolume(VolumeUnit::mL));

  d->ApplyActions(pulse);
}

void QVentilatorWidget::PhysiologyUpdateUI(const std::vector<SEAction const*>& actions)
{
  for (auto number : d->numbers)
  {
    number->updateValue();
  }
  // This is where we take the pulse data we pulled and push it to a UI widget
  for (const SEAction* a : actions)
  {
    if (auto mode = dynamic_cast<const SEMechanicalVentilatorMode*>(a))
    {
      d->stateMode = mode;
      UpdateModeUI();
    }
    else if (auto cfg = dynamic_cast<const SEMechanicalVentilatorConfiguration*>(a))
    {
      // TODO NO SUPPORT FOR FILE BASED SETTINGS YET
      d->stateSettings = const_cast<SEMechanicalVentilatorSettings*>(cfg->GetSettings());
      UpdateSettingsUI();
    }
  }
  d->hysteresis->PhysiologyUpdateUI(actions);

  // update plot axis labels
  {
    auto range = d->plot_0->getYRange();
    d->min_0->setText(QString::number(range.first, 'f', 0));
    d->max_0->setText(QString::number(range.second, 'f', 0));
  }
  {
    auto range = d->plot_1->getYRange();
    d->min_1->setText(QString::number(range.first, 'f', 0));
    d->max_1->setText(QString::number(range.second, 'f', 0));
  }
  {
    auto range = d->plot_2->getYRange();
    d->min_2->setText(QString::number(range.first, 'f', 0));
    d->max_2->setText(QString::number(range.second, 'f', 0));
  }
}

void QVentilatorWidget::UpdateModeUI()
{
  if (!d->stateMode)
    return;

  if (auto typedAction = dynamic_cast<const SEMechanicalVentilatorPressureControl*>(d->stateMode))
  {
    if (typedAction->GetMode() == eMechanicalVentilator_PressureControlMode::AssistedControl)
    {
      d->currentValues.pc_ac.Copy(*typedAction, *d->subMgr);
      d->modeBox->setCurrentText("PC-AC");// Triggers SetMode (which updates dial values)
    }
    else
    {
      d->currentValues.pc_cmv.Copy(*typedAction, *d->subMgr);
      d->modeBox->setCurrentText("PC-CMV");// Triggers SetMode (which updates dial values)
    }
  }
  else if (auto typedAction = dynamic_cast<const SEMechanicalVentilatorVolumeControl*>(d->stateMode))
  {
    if (typedAction->GetMode() == eMechanicalVentilator_VolumeControlMode::AssistedControl)
    {
      d->currentValues.vc_ac.Copy(*typedAction, *d->subMgr);
      d->modeBox->setCurrentText("VC-AC");// Triggers SetMode (which updates dial values)
    }
    else
    {
      d->currentValues.vc_cmv.Copy(*typedAction, *d->subMgr);
      d->modeBox->setCurrentText("VC-CMV");// Triggers SetMode (which updates dial values)
    }
  }
  else if (auto typedAction = dynamic_cast<const SEMechanicalVentilatorContinuousPositiveAirwayPressure*>(d->stateMode))
  {
    d->currentValues.cpap.Copy(*typedAction, *d->subMgr);
    d->modeBox->setCurrentText("CPAP");// Triggers SetMode (which updates dial values)
  }
  d->stateMode = nullptr;
}

void QVentilatorWidget::UpdateSettingsUI()
{
  std::string mode = d->modeBox->currentText().toStdString();

  if (mode == "NONE")
  {
    d->EnableDrugs(false);
    return;
  }

  // Update Drugs
  d->EnableDrugs(true);
  if (d->stateSettings->HasConcentrationInspiredAerosol(*d->albuterol))
  {
    SEScalarMassPerVolume& c = d->stateSettings->GetConcentrationInspiredAerosol(*d->albuterol).GetConcentration();
    if (c.IsZero())
    {
      d->albuterolConcentration->EnableInput(true, false);
    }
    else
    {
      d->albuterolConcentration->SetValue(c);
    }
  }
  else
  {
    d->albuterolConcentration->EnableInput(true, false);
  }

  if (d->stateSettings->HasFractionInspiredGas(*d->desflurane))
  {
    SEScalar0To1& a = d->stateSettings->GetFractionInspiredGas(*d->desflurane).GetFractionAmount();
    if (a.IsZero())
    {
      d->desfluraneVolumeFraction->EnableInput(true, false);
    }
    else
    {
      d->desfluraneVolumeFraction->SetValue(a);
    }
  }
  else
  {
    d->desfluraneVolumeFraction->EnableInput(true, false);
  }
  d->stateSettings = nullptr;
}



void QVentilatorWidget::EngineErrorUI()
{

}

void QVentilatorWidget::StartEngine()
{
  d->SetUIState(QVentilatorWidget::Private::VentilatorNone);
}

void QVentilatorWidget::UpdateSlope(double value)
{
  d->slopeWidget->setMaximumValue(value);
}

void QVentilatorWidget::ApplyAction()
{
  QString mode = d->modeBox->currentText();
  SEScalarTime t;
  t.Invalidate();

  if (mode == "NONE")
  {
    DisconnectAction();
    return;
  }
  else if (mode == "PC-CMV" || mode == "PC-AC")
  {
    auto action = std::make_unique<SEMechanicalVentilatorPressureControl>();
    if (mode == "PC-AC")
    {
      d->PullTrigger(d->currentValues.pc_ac.GetInspirationPatientTriggerFlow(),
                     d->currentValues.pc_ac.GetInspirationPatientTriggerPressure());
      PullSupplementalSettings(d->currentValues.pc_ac.GetSupplementalSettings());
      action->Copy(d->currentValues.pc_ac, *d->subMgr);
    }
    else
    {
      PullSupplementalSettings(d->currentValues.pc_cmv.GetSupplementalSettings());
      action->Copy(d->currentValues.pc_cmv, *d->subMgr);
    }
    emit(UpdateAction(*action, t));
    d->AddAction(std::move(action));
  }
  else if (mode == "VC-CMV" || mode == "VC-AC")
  {
    auto action = std::make_unique<SEMechanicalVentilatorVolumeControl>();
    
    // See SetMode for dial assignments
    if (mode == "VC-AC")
    {
      d->PullTrigger(d->currentValues.vc_ac.GetInspirationPatientTriggerFlow(),
                     d->currentValues.vc_ac.GetInspirationPatientTriggerPressure());
      PullSupplementalSettings(d->currentValues.vc_ac.GetSupplementalSettings());
      action->Copy(d->currentValues.vc_ac, *d->subMgr);
    }
    else
    {
      PullSupplementalSettings(d->currentValues.vc_cmv.GetSupplementalSettings());
      action->Copy(d->currentValues.vc_cmv, *d->subMgr);
    }
    emit(UpdateAction(*action, t));
    d->AddAction(std::move(action));
  }
  else if (mode == "CPAP")
  {
    auto action = std::make_unique<SEMechanicalVentilatorContinuousPositiveAirwayPressure>();
    d->PullTrigger(d->currentValues.cpap.GetInspirationPatientTriggerFlow(),
                   d->currentValues.cpap.GetInspirationPatientTriggerPressure());
    PullSupplementalSettings(d->currentValues.cpap.GetSupplementalSettings());
    action->Copy(d->currentValues.cpap, *d->subMgr);
    emit(UpdateAction(*action, t));
    d->AddAction(std::move(action));
  }
  else {
    qWarning() << "Mode " << mode << " not found.";
  }
  d->SetUIState(Private::Connected);
}

void QVentilatorWidget::DisconnectAction()
{
  auto action = std::make_unique<SEMechanicalVentilatorConfiguration>();
  action->GetSettings().SetConnection(eSwitch::Off);
  SEScalarTime t;
  t.Invalidate();
  emit(UpdateAction(*action, t));
  d->AddAction(std::move(action));

  d->SetUIState(Private::Disconnected);
}

void QVentilatorWidget::AddHold()
{
  /// If the hold button is enabled
  /// both of these other buttons are enabled as well, therefore
  /// we don't need to cache the state
  d->applyButton->setDisabled(true);
  d->disconnectButton->setDisabled(true);

  auto action = std::make_unique<SEMechanicalVentilatorHold>();
  action->SetState(eSwitch::On);
  d->AddAction(std::move(action));
}

void QVentilatorWidget::RemoveHold()
{
  d->applyButton->setDisabled(false);
  d->disconnectButton->setDisabled(false);

  auto action = std::make_unique<SEMechanicalVentilatorHold>();
  action->SetState(eSwitch::Off);
  d->AddAction(std::move(action));
  d->stateHold = nullptr;
}


void QVentilatorWidget::SetMode(QString val)
{
  d->SetMode(val);
}

void QVentilatorWidget::ResetDials()
{
  d->ResetDials();
}

void QVentilatorWidget::SetupNumbers()
{
  d->number_0->setup("PIP", "cmH20");
  d->number_1->setup("VT", "mL");
  d->number_2->setup("Pmean", "cmH20");
  d->number_3->setup("MVe", "L/min");
  d->number_4->setup("RR", "/min");
  d->number_5->setup("etCO2", "mmHg");
  d->number_6->setup("cStat", "mL/cmH2O");
  d->number_7->setup("I:E", "", 1, true);
}

void QVentilatorWidget::LockForHold()
{
  d->applyButton->setDisabled(true);
}

void QVentilatorWidget::Unlock()
{
  d->applyButton->setDisabled(false);
}

void QVentilatorWidget::EnableFlowTrigger()
{
  d->EnableFlowTrigger();
  
}
void QVentilatorWidget::EnablePressureTrigger()
{
  d->EnablePressureTrigger();
}
void QVentilatorWidget::EnableModelTrigger()
{
  d->EnableModelTrigger();
}

void QVentilatorWidget::ApplyTriggers()
{
  ApplyAction();
}

void QVentilatorWidget::ApplyDrugs()
{
  ApplyAction();
}

void QVentilatorWidget::PullSupplementalSettings(SEMechanicalVentilatorSettings& settings)
{
  settings.Clear();
  // Pull from Drugs Tab
  if (d->albuterolConcentration->IsChecked())
  {
    d->albuterolConcentration->GetValue(
      settings.GetConcentrationInspiredAerosol(*d->albuterol).GetConcentration()
    );
    if (!settings.GetConcentrationInspiredAerosol(*d->albuterol).GetConcentration().IsValid())
      settings.GetConcentrationInspiredAerosol(*d->albuterol).GetConcentration().SetValue(0, MassPerVolumeUnit::g_Per_L);
    if (settings.GetConcentrationInspiredAerosol(*d->albuterol).GetConcentration().IsZero())
    {
      d->albuterolConcentration->EnableInput(true, false);
    }
  }
  else
  {
    settings.GetConcentrationInspiredAerosol(*d->albuterol).GetConcentration().SetValue(0, MassPerVolumeUnit::g_Per_L);
  }

  if (d->desfluraneVolumeFraction->IsChecked())
  {
    d->desfluraneVolumeFraction->GetValue(
      settings.GetFractionInspiredGas(*d->desflurane).GetFractionAmount()
    );
    if (!settings.GetFractionInspiredGas(*d->desflurane).GetFractionAmount().IsValid())
      settings.GetFractionInspiredGas(*d->desflurane).GetFractionAmount().SetValue(0);
    if (settings.GetFractionInspiredGas(*d->desflurane).GetFractionAmount().IsZero())
    {
      d->desfluraneVolumeFraction->EnableInput(true, false);
    }
  }
  else
  {
    settings.GetFractionInspiredGas(*d->desflurane).GetFractionAmount().SetValue(0);
  }
}
