/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QWidget>
#include "QPulse.h"

namespace Ui {
  class VitalsMonitorWidget;
}

class QVitalsMonitorWidget : public QWidget, public PulseListener
{
  Q_OBJECT
public:
  QVitalsMonitorWidget(QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QVitalsMonitorWidget();

  void Reset();

  void ShowHeartRate(bool);
  void ShowBloodPressure(bool);
  void ShowSpO2(bool);
  void ShowETCO2(bool);
  void ShowRespirationRate(bool);
  void ShowTemperature(bool);
  void ShowECGWaveform(bool);
  void ShowPLETHWaveform(bool);
  void ShowETCO2Waveform(bool);

  void UseComputedTemperature();
  void OverrideTemperature_C(double t);

  void AtSteadyState(PhysiologyEngine& pulse) override {}
  void AtSteadyStateUpdateUI() override {} // Main Window will call this to update UI Components
  void ProcessPhysiology(PhysiologyEngine& pulse) override;
  void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override;// Main Window will call this to update UI Components
  void EngineErrorUI() override {};// Main Window will call this to update UI Components

//signals:
//protected slots:
protected:
  void ShowControl(bool b, QWidget* w, QWidget* baseColor);

private:
  class Controls;
  Controls* m_Controls;
};
