/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QDockWidget>
#include "controls/QPulse.h"
class SEDataRequest;

namespace Ui {
  class DataRequestWidget;
}

class QDataRequestWidget : public QDockWidget
{
  Q_OBJECT
public:
  QDataRequestWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QDataRequestWidget();

  SEDataRequest& GetDataRequest(SEDataRequestManager& drMgr);

signals:
protected slots:
  void UpdateSelectionBoxes();

private:
  class Controls;
  Controls* m_Controls;
};
