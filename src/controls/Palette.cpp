// Property of Kitware, Inc. and KbPort.
// PROPRIETARY: DO NOT DISTRIBUTE.

#include "Palette.h"

#include <QSettings>

// ----------------------------------------------------------------------------
Palette::Palette(WindowStyle w, ButtonColor b)
{
  load(QStringLiteral("base"));

  switch (w)
  {
    case Workspace:
      load(QStringLiteral("workspace"));
      break;

    case Sidebar:
    case SidebarNested:
      load(QStringLiteral("sidebar"));
      if (w == SidebarNested)
        load(QStringLiteral("sidebar-nested"));
      break;

    case Toolbar:
      load(QStringLiteral("toolbar"));
      break;
  }

  switch (b)
  {
    case Blue:
      load(QStringLiteral("button-blue"));
      break;

    case Green:
      load(QStringLiteral("button-green"));
      break;

    case Red:
      load(QStringLiteral("button-red"));
      break;

    default:
      break;
  }
}

// ----------------------------------------------------------------------------
Palette& Palette::load(Special s)
{
  switch (s)
  {
    case Error:
      return load(QStringLiteral("error"));
    case Throbber:
      return load(QStringLiteral("throbber"));
    default: // unknown
      return *this;
  }
}

// ----------------------------------------------------------------------------
Palette& Palette::load(QString const& name)
{
  load(QSettings{QStringLiteral(":/theme/%1.ini").arg(name),
                 QSettings::IniFormat});
  return *this;
}
