/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "ScenarioSaveDialog.h"
#include "ui_ScenarioSave.h"

#include <QFileDialog>

#include "QPulse.h"

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/utils/FileUtils.h"

class ScenarioSaveDialog::Controls : public Ui::ScenarioSave
{
public:
  Controls() {}

  ~Controls() {}

  Mode    mode;
  QString ScenarioFile;
  QString SeparateFile;
};

ScenarioSaveDialog::ScenarioSaveDialog(Mode m,
                                       std::string const& scenarioFilePath,
                                       std::string const& separateFilePath,
                                       QWidget *parent) : QDialog(parent)
{
  m_Controls = new Controls();
  m_Controls->setupUi(this);

  setWindowTitle("Save Scenario File(s)");

  m_Controls->mode = m;
  m_Controls->ScenarioFile = QString::fromStdString(scenarioFilePath);
  m_Controls->SeparateFile = QString::fromStdString(separateFilePath);

  m_Controls->ScenarioFilePath->setText(m_Controls->ScenarioFile);
  m_Controls->SeparateFilePath->setText(m_Controls->SeparateFile);

  if (m == Mode::Patient)
  {
    m_Controls->Separate->setText("Separate patient to");
    m_Controls->BrowseSeparateFile->setEnabled(false);
    m_Controls->SeparateFilePath->setEnabled(false);
  }
  else
  {
    m_Controls->Separate->setDisabled(true);// Cannot embed state yet...
    m_Controls->Separate->setText("Embed state into scenario");
    m_Controls->BrowseSeparateFile->hide();
    m_Controls->SeparateFilePath->hide();
  }

  connect(m_Controls->BrowseScenarioFile, SIGNAL(clicked()), this, SLOT(BrowseForScenarioSaveFile()));
  connect(m_Controls->Separate, SIGNAL(clicked()), this, SLOT(EnableSeparateFile()));
  connect(m_Controls->BrowseSeparateFile, SIGNAL(clicked()), this, SLOT(BrowseForSeparateSaveFile()));
}

ScenarioSaveDialog::~ScenarioSaveDialog()
{
  delete m_Controls;
}

QString ScenarioSaveDialog::GetScenarioFilename()
{
  return m_Controls->ScenarioFilePath->text();
}

void ScenarioSaveDialog::BrowseForScenarioSaveFile()
{
  m_Controls->ScenarioFile = QPulse::AttemptRelativePath(QFileDialog::getSaveFileName(this, "Save Scenario", QPulse::GetSaveScenariosDir(), "JSON (*.json);;Protobuf Binary  (*.pbb)"));
  m_Controls->ScenarioFilePath->setText(m_Controls->ScenarioFile);
}

bool ScenarioSaveDialog::SeparateScenario()
{
  return m_Controls->Separate->isChecked();
}

QString ScenarioSaveDialog::GetSeparateFilename()
{
  return m_Controls->SeparateFilePath->text();
}

void ScenarioSaveDialog::BrowseForSeparateSaveFile()
{
  QString save_type;
  QString inital_dir;
  if (m_Controls->mode == Mode::Patient)
  {
    save_type = "Save Patient";
    inital_dir = QPulse::GetSavePatientsDir();
  }
  else
  {
    save_type = "Save State";
    inital_dir = QPulse::GetSaveStatesDir();
  }
  m_Controls->SeparateFile = QPulse::AttemptRelativePath(QFileDialog::getSaveFileName(this, save_type, inital_dir, "JSON (*.json);;Protobuf Binary  (*.pbb)"));
}

void ScenarioSaveDialog::EnableSeparateFile()
{
  bool b = m_Controls->Separate->isEnabled();
  m_Controls->BrowseSeparateFile->setEnabled(b);
  m_Controls->SeparateFilePath->setEnabled(b);
}
