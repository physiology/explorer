/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#include "VitalsMonitorWidget.h"
#include "ui_VitalsMonitor.h"
#include "QwtPulsePlot.h"

#include <QLayout>
#include <QGraphicsLayout>

#include "pulse/engine/PulseEngine.h"
#include "pulse/cdm/substance/SESubstanceManager.h"
#include "pulse/cdm/compartment/SECompartmentManager.h"
#include "pulse/cdm/compartment/fluid/SEGasCompartment.h"
#include "pulse/cdm/system/physiology/SEBloodChemistrySystem.h"
#include "pulse/cdm/system/physiology/SECardiovascularSystem.h"
#include "pulse/cdm/system/physiology/SERespiratorySystem.h"
#include "pulse/cdm/system/physiology/SEEnergySystem.h"
#include "pulse/cdm/system/equipment/electrocardiogram/SEElectroCardioGram.h"
#include "pulse/cdm/properties/SEScalarFrequency.h"
#include "pulse/cdm/properties/SEScalarTime.h"

class QVitalsMonitorWidget::Controls : public Ui::VitalsMonitorWidget
{
public:
  Controls() {}
  double                        HeartRate_bpm;
  double                        ECG_III_mV;
  double                        ArterialPressure_mmHg;
  double                        MeanArterialPressure_mmHg;
  double                        DiastolicPressure_mmHg;
  double                        SystolicPressure_mmHg;
  double                        OxygenSaturation;
  double                        EndTidalCarbonDioxidePressure_mmHg;
  double                        RespirationRate_bpm;
  bool                          OverrideTemperature = false;
  double                        Temperature_C;
  const SEGasSubstanceQuantity* CarinaCO2=nullptr;
};

QVitalsMonitorWidget::QVitalsMonitorWidget(QWidget *parent, Qt::WindowFlags flags) : QWidget(parent,flags)
{
  m_Controls = new Controls();
  m_Controls->setupUi(this);

  m_Controls->ECGDisplayPlot->setMaxPoints(250);
  m_Controls->ECGDisplayPlot->setYRange(-0.1, 0.9);

  m_Controls->PLETHDisplayPlot->setMaxPoints(250);
  m_Controls->PLETHDisplayPlot->setYRange(50, 135);

  m_Controls->etCO2DisplayPlot->setMaxPoints(750);
  m_Controls->etCO2DisplayPlot->setYRange(0.2, 30);

  Reset();
}

QVitalsMonitorWidget::~QVitalsMonitorWidget()
{
  delete m_Controls;
}

void QVitalsMonitorWidget::Reset()
{
  ShowHeartRate(false);
  ShowBloodPressure(false);
  ShowSpO2(false);
  ShowETCO2(false);
  ShowRespirationRate(false);
  ShowTemperature(false);

  ShowECGWaveform(false);
  ShowPLETHWaveform(false);
  ShowETCO2Waveform(false);

  m_Controls->HeartRateValue->setText("0");
  m_Controls->BloodPressureValues->setText("000/000");
  m_Controls->MeanBloodPressureValue->setText("(0)");
  m_Controls->SpO2Value->setText("0");
  m_Controls->etCO2Value->setText("0");
  m_Controls->RespiratoryRateValue->setText("0");
  m_Controls->TempeartureValue->setText("0");

  m_Controls->ECGDisplayPlot->clear();
  m_Controls->PLETHDisplayPlot->clear();
  m_Controls->etCO2DisplayPlot->clear();

  m_Controls->CarinaCO2 = nullptr;
}

void QVitalsMonitorWidget::ShowControl(bool b, QWidget* w, QWidget* baseColor)
{
  QPalette palette = w->palette();
  const QColor& color = b ? baseColor->palette().color(baseColor->foregroundRole()) : Qt::black;
  palette.setColor(w->foregroundRole(), color);
  w->setPalette(palette);
}

void QVitalsMonitorWidget::ShowHeartRate(bool b)
{
  ShowControl(b, m_Controls->HeartRateValue, m_Controls->HeartRateUnit);
}
void QVitalsMonitorWidget::ShowBloodPressure(bool b)
{
  ShowControl(b, m_Controls->BloodPressureValues, m_Controls->BloodPressureUnit);
  ShowControl(b, m_Controls->MeanBloodPressureValue, m_Controls->BloodPressureUnit);
}
void QVitalsMonitorWidget::ShowSpO2(bool b)
{
  ShowControl(b, m_Controls->SpO2Value, m_Controls->SpO2Unit);
}
void QVitalsMonitorWidget::ShowETCO2(bool b)
{
  ShowControl(b, m_Controls->etCO2Value, m_Controls->etCO2Unit);
}
void QVitalsMonitorWidget::ShowRespirationRate(bool b)
{
  ShowControl(b, m_Controls->RespiratoryRateValue, m_Controls->RespiratoryRateUnit);
}
void QVitalsMonitorWidget::ShowTemperature(bool b)
{
  ShowControl(b, m_Controls->TempeartureValue, m_Controls->TempeartureUnit);
}
void QVitalsMonitorWidget::ShowECGWaveform(bool b)
{
  m_Controls->ECGDisplayPlot->setVisible(b);
}
void QVitalsMonitorWidget::ShowPLETHWaveform(bool b)
{
  m_Controls->PLETHDisplayPlot->setVisible(b);
}
void QVitalsMonitorWidget::ShowETCO2Waveform(bool b)
{
  m_Controls->etCO2DisplayPlot->setVisible(b);
}

void QVitalsMonitorWidget::UseComputedTemperature()
{
  m_Controls->OverrideTemperature = false;
}
void QVitalsMonitorWidget::OverrideTemperature_C(double t)
{
  m_Controls->OverrideTemperature = true;
  m_Controls->Temperature_C = t;
}

void QVitalsMonitorWidget::ProcessPhysiology(PhysiologyEngine& pulse)
{// This is called from a thread, you should NOT update UI here
  // This is where we pull data from pulse, and push any actions to it

  m_Controls->HeartRate_bpm = pulse.GetCardiovascularSystem()->GetHeartRate(FrequencyUnit::Per_min);
  m_Controls->ECG_III_mV = pulse.GetElectroCardioGram()->GetLead3ElectricPotential(ElectricPotentialUnit::mV);
  m_Controls->ArterialPressure_mmHg = pulse.GetCardiovascularSystem()->GetArterialPressure(PressureUnit::mmHg);
  m_Controls->MeanArterialPressure_mmHg = pulse.GetCardiovascularSystem()->GetMeanArterialPressure(PressureUnit::mmHg);
  m_Controls->DiastolicPressure_mmHg = pulse.GetCardiovascularSystem()->GetDiastolicArterialPressure(PressureUnit::mmHg);
  m_Controls->SystolicPressure_mmHg = pulse.GetCardiovascularSystem()->GetSystolicArterialPressure(PressureUnit::mmHg);
  m_Controls->OxygenSaturation = pulse.GetBloodChemistrySystem()->GetPulseOximetry();
  m_Controls->RespirationRate_bpm = pulse.GetRespiratorySystem()->GetRespirationRate(FrequencyUnit::Per_min);
  m_Controls->EndTidalCarbonDioxidePressure_mmHg = pulse.GetRespiratorySystem()->GetEndTidalCarbonDioxidePressure(PressureUnit::mmHg);
  if(!m_Controls->OverrideTemperature)
    m_Controls->Temperature_C = pulse.GetEnergySystem()->GetCoreTemperature(TemperatureUnit::C);

  if (m_Controls->CarinaCO2 == nullptr)
  {
    const SESubstance* CO2 = pulse.GetSubstanceManager().GetSubstance("CarbonDioxide");
    m_Controls->CarinaCO2 = pulse.GetCompartments().GetGasCompartment(pulse::PulmonaryCompartment::Carina)->GetSubstanceQuantity(*CO2);
  }
  double time_s = pulse.GetSimulationTime(TimeUnit::s);
  m_Controls->ECGDisplayPlot->append(time_s, m_Controls->ECG_III_mV);
  m_Controls->PLETHDisplayPlot->append(time_s, m_Controls->ArterialPressure_mmHg);
  m_Controls->etCO2DisplayPlot->append(time_s, m_Controls->CarinaCO2->GetPartialPressure(PressureUnit::mmHg));
}

void QVitalsMonitorWidget::PhysiologyUpdateUI(const std::vector<SEAction const*>& actions)
{
  // This is called from a slot, you can update UI here
  // This is where we take the pulse data we pulled and push it to a UI widget
  // Cast to int truncates add 0.5 to do round the numbers
  
  m_Controls->HeartRateValue->setText(QString::number(int(m_Controls->HeartRate_bpm + 0.5),'d',0));
  auto bpText = QString::number(int(m_Controls->SystolicPressure_mmHg + 0.5), 'd', 0) + "/" + QString::number(int(m_Controls->DiastolicPressure_mmHg + 0.5), 'd', 0)
      + ((int(m_Controls->DiastolicPressure_mmHg + 0.5) < 100)? QString(" ") : QString());
      
  m_Controls->BloodPressureValues->setText(bpText);
  m_Controls->MeanBloodPressureValue->setText("("+QString::number(int(m_Controls->MeanArterialPressure_mmHg + 0.5), 'd', 0)+")");
  m_Controls->SpO2Value->setText(QString::number(int(m_Controls->OxygenSaturation*100 + 0.5), 'd', 0));
  m_Controls->etCO2Value->setText(QString::number(int(m_Controls->EndTidalCarbonDioxidePressure_mmHg + 0.5), 'd', 0));
  m_Controls->RespiratoryRateValue->setText(QString::number(int(m_Controls->RespirationRate_bpm + 0.5), 'd', 0));
  m_Controls->TempeartureValue->setText(QString::number(m_Controls->Temperature_C, 'd', 1));
}
