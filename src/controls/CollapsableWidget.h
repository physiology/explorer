/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QAction>
#include <QDrag>
#include <QMenu>
#include <QMimeData>
#include <QMouseEvent>
#include <QFrame>
#include <QStyleOption>
#include <QGridLayout>
#include <QParallelAnimationGroup>
#include <QPainter>
#include <QScrollArea>
#include <QToolButton>
#include <QWidget>
#include <QDebug>

class QCollapsableWidget : public QWidget
{
  Q_OBJECT
public:
  explicit QCollapsableWidget(const QString & title = "", const int animationDuration = 120, QWidget *parent = nullptr);
  void setContentLayout(QLayout & contentLayout);
  void expand(bool b);
  bool expanded();
  void paintEvent(QPaintEvent*);
  int  getContentHeight() { return contentHeight; }

protected:
  QGridLayout mainLayout;
  QToolButton toggleButton;
  QFrame headerLine;
  QParallelAnimationGroup toggleAnimation;
  QScrollArea contentArea;
  int animationDuration{ 300 };
  int contentHeight;
};