/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QDockWidget>

#include "QPulse.h"

#include "pulse/engine/PulseEngine.h"
#include "pulse/engine/PulseScenario.h"
#include "pulse/cdm/substance/SESubstanceManager.h"

class SECondition;
class SEConditionManager;

namespace Ui {
  class ConditionsEditorWidget;
}

class QConditionsEditorWidget : public QDockWidget, public PulseListener
{
  Q_OBJECT
public:
  QConditionsEditorWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QConditionsEditorWidget();

  void Clear();
  void EnableInput(bool b);
  void ConditionsToControls(SEConditionManager const& mgr);
  void ControlsToConditions(SEConditionManager& mgr, SESubstanceManager& subMgr);

  void AtSteadyState(PhysiologyEngine& pulse) override;
  void AtSteadyStateUpdateUI() override; // Main Window will call this to update UI Components
  void ProcessPhysiology(PhysiologyEngine& pulse) override {}
  void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override {} // Main Window will call this to update UI Components
  void EngineErrorUI() override {};// Main Window will call this to update UI Components

public slots:
  void ClearConditions();

protected:
  void EnableConverter(bool b);

private:
  class Controls;
  Controls* m_Controls;
  
};