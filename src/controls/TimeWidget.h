/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QSpacerItem>
#include <QSpinBox>

#include "pulse/cdm/CommonDefs.h"
#include "pulse/cdm/properties/SEScalarTime.h"


namespace Ui {
  class TimeWidget;
}

class QTimeWidget : public QWidget
{
  Q_OBJECT
public:
  QTimeWidget(QWidget* parent = nullptr, QString label = "", int maxWidth = 70);

  void Reset();
  void SetEnabled(bool b);

  void SetTime(SEScalarTime const& t);
  void GetTime(SEScalarTime& t);

protected slots:
  void PrefixCheck();

private:
  class Controls;
  Controls* m_Controls;
};
