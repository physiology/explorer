// Property of Kitware, Inc. and KbPort.
// PROPRIETARY: DO NOT DISTRIBUTE.

#include "Icon.h"

#include <QVariant>
#include <QWidget>

// ----------------------------------------------------------------------------
QChar pulseIcon(QWidget const* widget)
{
  return widget->property("pulse-icon").toChar();
}

// ----------------------------------------------------------------------------
void setPulseIcon(QChar icon, QWidget* widget)
{
  widget->setProperty("pulse-icon", icon);
}
