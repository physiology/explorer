// Property of Kitware, Inc. and KbPort.
// PROPRIETARY: DO NOT DISTRIBUTE.

#pragma once

#include <qtColorScheme.h>

// ----------------------------------------------------------------------------
class Palette : public qtColorScheme
{
public:
  enum WindowStyle
  {
    Workspace,
    Sidebar,
    SidebarNested,
    Toolbar,
  };

  enum ButtonColor
  {
    Blue,
    Green,
    Red,
    Default,
  };

  enum Special
  {
    Error,
    Throbber,
  };

  Palette(WindowStyle, ButtonColor = Default);

  Palette& load(QString const&);
  Palette& load(Special);
  using qtColorScheme::load;
};
