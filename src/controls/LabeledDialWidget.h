/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/

#pragma once

#include <QWidget>
#include <memory>

class SEScalar;

class LabeledDialWidget : public QWidget
{
  Q_OBJECT

public:
  explicit LabeledDialWidget(QWidget* parent = nullptr);
  virtual ~LabeledDialWidget();

  void setTitle(QString title);
  void setUnit(QString unit);

  /// Use the scalar for backing the value
  void setScalar(SEScalar* scalar);

  void setup(QString title, QString unit, double min, double max, double scale = 1.0, double step = 1.0, SEScalar* scalar = nullptr);

  void setMinimumValue(double val);
  void setMaximumValue(double val);

  void setValue(double val);
  double getValue() const;

signals:
  void valueChanged(double val);

private:
  void spinBoxUpdated(int val);

  struct Private;
  Private* d;
};
