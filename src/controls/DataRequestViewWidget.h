/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include "QPulse.h"
#include "PlotSetWidget.h"

#include "pulse/cdm/engine/SEDataRequest.h"
#include "pulse/cdm/scenario/SEScenario.h"

namespace Ui {
  class DataRequestViewWidget;
}

class QDataRequestViewWidget : public QDockWidget, public PulseListener
{
  Q_OBJECT
public:
  QDataRequestViewWidget(QPulse& qp, QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
  virtual ~QDataRequestViewWidget();

  void Clear();
  void Reset();

  void EnableControls(bool b);
  void LoadScenario(SEScenario& scenario);
  void SaveScenario(SEScenario& scenario);
  void CreatePlot(SEDataRequest& dr);

  QPlotSetWidget *PlotWidget;
  void AtSteadyState(PhysiologyEngine& pulse) override;
  void AtSteadyStateUpdateUI() override;// Main Window will call this to update UI Components
  void ProcessPhysiology(PhysiologyEngine& pulse) override;
  void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override;// Main Window will call this to update UI Components
  void EngineErrorUI() override {};// Main Window will call this to update UI Components

signals:
protected slots:
  void Tile();
  void NewPlotSet();

private:
  class Controls;
  Controls* m_Controls;
};
