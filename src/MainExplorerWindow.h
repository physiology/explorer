/* Distributed under the Apache License, Version 2.0.
See accompanying NOTICE file for details.*/
#pragma once

#include <QMainWindow>
#include <QObject>
#include "controls/QPulse.h"
#include "pulse/cdm/engine/SEAction.h"
#include "pulse/cdm/engine/SEDataRequest.h"
#include "pulse/cdm/scenario/SEScenario.h"

namespace Ui {
  class MainExplorerWindow;
}

class MainExplorerWindow : public QMainWindow, public PulseListener
{
  Q_OBJECT

public:
  MainExplorerWindow();
  ~MainExplorerWindow();
  void closeEvent(QCloseEvent *event) override;

  void AtSteadyState(PhysiologyEngine& pulse) override;
  void AtSteadyStateUpdateUI() override; // Main Window will call this to update UI Components
  void ProcessPhysiology(PhysiologyEngine& pulse) override;
  void PhysiologyUpdateUI(const std::vector<SEAction const*>& actions) override;// Main Window will call this to update UI Components
  void EngineErrorUI() override;// Main Window will call this to update UI Components

  signals:
  void setRequest(SEDataRequest& request);

protected:

protected slots:
  void PlayPause();
  void RunInRealtime();
  void ResetEditor();
  void ClearEditor();
  void RestartExplorer();
  void UpdateAction(SEAction const&, SEScalarTime const&);
  void LoadScenario();
  void SaveScenario();

  void StartShowcase();
  void StartPulseEditor();
  /**/void StartEngine();

private:
  class Controls;
  Controls* m_Controls;
};
