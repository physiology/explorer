# Command: `DOCKER_BUILDKIT=1 docker build -o out .`

ARG BASE=slicer/buildenv-qt5-centos7:latest
FROM $BASE AS build-stage

ENV TERM linux

# Install necessary dependencies

RUN yum --disablerepo=centos-sclo-rh --disablerepo=centos-sclo-sclo \
    install -y fontconfig-devel libxkbcommon-devel

RUN rpm --import https://yum.corretto.aws/corretto.key && \
    curl -L -o /etc/yum.repos.d/corretto.repo https://yum.corretto.aws/corretto.repo && \
    yum --disablerepo=centos-sclo-rh --disablerepo=centos-sclo-sclo install -y java-17-amazon-corretto-devel \
    export JAVA_HOME=/usr/lib/jvm/java-1.8.0-amazon-corretto


COPY . /source

RUN mkdir build && \
    mkdir explorer && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE:STRING=Release \
          -DCMAKE_INSTALL_PREFIX=/explorer \
          /source && \
    make -j8 && \
    cd Innerbuild && \
    make package

# Build-time metadata as defined at http://label-schema.org
ARG BUILD_DATE
ARG IMAGE=explorer_buildenv
ARG VERSION=latest
ARG VCS_REF
ARG VCS_URL
LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.name=$IMAGE \
      org.label-schema.version=$VERSION \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url=$VCS_URL \
      org.label-schema.schema-version="1.0"


# Copy package back to host
# TODO: It's possible that the package name changes, if so would need to be changed here
#       as well (or copy entire Innerbuild) or determine name during build somehow
FROM scratch AS export-stage
COPY --from=build-stage /work/build/Innerbuild/PulseExplorer-3.0.0-el7.x86_64.tar.gz PulseExplorer-3.0.0-el7.x86_64.tar.gz
