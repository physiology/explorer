
function(define_dependency NAME)
  option(USE_SYSTEM_${NAME}
    "Exclude ${NAME} from superbuild and use an external version."
    OFF
    )
  list(APPEND ${PROJECT_NAME}_DEPENDENCIES ${NAME})
  set(${PROJECT_NAME}_DEPENDENCIES
    "${${PROJECT_NAME}_DEPENDENCIES}" PARENT_SCOPE
    )
endfunction()

# Find Qt now so that we can pass its location to dependencies
find_package(Qt5 COMPONENTS Core REQUIRED)

# Add our external dependencies
define_dependency(Pulse)
define_dependency(Qwt)
