set(QT5_LINUX)
if (UNIX AND NOT APPLE)
  set(QT5_LINUX
      XcbQpa
      DBus)
endif()

find_package(Qt5 REQUIRED COMPONENTS Core
                                     Widgets
                                     OpenGL
                                     Concurrent
                                     Svg
                                     PrintSupport
                                     ${QT5_LINUX})

foreach(comp ${QT5_LINUX})
  list(APPEND QT5_LINUX_LIBS "Qt5::${comp}")
endforeach()
list(APPEND QT_LIBRARIES
    "Qt5::Core"
    "Qt5::Gui"
    "Qt5::Widgets"
    "Qt5::OpenGL"
    "Qt5::Concurrent"
    "Qt5::Svg"
    "Qt5::PrintSupport"
    ${QT5_LINUX_LIBS}
)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
  ${CMAKE_CURRENT_SOURCE_DIR}/cmake
  ${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules
)
find_package(Qwt REQUIRED)
find_package(Pulse REQUIRED)
