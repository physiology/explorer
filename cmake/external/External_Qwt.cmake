#-----------------------------------------------------------------------------
# Add External Project
#-----------------------------------------------------------------------------
set(_make_command make)
if(MSVC)
  set(_make_command nmake)
elseif(MINGW)
  set(_make_command mingw32-make)
endif()
include(${CMAKE_CURRENT_SOURCE_DIR}/AddExternalProject.cmake)
define_external_dirs_ex(Qwt)
add_external_project_ex(Qwt
  URL https://sourceforge.net/projects/qwt/files/qwt/6.1.5/qwt-6.1.5.zip/download
  URL_MD5 61a8cae35ab6201d916304ec4a6f06b8
  PATCH_COMMAND
    COMMAND ${CMAKE_COMMAND} -DQwt_INSTALL_DIR=${Qwt_PREFIX}/install
                             -DQwt_SOURCE_DIR=${Qwt_SOURCE_DIR}
                             -DQwt_PATCH_DIR=${CMAKE_SOURCE_DIR}/cmake/patches/Qwt
      -P ${CMAKE_SOURCE_DIR}/cmake/patches/Qwt/patch.cmake
  CONFIGURE_COMMAND
    Qt5::qmake ${Qwt_SOURCE_DIR}/qwt.pro
  BUILD_COMMAND
    ${_make_command}
  INSTALL_COMMAND
    ${_make_command} install
  RELATIVE_INCLUDE_PATH ""
  DEPENDENCIES ""
  #VERBOSE
  )

if (NOT USE_SYSTEM_Qwt)
  set(Qwt_ROOT_DIR ${Qwt_PREFIX}/install)
  message(STATUS "Qwt_ROOT_DIR : ${Qwt_ROOT_DIR}")
endif()
