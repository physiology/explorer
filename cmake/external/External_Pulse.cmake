#-----------------------------------------------------------------------------
# Add External Project
#-----------------------------------------------------------------------------
include(${CMAKE_CURRENT_SOURCE_DIR}/AddExternalProject.cmake)
define_external_dirs_ex(Pulse)
add_external_project_ex(Pulse
  GIT_REPOSITORY https://gitlab.kitware.com/physiology/engine.git
  GIT_TAG REL_4_3_1
  CMAKE_CACHE_ARGS
      -DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_INSTALL_PREFIX}
      -DPulse_DEPENDENT_BUILD:BOOL=ON
      -DPulse_JAVA_API:BOOL=ON
      -DPulse_PYTHON_API:BOOL=ON
      -DPulse_MSVC_STATIC_RUNTIME:BOOL=OFF
  INSTALL_COMMAND ${SKIP_STEP_COMMAND}
  RELATIVE_INCLUDE_PATH ""
  DEPENDENCIES ""
  #VERBOSE
  )

if (NOT USE_SYSTEM_Pulse)
  set(Pulse_DIR ${CMAKE_INSTALL_PREFIX}/lib/cmake/pulse)
  message(STATUS "Pulse_DIR : ${Pulse_DIR}")
endif()
