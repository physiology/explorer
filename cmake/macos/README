MacOS Signing Notes
---------------------

Building QT Versions
%%%%%%%%%%%%%%%%%%%%%

Before Compiling:
^^^^^^^^^^^^^^^^^

For sufficiently new versions of XCode (>15.0) with the targeted version of QT (5.15),
there are some changes that need to be made for QMake to properly function. This is needed
for both installed and built versions of QT.  The reasoning and the changes, which need to be updated
can be found here: https://bugreports.qt.io/browse/QTBUG-117225

For installed versions, change the `<QT_Install_Path>/<QT_VERSION>/clang_64/mkspecs/features/toolchain.prf`
For built versions, the files is in `<QT_SOURCE_DIR>/qtbase/mkspecs/features/toolchain.prf`

Compiling QT
^^^^^^^^^

For building on MacOS for arm64, a different version of QT 5.15 will be needed.
The arm64 version will need to be built from source, with a version of at least v5.15.4.
The instructions here used the latest version at the time, which was v5.15.11.

After downloading, create a build directory next to the unzipped source and execute the
following command to bootstrap QT.  The path to the `-prefix` argument is where QT will be
installed to so ensure that it is user-writable.

```

../qt-everywhere-src-5.15.11/configure -device-option QMAKE_APPLE_DEVICE_ARCHS=arm64 -nomake tests -nomake examples -opensource -confirm-license  -prefix ~/armQt

```

Compiling Explorer
%%%%%%%%%%%%%%%%%%%

Most of the compilation of the Explorer tool works as it normally would.  The only additional information
that was used was to change one CMake variable: `CMAKE_OSX_DEPLOYMENT_TARGET`.  This value should be
set to the lowest version of OSX that should be supported.  For the 3.1 release, this variable was set to
`11.0`.

Packaging the DMG
%%%%%%%%%%%%%%%%%%

Another issue of note was that the CPack system would always append an `arm64` to the DMG file even when
building the x86 version.  This was avoided by changing the `CPACK_PACKAGE_FILE_NAME` variable in the `CPackConfig.cmake`
file prior to running `make package`.