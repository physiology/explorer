#-----------------------------------------------------------------------------
# Find All Headers and Libraries for Qwt
#-----------------------------------------------------------------------------
if(Qwt_ROOT_DIR)
  include(find)
  if(APPLE)
      set(CMAKE_FIND_FRAMEWORK ONLY)
      find_library(QWT
          NAMES qwt
          HINTS ${Qwt_ROOT_DIR}/lib
          REQUIRED)
      if(QWT)
          set(QWT_INCLUDE_DIRS "${QWT}/Headers")
          string(TOUPPER ${CMAKE_BUILD_TYPE} _CONFIG)
          set(QWT_${_CONFIG}_LIBRARIES ${QWT})
          message("QWT found: ${QWT}")
          message("QWT_INCLUDE_DIRS: ${QWT_INCLUDE_DIRS}}")
          message("QWT_${_CONFIG}_LIBRARIES: ${QWT_${_CONFIG}_LIBRARIES}")
          find_package_ex(Qwt Qwt::Qwt)
      endif()
  else()
    set(Qwt_LIB_DIR lib)
    find_header_ex(Qwt qwt.h include)
    find_libary_ex(Qwt qwt)
    find_package_ex(Qwt Qwt::Qwt)

    #message(STATUS "Qwt include : ${QWT_INCLUDE_DIRS}")
    #message(STATUS "Qwt libraries : ${QWT_LIBRARIES}")
  endif()
else()
  include(FindPackageHandleStandardArgs)

  find_path(Qwt_INCLUDE_DIR qwt.h
    PATH_SUFFIXES qt5/qwt
    ${Qwt_FIND_OPTS}
  )
  find_library(Qwt_LIBRARY qwt-qt5 ${Qwt_FIND_OPTS})

  find_package_handle_standard_args(
    Qwt REQUIRED_VARS Qwt_LIBRARY Qwt_INCLUDE_DIR)

  if(Qwt_FOUND)
    if(NOT TARGET Qwt::Qwt)
      add_library(Qwt::Qwt UNKNOWN IMPORTED)
      set_target_properties(Qwt::Qwt PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${Qwt_INCLUDE_DIR}")

      if(EXISTS "${Qwt_LIBRARY}")
        set_target_properties(Qwt::Qwt PROPERTIES
          IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
          IMPORTED_LOCATION "${Qwt_LIBRARY}")
      endif()
    endif()
  endif()
endif()
